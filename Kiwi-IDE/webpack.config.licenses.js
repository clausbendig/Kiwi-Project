/*
* Copyright (C) Claus Bendig
*/

'use strict'

const { VueLoaderPlugin } = require('vue-loader');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
var LicenseWebpackPlugin = require('license-webpack-plugin').LicenseWebpackPlugin;
const path = require('path');

module.exports = {
  target: "electron-renderer",
  mode: 'production',
  entry: {
    licenses: './src/licenses.js',
  },
  module: {
    rules: [
      { test:/\.css$/, use:['style-loader','css-loader'] },
      { test:/\.less$/, use:['style-loader','css-loader', 'less-loader'] },
      { test: /\.vue$/, use: 'vue-loader' },
      { test: /\.(png|woff|woff2|eot|ttf|svg)$/, loader: 'url-loader', options: {limit: 10, name: '[name].[ext]'}},
    ]
  },
  plugins: [
    new webpack.ProvidePlugin({
      $: "jquery",
      jQuery: "jquery"
    }),
    new HtmlWebpackPlugin({
      filename: '../licenses.html',
      template: 'build/licenses.html',
      inject: true,
      chunks: ['licenses'],
    }),
  ],
  output: {
    publicPath: 'static/',
    path: path.resolve(__dirname, 'app/static'),
    filename: '[name].js',
  },
};