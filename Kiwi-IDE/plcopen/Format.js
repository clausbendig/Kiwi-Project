var Project = {
  Meta: {
    UUID: '',
    Name: 'Name',
    Author: 'John Doe',
    CompanyName: 'Company',
    CompanyURL: 'https://company.com',
    ProductName: 'Product',
    Description: 'This is an example description.',
    CreationDateTime: '27.03.1984-23:00',
    ModificationDateTime: '27.03.1984-23:00',
  },
  DataTypes: [
    {
      Name: 'Directly',
      InitialValue: 'FALSE',
      DerivationType: 'Directly',
      BaseType: 'BOOL'
    },
    {
      Name: 'Subrange',
      InitialValue: '0',
      Derivation: 'Subrange',
      BaseType: 'SINT',
      Range: {
        Lower: -128,
        Upper: 127
      }
    },
    {
      Name: 'Array',
      InitialValue: '0',
      DerivationType: 'Array',
      BaseType: 'SINT',
      Dimensions: [
        {
          Lower: 0,
          Upper: 9
        }
      ]
    },
    {
      Name: 'Enumeration',
      InitialValue: 'A',
      DerivationType: 'Enumeration',
      Members: [
        'A',
        'B',
        'C'
      ]
    },
    {
      Name: 'Structure',
      InitialValue: '',
      DerivationType: 'Structure',
      Members: [
        {
          Name: 'A',
          BaseType: 'BOOL',
          InitialValue: ''
        },
        {
          Name: 'B',
          BaseType: 'Directly',
          InitialValue: ''
        },{
          Name: 'C',
          BaseType: 'Array',
          InitialValue: ''
        }
      ]
    }
  ],
  Programs: [
    {
      Name: 'Name',
      Language: 'FBD',
      Type: 'Program',
      Variables: [
        {
          Name: 'A',
          DataType: 'BOOL',
          InitialValue: '',
          Class: '',
          Address: '',
          Option: 'Retain || Non-Retain || Persistent || Non-Persistent || Constant',
          Documentation: 'This is a documentation.',
        }
      ],
      Code: {
      }
    }
  ],
  Functions: [
    {
      Name: 'Name',
      Language: 'FBD',
      Type: 'Function',
      Variables: [
        {
          Name: 'A',
          DataType: 'BOOL',
          InitialValue: '',
          Class: '',
          Option: 'Retain || Non-Retain || Persistent || Non-Persistent || Constant',
          Documentation: 'This is a documentation.',
        }
      ],
      Code: {
      }
    }
  ],
  FunctionBlocks: [
    {
      Name: 'Name',
      Language: 'FBD',
      Type: 'FunctionBlock',
      Variables: [
        {
          Name: 'A',
          DataType: 'BOOL',
          InitialValue: '',
          Address: '',
          Class: '',
          Option: 'Retain || Non-Retain || Persistent || Non-Persistent || Constant',
          Documentation: 'This is a documentation.',
        }
      ],
      Code: {
      }
    }
  ],
  Resource: {
    Variables: [
      {
        Name: 'A',
        DataType: 'BOOL',
        InitialValue: '',
        Address: '',
        Option: 'Retain || Non-Retain || Persistent || Non-Persistent || Constant',
        Documentation: 'This is a documentation.',
      }
    ],
    Tasks: [
      {
        Name: 'MainTask',
        Priority: 0,
        Program: info.ProductName,
        Type: 'Periodic || EventDriven',
        Cycle: 'T#20MS',
        Event: 'A',
      }
    ]
  },
};