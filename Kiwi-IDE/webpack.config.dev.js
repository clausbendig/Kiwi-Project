/*
* Copyright (C) Claus Bendig
*/

'use strict'

const { VueLoaderPlugin } = require('vue-loader');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
var LicenseWebpackPlugin = require('license-webpack-plugin').LicenseWebpackPlugin;
const path = require('path');

module.exports = {
  target: "electron-renderer",
  mode: 'development',
  entry: {
    app: './src/app.js',
  },
  module: {
    rules: [
      { test:/\.css$/, use:['style-loader','css-loader'] },
      { test:/\.less$/, use:['style-loader','css-loader', 'less-loader'] },
      { test: /\.vue$/, use: 'vue-loader' },
      { test: /\.(png|woff|woff2|eot|ttf|svg)$/, loader: 'url-loader', options: { limit: 10, name: '[name].[ext]' }},
      { test: /\.ejs$/, loader: 'compile-ejs-loader' }
    ]
  },
  plugins: [
    new LicenseWebpackPlugin({
      pattern: /.*/,
      outputTemplate: 'src/licenses.ejs',
      outputFilename: '../../build/licenses.html',
      //unacceptablePattern: /BSD-3-Clause/,
      modulesDirectories: ['node_modules']
    }),
    new VueLoaderPlugin(),
    new webpack.ProvidePlugin({
      $: "jquery",
      jQuery: "jquery",
      _: "underscore"
    }),
    new HtmlWebpackPlugin({
      filename: '../index.html',
      template: 'src/index.html',
      inject: true,
      chunks: ['app'],
    }),
    new CleanWebpackPlugin(['dist', 'app', 'build/licenses.html']),
  ],
  output: {
    publicPath: 'static/',
    path: path.resolve(__dirname, 'app/static'),
    filename: '[name].js',
  },
};