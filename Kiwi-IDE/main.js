/*
* Copyright (C) Claus Bendig
*/

const electron = require('electron');
const { app, BrowserWindow, Menu } = require('electron');

let mainWindow;

var shouldQuit = app.makeSingleInstance(function(commandLine, workingDirectory) {
  if (mainWindow) {
    if (mainWindow.isMinimized()) mainWindow.restore();
    mainWindow.focus();
  }
});

if (shouldQuit) {
  app.quit();
  return;
}

function createWindow () {
  let bounds = electron.screen.getPrimaryDisplay().bounds;
  let x = bounds.x + ((bounds.width - 1280) / 2);
  let y = bounds.y + ((bounds.height - 720) / 2);

  if (process.env.NODE_ENV == 'development') {
    mainWindow = new BrowserWindow({ width: 1280, title: 'Kiwi-IDE', height: 720,  minWidth: 1280, minHeight: 720, toolbar: false, backgroundColor: "#353c42", show: false, x: x, y: y, center: true });
  } else {
    mainWindow = new BrowserWindow({ width: 1280, title: 'Kiwi-IDE', height: 720, minWidth: 1280, minHeight: 720, toolbar: false, backgroundColor: "#353c42", x: x, y: y, show: false, center: true });
  }
  
  mainWindow.loadFile('app/index.html');

  mainWindow.setMenu(null);
  if (process.env.NODE_ENV == 'development') {
    mainWindow.webContents.openDevTools();
  }

  mainWindow.on('page-title-updated', function(e) {
    e.preventDefault()
  });

  mainWindow.on('close', function(e) {
    e.preventDefault();
  });
}

app.on('ready', function() {
  if (process.env.NODE_ENV == 'development') require('vue-devtools').install();
  createWindow();
});

app.on('window-all-closed', function () {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', function () {
  if (mainWindow === null) {
    createWindow();
  }
});
