const ILConfiguration = {
	comments: {
		lineComment: '//',
		blockComment: ['(*', '*)'],
	},
	brackets: [
		['{', '}'],
		['[', ']'],
		['(', ')'],
	],
	autoClosingPairs: [
		{ open: '[', close: ']' },
		{ open: '{', close: '}' },
		{ open: '(', close: ')' },
		{ open: '/*', close: '*/' },
		{ open: '\'', close: '\'', notIn: ['string_sq'] },
		{ open: '"', close: '"', notIn: ['string_dq'] },
	],
	surroundingPairs: [
		{ open: '{', close: '}' },
		{ open: '[', close: ']' },
		{ open: '(', close: ')' },
		{ open: '"', close: '"' },
		{ open: '\'', close: '\'' },
	],
	folding: {
		markers: {
			start: new RegExp("^\\s*#pragma\\s+region\\b"),
			end: new RegExp("^\\s*#pragma\\s+endregion\\b")
		}
	}
};

const ILLanguage = {
	defaultToken: '',
	tokenPostfix: '.st',
	ignoreCase: true,

	brackets: [
		{ token: 'delimiter.curly', open: '{', close: '}' },
		{ token: 'delimiter.parenthesis', open: '(', close: ')' },
		{ token: 'delimiter.square', open: '[', close: ']' }
	],

	keywords: [],

	constant: ['false', 'true', 'null'],

	defineKeywords: [],

	typeKeywords: ['INT', 'SINT', 'DINT', 'LINT', 'USINT', 'UINT', 'UDINT', 'ULINT',
		'REAL', 'LREAL', 'TIME', 'DATE', 'TIME_OF_DAY', 'DATE_AND_TIME', 'STRING',
		'BOOL', 'BYTE', 'WORD', 'DWORD', 'ARRAY', 'POINTER', 'LWORD'],

	operators: ['=', '>', '<', ':', ':=', '<=', '>=', '<>', '&', '+', '-', '*', '**',
		'MOD', '^', 'OR', 'AND', 'NOT', 'XOR', 'ABS', 'ACOS', 'ASIN', 'ATAN', 'COS',
		'EXP', 'EXPT', 'LN', 'LOG', 'SIN', 'SQRT', 'TAN', 'SEL', 'MAX', 'MIN', 'LIMIT',
		'MUX', 'SHL', 'SHR', 'ROL', 'ROR', 'INDEXOF', 'SIZEOF', 'ADR', 'ADRINST',
		'BITADR', 'IS_VALID'],

	builtinVariables: [

	],

	builtinFunctions: ['SR', 'RS', 'TP', 'TON', 'TOF', 'EQ', 'GE', 'LE', 'LT',
		'NE', 'ROUND', 'TRUNC', 'CTD', 'CTU', 'CTUD', 'R_TRIG', 'F_TRIG',
		'MOVE', 'CONCAT', 'DELETE', 'FIND', 'INSERT', 'LEFT', 'LEN', 'REPLACE',
		'RIGHT', 'RTC'],

	// we include these common regular expressions
	symbols: /[=><!~?:&|+\-*\/\^%]+/,

	// C# style strings
	escapes: /\\(?:[abfnrtv\\"']|x[0-9A-Fa-f]{1,4}|u[0-9A-Fa-f]{4}|U[0-9A-Fa-f]{8})/,

	// The main tokenizer for our languages
	tokenizer: {
		root: [
			[/(T|DT|TOD)#[0-9:-_shmyd]*/, 'tag'],
			[/[A-Za-z]{1,6}#[0-9]*/, 'tag'],
			[/\%(I|Q|M)(X|B|W|D|L)[0-9\.]*/, 'tag'],
			[/\%(I|Q|M)[0-9\.]*/, 'tag'],
			[/(TO_|CTU_|CTD_|CTUD_|MUX_|SEL_)[A_Za-z]*/, 'predefined'],
			[/[A_Za-z]*(_TO_)[A_Za-z]*/, 'predefined'],

			// identifiers and keywords
			[/[a-zA-Z_]\w*/, {
				cases: {
					'@operators': 'operators',
					'@keywords': 'keyword',
					'@typeKeywords': 'type',
					'@defineKeywords': 'variable',
					'@constant': 'constant',
					'@builtinVariables': 'predefined',
					'@builtinFunctions': 'predefined',
					'@default': 'identifier'
				}
			}],

			{ include: '@whitespace' },

			[/[;.]/, 'delimiter'],
			[/[{}()\[\]]/, '@brackets'],
			[/\d*\.\d+([eE][\-+]?\d+)?/, 'number.float'],
			[/16#[0-9a-fA-F]+/, 'number.hex'],
			[/2#[0-9_]+/, 'number.binary'],
			[/\d+/, 'number'],

			[/"([^"\\]|\\.)*$/, 'string.invalid'],  // non-teminated string
			[/"/, { token: 'string.quote', bracket: '@open', next: '@string_dq' }],
			[/'/, { token: 'string.quote', bracket: '@open', next: '@string_sq' }],

			[/'[^\\']'/, 'string'],
			[/(')(@escapes)(')/, ['string', 'string.escape', 'string']],
			[/'/, 'string.invalid']
		],
		comment: [
			[/[^\/*]+/, 'comment'],
			[/\/\*/, 'comment', '@push'],    // nested comment
			["\\*/", 'comment', '@pop'],
			[/[\/*]/, 'comment']
		],
		comment2: [
			[/[^\(*]+/, 'comment'],
			[/\(\*/, 'comment', '@push'],    // nested comment
			["\\*\\)", 'comment', '@pop'],
			[/[\(*]/, 'comment']
		],
		whitespace: [
			[/[ \t\r\n]+/, 'white'],
			[/\/\/.*$/, 'comment'],
			[/\/\*/, 'comment', '@comment'],
			[/\(\*/, 'comment', '@comment2'],
		],
		string_dq: [
			[/[^\\"]+/, 'string'],
			[/@escapes/, 'string.escape'],
			[/\\./, 'string.escape.invalid'],
			[/"/, { token: 'string.quote', bracket: '@close', next: '@pop' }]
		],
		string_sq: [
			[/[^\\']+/, 'string'],
			[/@escapes/, 'string.escape'],
			[/\\./, 'string.escape.invalid'],
			[/'/, { token: 'string.quote', bracket: '@close', next: '@pop' }]
		]
	}
};

export { ILConfiguration, ILLanguage };