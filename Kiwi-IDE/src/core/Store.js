import Vue from 'vue';
import Vuex from 'vuex';


Vue.use(Vuex);
const Store = new Vuex.Store({
  state: {
    App: {
      LastPath: '',
    },
    Projects: null,
    CurrentProject: undefined,
    UI: {
      Console: {
        Visible: false,
      },
      SourceCode: {
        Visible: false,
      },
      Search: {
        Visible: false,
        Query: '',
        Type: ''
      }
    },
    PLC: {
      Information: {
        canStartStop: false,
        canDebug: false
      },
      Connected: false,
      Compiled: false,
      hasProgram: false,
      Running: false,
      Compiling: false,
      Transfering: false,
      Blocked: false
    }
  },
  mutations: {
  }
});

export default Store;