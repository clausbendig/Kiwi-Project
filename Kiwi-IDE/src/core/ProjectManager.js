/*
* Copyright (C) Claus Bendig
*/

'use strict'

import Database from './Database';
import jetpack from 'fs-jetpack';
import uuidv1 from 'uuid/v1';
import { fstat } from 'fs';

var loadProject = function(project) {
  return new Promise(function(resolve, reject) {
    var p = Database.load(project);
    resolve(p);
  });
};

var saveProject = function(project) {
  return new Promise(function(resolve, reject) {
    project.Meta.ModificationDateTime = Date();
    Database.save(project);
    resolve();
  });
};

var newProject = function(info) {
  return new Promise(function(resolve, reject) {
    var currentDate = new Date();

    var code = {};
    switch(info.ProgramLanguage) {
      case 'IL': 
        code.SourceCode = '';
        break
      
      case 'ST': 
        code.SourceCode = '';
        break

      case 'FBD': 
        break

      case 'LD': 
        break

      case 'SFC': 
        break;
    }

    var kiwi = {
      Meta: {
        UUID: uuidv1(),
        Name: info.ProjectName,
        Author: info.Author,
        CompanyName: info.CompanyName,
        CompanyURL:  info.CompanyURL,
        ProductName: info.ProductName,
        ProductVersion: info.ProductVersion,
        Description: info.Description,
        CreationDateTime: currentDate,
        ModificationDateTime: currentDate,
      },
      DataTypes: [    
      ],
      Programs: [
        {
          Name: info.ProgramName,
          POUType: 'Program',
          Language: info.ProgramLanguage,
          Variables: [],
          Code: code
        }
      ],
      Functions: [
      ],
      FunctionBlocks: [
      ],
      Resource: {
        Variables: [
        ],
        Tasks: [
          {
            Name: 'MainTask',
            Priority: 0,
            Program: info.ProgramName,
            Type: 'Periodic',
            Cycle: 'T#20MS',
            Event: '',
          }
        ]
      }
    };

    Database.add(kiwi);
    Database.save(kiwi);
    resolve(kiwi);
  });
};

var importProject = function(fromPath) {
  return new Promise(function(resolve, reject) {
    try {
      var json = jetpack.read(fromPath);
      var project = JSON.parse(json);
      project.Meta.UUID = uuidv1();
      Database.add(project);
      Database.save(project);
      resolve(project);
    } catch(e) {
      reject();
    }
  });
};

var exportProject = function(project, toPath) {
  return new Promise(function(resolve, reject) {
    try {
      jetpack.write(toPath, JSON.stringify(project));
      resolve();
    } catch(e) {
      reject();
    }
  });
};

var deleteProject = function(project) {
  return new Promise(function(resolve, reject) {
    Database.remove(project);
    resolve();
  });
};

var getProjects = function() {
  return Database.getProjects();
};

var ProjectManager = {
  getProjects: getProjects,
  newProject: newProject,
  importProject: importProject,
  exportProject: exportProject,
  deleteProject: deleteProject,
  loadProject: loadProject,
  saveProject: saveProject
};

export default ProjectManager;
  