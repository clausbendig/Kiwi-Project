var functions = {
  Bit: [
    {
      Name: 'AND',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN1',
          DataType: 'ANY_BIT',
          Class: 'Input',
          Documentation: '',
        },
        {
          Name: 'IN2',
          DataType: 'ANY_BIT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'ANY_BIT',
        Documentation: '',
      }
    },
    {
      Name: 'OR',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN1',
          DataType: 'ANY_BIT',
          Class: 'Input',
          Documentation: '',
        },
        {
          Name: 'IN2',
          DataType: 'ANY_BIT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'ANY_BIT',
        Documentation: '',
      }
    },
    {
      Name: 'XOR',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN1',
          DataType: 'ANY_BIT',
          Class: 'Input',
          Documentation: '',
        },
        {
          Name: 'IN2',
          DataType: 'ANY_BIT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'ANY_BIT',
        Documentation: '',
      }
    },
    {
      Name: 'NOT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'ANY_BIT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'ANY_BIT',
        Documentation: '',
      }
    },
    {
      Name: 'SR',
      Language: 'ST',
      POUType: 'FunctionBlock',
      Variables: [
        {
          Name: 'S',
          DataType: 'BOOL',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'R',
          DataType: 'BOOL',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'Q',
          DataType: 'BOOL',
          Class: 'Output',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        }
      ],
      Code: 'Q := S OR ((NOT R) AND Q);'
    },
    {
      Name: 'RS',
      Language: 'ST',
      POUType: 'FunctionBlock',
      Variables: [
        {
          Name: 'R',
          DataType: 'BOOL',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'S',
          DataType: 'BOOL',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'Q',
          DataType: 'BOOL',
          Class: 'Output',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        }
      ],
      Code: 'Q := (NOT R) AND (S OR Q);'
    },
    {
      Name: 'R_TRIG',
      Language: 'ST',
      POUType: 'FunctionBlock',
      Variables: [
        {
          Name: 'CLK',
          DataType: 'BOOL',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'Q',
          DataType: 'BOOL',
          Class: 'Output',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'M',
          DataType: 'BOOL',
          Class: 'Local',
          InitialValue: '',
          Address: '',
          Option: 'Retain',
          Documentation: '',
        },
      ],
      Code: 'Q := CLK AND NOT M; M := CLK;'
    },
    {
      Name: 'F_TRIG',
      Language: 'ST',
      POUType: 'FunctionBlock',
      Variables: [
        {
          Name: 'CLK',
          DataType: 'BOOL',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'Q',
          DataType: 'BOOL',
          Class: 'Output',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'M',
          DataType: 'BOOL',
          Class: 'Local',
          InitialValue: '',
          Address: '',
          Option: 'Retain',
          Documentation: '',
        },
      ],
      Code: 'Q := NOT CLK AND NOT M; M := NOT CLK;'
    },
  ],
  Additional: [
    {
      Name: 'RTC',
      Language: 'ST',
      POUType: 'FunctionBlock',
      Variables: [
        {
          Name: 'IN',
          DataType: 'BOOL',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'PDT',
          DataType: 'DT',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'Q',
          DataType: 'BOOL',
          Class: 'Output',
          InitialValue: 'FALSE',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'CDT',
          DataType: 'DT',
          Class: 'Output',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'PREV_IN',
          DataType: 'BOOL',
          Class: 'Local',
          InitialValue: 'FALSE',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'OFFSET',
          DataType: 'TIME',
          Class: 'Local',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'CURRENT_TIME',
          DataType: 'DT',
          Class: 'Local',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
      ],
      Code: '{__SET_VAR(data__->,CURRENT_TIME,,__CURRENT_TIME)}\n\nIF IN\nTHEN\n  IF NOT PREV_IN\n  THEN\n      OFFSET := PDT - CURRENT_TIME;\n  END_IF;\n\n  (* PDT + time since PDT was loaded *)\n  CDT := CURRENT_TIME + OFFSET;\nELSE\n  CDT := CURRENT_TIME;\nEND_IF;\n\nQ := IN;\nPREV_IN := IN;'
    },
    {
      Name: 'SEMA',
      Language: 'ST',
      POUType: 'FunctionBlock',
      Variables: [
        {
          Name: 'CLAIM',
          DataType: 'BOOL',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'RELEASE',
          DataType: 'BOOL',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'BUSY',
          DataType: 'BOOL',
          Class: 'OUTPUT',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'Q_INTERNAL',
          DataType: 'BOOL',
          Class: 'Local',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
      ],
      Code: 'Q_INTERNAL := CLAIM OR ( Q_INTERNAL AND (NOT RELEASE)); BUSY := Q_INTERNAL;'
    },
  ],
  Regulation: [
    {
      Name: 'PID',
      Language: 'ST',
      POUType: 'FunctionBlock',
      Variables: [
        {
          Name: 'AUTO',
          DataType: 'BOOL',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'PV',
          DataType: 'REAL',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'SP',
          DataType: 'REAL',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'X0',
          DataType: 'REAL',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'KP',
          DataType: 'REAL',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'TR',
          DataType: 'REAL',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'TD',
          DataType: 'REAL',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'CYCLE',
          DataType: 'TIME',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'XOUT',
          DataType: 'REAL',
          Class: 'Output',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'ERROR',
          DataType: 'REAL',
          Class: 'Local',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'ITERM',
          DataType: 'INTEGRAL',
          Class: 'Local',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'DTERM',
          DataType: 'DERIVATIVE',
          Class: 'Local',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
      ],
      Code: 'ERROR := PV - SP ;\n(*** Adjust ITERM so that XOUT := X0 when AUTO = 0 ***)\nITERM(RUN := AUTO, R1 := NOT AUTO, XIN := ERROR, X0 := TR * (X0 - ERROR), CYCLE := CYCLE);\nDTERM(RUN := AUTO, XIN := ERROR, CYCLE := CYCLE);\nXOUT := KP * (ERROR + ITERM.XOUT/TR + DTERM.XOUT*TD);'
    },
    {
      Name: 'RAMP',
      Language: 'ST',
      POUType: 'FunctionBlock',
      Variables: [
        {
          Name: 'RUN',
          DataType: 'BOOL',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'X0',
          DataType: 'REAL',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'X1',
          DataType: 'REAL',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'TR',
          DataType: 'TIME',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'CYCLE',
          DataType: 'TIME',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'BUSY',
          DataType: 'BOOL',
          Class: 'Output',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'XOUT',
          DataType: 'REAL',
          Class: 'Output',
          InitialValue: '0.0',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'XI',
          DataType: 'REAL',
          Class: 'Local',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'T',
          DataType: 'TIME',
          Class: 'Local',
          InitialValue: 'T#0S',
          Address: '',
          Option: '',
          Documentation: '',
        },
      ],
      Code: 'BUSY := RUN;\nIF RUN THEN\n  IF T >= TR THEN\n    BUSY := 0;\n    XOUT := X1;\n  ELSE XOUT := XI + (X1-XI) * TIME_TO_REAL(T)\n                            / TIME_TO_REAL(TR);\n    T := T + CYCLE;\n  END_IF;\nELSE\n  XOUT := X0;\n  XI := X0;\n  T := T#0s;\nEND_IF;'
    },
    {
      Name: 'HYSTERESIS',
      Language: 'ST',
      POUType: 'FunctionBlock',
      Variables: [
        {
          Name: 'XIN1',
          DataType: 'REAL',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'XIN2',
          DataType: 'REAL',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'EPS',
          DataType: 'REAL',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'Q',
          DataType: 'BOOL',
          Class: 'Output',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
      ],
      Code: 'IF Q THEN\n  IF XIN1 < (XIN2 - EPS) THEN\n    Q := 0;\n  END_IF;\nELSIF XIN1 > (XIN2 + EPS) THEN\n  Q := 1;\nEND_IF;'
    },
  ],
  TypeConversion: [
    {
      Name: 'BOOL_TO_SINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'BOOL',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'SINT',
        Documentation: '',
      }
    },
    {
      Name: 'BOOL_TO_INT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'BOOL',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'INT',
        Documentation: '',
      }
    },
    {
      Name: 'BOOL_TO_DINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'BOOL',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'DINT',
        Documentation: '',
      }
    },
    {
      Name: 'BOOL_TO_LINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'BOOL',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'LINT',
        Documentation: '',
      }
    },
    {
      Name: 'BOOL_TO_USINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'BOOL',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'USINT',
        Documentation: '',
      }
    },
    {
      Name: 'BOOL_TO_UINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'BOOL',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'UINT',
        Documentation: '',
      }
    },
    {
      Name: 'BOOL_TO_UDINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'BOOL',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'UDINT',
        Documentation: '',
      }
    },
    {
      Name: 'BOOL_TO_ULINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'BOOL',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'ULINT',
        Documentation: '',
      }
    },
    {
      Name: 'BOOL_TO_BYTE',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'BOOL',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'BYTE',
        Documentation: '',
      }
    },
    {
      Name: 'BOOL_TO_WORD',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'BOOL',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'WORD',
        Documentation: '',
      }
    },
    {
      Name: 'BOOL_TO_DWORD',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'BOOL',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'DWORD',
        Documentation: '',
      }
    },
    {
      Name: 'BOOL_TO_LWORD',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'BOOL',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'LWORD',
        Documentation: '',
      }
    },
    {
      Name: 'BOOL_TO_REAL',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'BOOL',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'REAL',
        Documentation: '',
      }
    },
    {
      Name: 'BOOL_TO_LREAL',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'BOOL',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'LREAL',
        Documentation: '',
      }
    },
    {
      Name: 'BOOL_TO_STRING',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'BOOL',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'STRING',
        Documentation: '',
      }
    },
    {
      Name: 'BOOL_TO_DATE',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'BOOL',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'DATE',
        Documentation: '',
      }
    },
    {
      Name: 'BOOL_TO_TOD',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'BOOL',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'TOD',
        Documentation: '',
      }
    },
    {
      Name: 'BOOL_TO_DT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'BOOL',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'DT',
        Documentation: '',
      }
    },
    {
      Name: 'BOOL_TO_TIME',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'BOOL',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'TIME',
        Documentation: '',
      }
    },
    {
      Name: 'REAL_TO_BOOL',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'REAL',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'BOOL',
        Documentation: '',
      }
    },
    {
      Name: 'SINT_TO_BOOL',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'SINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'BOOL',
        Documentation: '',
      }
    },
    {
      Name: 'SINT_TO_INT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'SINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'INT',
        Documentation: '',
      }
    },
    {
      Name: 'SINT_TO_DINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'SINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'DINT',
        Documentation: '',
      }
    },
    {
      Name: 'SINT_TO_LINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'SINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'LINT',
        Documentation: '',
      }
    },
    {
      Name: 'SINT_TO_USINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'SINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'USINT',
        Documentation: '',
      }
    },
    {
      Name: 'SINT_TO_UINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'SINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'UINT',
        Documentation: '',
      }
    },
    {
      Name: 'SINT_TO_UDINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'SINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'UDINT',
        Documentation: '',
      }
    },
    {
      Name: 'SINT_TO_ULINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'SINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'ULINT',
        Documentation: '',
      }
    },
    {
      Name: 'SINT_TO_BYTE',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'SINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'BYTE',
        Documentation: '',
      }
    },
    {
      Name: 'SINT_TO_WORD',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'SINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'WORD',
        Documentation: '',
      }
    },
    {
      Name: 'SINT_TO_DWORD',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'SINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'DWORD',
        Documentation: '',
      }
    },
    {
      Name: 'SINT_TO_LWORD',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'SINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'LWORD',
        Documentation: '',
      }
    },
    {
      Name: 'SINT_TO_REAL',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'SINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'REAL',
        Documentation: '',
      }
    },
    {
      Name: 'SINT_TO_LREAL',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'SINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'LREAL',
        Documentation: '',
      }
    },
    {
      Name: 'SINT_TO_STRING',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'SINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'STRING',
        Documentation: '',
      }
    },
    {
      Name: 'SINT_TO_DATE',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'SINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'DATE',
        Documentation: '',
      }
    },
    {
      Name: 'SINT_TO_TOD',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'SINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'TOD',
        Documentation: '',
      }
    },
    {
      Name: 'SINT_TO_DT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'SINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'DT',
        Documentation: '',
      }
    },
    {
      Name: 'SINT_TO_TIME',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'SINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'TIME',
        Documentation: '',
      }
    },
    {
      Name: 'INT_TO_BOOL',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'INT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'BOOL',
        Documentation: '',
      }
    },
    {
      Name: 'INT_TO_SINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'INT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'SINT',
        Documentation: '',
      }
    },
    {
      Name: 'INT_TO_DINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'INT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'DINT',
        Documentation: '',
      }
    },
    {
      Name: 'INT_TO_LINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'INT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'LINT',
        Documentation: '',
      }
    },
    {
      Name: 'INT_TO_USINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'INT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'USINT',
        Documentation: '',
      }
    },
    {
      Name: 'INT_TO_UINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'INT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'UINT',
        Documentation: '',
      }
    },
    {
      Name: 'INT_TO_UDINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'INT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'UDINT',
        Documentation: '',
      }
    },
    {
      Name: 'INT_TO_ULINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'INT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'ULINT',
        Documentation: '',
      }
    },
    {
      Name: 'INT_TO_BYTE',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'INT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'BYTE',
        Documentation: '',
      }
    },
    {
      Name: 'INT_TO_WORD',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'INT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'WORD',
        Documentation: '',
      }
    },
    {
      Name: 'INT_TO_DWORD',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'INT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'DWORD',
        Documentation: '',
      }
    },
    {
      Name: 'INT_TO_LWORD',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'INT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'LWORD',
        Documentation: '',
      }
    },
    {
      Name: 'INT_TO_REAL',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'INT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'REAL',
        Documentation: '',
      }
    },
    {
      Name: 'INT_TO_LREAL',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'INT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'LREAL',
        Documentation: '',
      }
    },
    {
      Name: 'INT_TO_STRING',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'INT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'STRING',
        Documentation: '',
      }
    },
    {
      Name: 'INT_TO_DATE',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'INT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'DATE',
        Documentation: '',
      }
    },
    {
      Name: 'INT_TO_TOD',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'INT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'TOD',
        Documentation: '',
      }
    },
    {
      Name: 'INT_TO_DT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'INT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'DT',
        Documentation: '',
      }
    },
    {
      Name: 'INT_TO_TIME',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'INT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'TIME',
        Documentation: '',
      }
    },
    {
      Name: 'DINT_TO_BOOL',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'DINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'BOOL',
        Documentation: '',
      }
    },
    {
      Name: 'DINT_TO_SINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'DINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'SINT',
        Documentation: '',
      }
    },
    {
      Name: 'DINT_TO_INT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'DINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'INT',
        Documentation: '',
      }
    },
    {
      Name: 'DINT_TO_LINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'DINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'LINT',
        Documentation: '',
      }
    },
    {
      Name: 'DINT_TO_USINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'DINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'USINT',
        Documentation: '',
      }
    },
    {
      Name: 'DINT_TO_UINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'DINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'UINT',
        Documentation: '',
      }
    },
    {
      Name: 'DINT_TO_UDINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'DINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'UDINT',
        Documentation: '',
      }
    },
    {
      Name: 'DINT_TO_ULINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'DINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'ULINT',
        Documentation: '',
      }
    },
    {
      Name: 'DINT_TO_BYTE',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'DINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'BYTE',
        Documentation: '',
      }
    },
    {
      Name: 'DINT_TO_WORD',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'DINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'WORD',
        Documentation: '',
      }
    },
    {
      Name: 'DINT_TO_DWORD',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'DINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'DWORD',
        Documentation: '',
      }
    },
    {
      Name: 'DINT_TO_LWORD',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'DINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'LWORD',
        Documentation: '',
      }
    },
    {
      Name: 'DINT_TO_REAL',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'DINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'REAL',
        Documentation: '',
      }
    },
    {
      Name: 'DINT_TO_LREAL',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'DINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'LREAL',
        Documentation: '',
      }
    },
    {
      Name: 'DINT_TO_STRING',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'DINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'STRING',
        Documentation: '',
      }
    },
    {
      Name: 'DINT_TO_DATE',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'DINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'DATE',
        Documentation: '',
      }
    },
    {
      Name: 'DINT_TO_TOD',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'DINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'TOD',
        Documentation: '',
      }
    },
    {
      Name: 'DINT_TO_DT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'DINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'DT',
        Documentation: '',
      }
    },
    {
      Name: 'DINT_TO_TIME',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'DINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'TIME',
        Documentation: '',
      }
    },
    {
      Name: 'LINT_TO_BOOL',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'LINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'BOOL',
        Documentation: '',
      }
    },
    {
      Name: 'LINT_TO_SINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'LINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'SINT',
        Documentation: '',
      }
    },
    {
      Name: 'LINT_TO_INT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'LINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'INT',
        Documentation: '',
      }
    },
    {
      Name: 'LINT_TO_DINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'LINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'DINT',
        Documentation: '',
      }
    },
    {
      Name: 'LINT_TO_USINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'LINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'USINT',
        Documentation: '',
      }
    },
    {
      Name: 'LINT_TO_UINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'LINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'UINT',
        Documentation: '',
      }
    },
    {
      Name: 'LINT_TO_UDINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'LINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'UDINT',
        Documentation: '',
      }
    },
    {
      Name: 'LINT_TO_ULINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'LINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'ULINT',
        Documentation: '',
      }
    },
    {
      Name: 'LINT_TO_BYTE',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'LINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'BYTE',
        Documentation: '',
      }
    },
    {
      Name: 'LINT_TO_WORD',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'LINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'WORD',
        Documentation: '',
      }
    },
    {
      Name: 'LINT_TO_DWORD',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'LINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'DWORD',
        Documentation: '',
      }
    },
    {
      Name: 'LINT_TO_LWORD',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'LINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'LWORD',
        Documentation: '',
      }
    },
    {
      Name: 'LINT_TO_REAL',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'LINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'REAL',
        Documentation: '',
      }
    },
    {
      Name: 'LINT_TO_LREAL',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'LINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'LREAL',
        Documentation: '',
      }
    },
    {
      Name: 'LINT_TO_STRING',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'LINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'STRING',
        Documentation: '',
      }
    },
    {
      Name: 'LINT_TO_DATE',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'LINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'DATE',
        Documentation: '',
      }
    },
    {
      Name: 'LINT_TO_TOD',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'LINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'TOD',
        Documentation: '',
      }
    },
    {
      Name: 'LINT_TO_DT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'LINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'DT',
        Documentation: '',
      }
    },
    {
      Name: 'LINT_TO_TIME',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'LINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'TIME',
        Documentation: '',
      }
    },
    {
      Name: 'USINT_TO_BOOL',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'USINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'BOOL',
        Documentation: '',
      }
    },
    {
      Name: 'USINT_TO_SINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'USINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'SINT',
        Documentation: '',
      }
    },
    {
      Name: 'USINT_TO_INT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'USINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'INT',
        Documentation: '',
      }
    },
    {
      Name: 'USINT_TO_DINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'USINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'DINT',
        Documentation: '',
      }
    },
    {
      Name: 'USINT_TO_LINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'USINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'LINT',
        Documentation: '',
      }
    },
    {
      Name: 'USINT_TO_UINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'USINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'UINT',
        Documentation: '',
      }
    },
    {
      Name: 'USINT_TO_UDINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'USINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'UDINT',
        Documentation: '',
      }
    },
    {
      Name: 'USINT_TO_ULINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'USINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'ULINT',
        Documentation: '',
      }
    },
    {
      Name: 'USINT_TO_BYTE',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'USINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'BYTE',
        Documentation: '',
      }
    },
    {
      Name: 'USINT_TO_WORD',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'USINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'WORD',
        Documentation: '',
      }
    },
    {
      Name: 'USINT_TO_DWORD',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'USINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'DWORD',
        Documentation: '',
      }
    },
    {
      Name: 'USINT_TO_LWORD',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'USINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'LWORD',
        Documentation: '',
      }
    },
    {
      Name: 'USINT_TO_REAL',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'USINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'REAL',
        Documentation: '',
      }
    },
    {
      Name: 'USINT_TO_LREAL',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'USINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'LREAL',
        Documentation: '',
      }
    },
    {
      Name: 'USINT_TO_STRING',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'USINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'STRING',
        Documentation: '',
      }
    },
    {
      Name: 'USINT_TO_DATE',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'USINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'DATE',
        Documentation: '',
      }
    },
    {
      Name: 'USINT_TO_TOD',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'USINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'TOD',
        Documentation: '',
      }
    },
    {
      Name: 'USINT_TO_DT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'USINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'DT',
        Documentation: '',
      }
    },
    {
      Name: 'USINT_TO_TIME',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'USINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'TIME',
        Documentation: '',
      }
    },
    {
      Name: 'UINT_TO_BOOL',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'UINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'BOOL',
        Documentation: '',
      }
    },
    {
      Name: 'UINT_TO_SINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'UINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'SINT',
        Documentation: '',
      }
    },
    {
      Name: 'UINT_TO_INT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'UINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'INT',
        Documentation: '',
      }
    },
    {
      Name: 'UINT_TO_DINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'UINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'DINT',
        Documentation: '',
      }
    },
    {
      Name: 'UINT_TO_LINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'UINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'LINT',
        Documentation: '',
      }
    },
    {
      Name: 'UINT_TO_USINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'UINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'USINT',
        Documentation: '',
      }
    },
    {
      Name: 'UINT_TO_UDINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'UINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'UDINT',
        Documentation: '',
      }
    },
    {
      Name: 'UINT_TO_ULINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'UINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'ULINT',
        Documentation: '',
      }
    },
    {
      Name: 'UINT_TO_BYTE',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'UINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'BYTE',
        Documentation: '',
      }
    },
    {
      Name: 'UINT_TO_WORD',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'UINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'WORD',
        Documentation: '',
      }
    },
    {
      Name: 'UINT_TO_DWORD',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'UINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'DWORD',
        Documentation: '',
      }
    },
    {
      Name: 'UINT_TO_LWORD',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'UINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'LWORD',
        Documentation: '',
      }
    },
    {
      Name: 'UINT_TO_REAL',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'UINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'REAL',
        Documentation: '',
      }
    },
    {
      Name: 'UINT_TO_LREAL',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'UINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'LREAL',
        Documentation: '',
      }
    },
    {
      Name: 'UINT_TO_STRING',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'UINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'STRING',
        Documentation: '',
      }
    },
    {
      Name: 'UINT_TO_DATE',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'UINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'DATE',
        Documentation: '',
      }
    },
    {
      Name: 'UINT_TO_TOD',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'UINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'TOD',
        Documentation: '',
      }
    },
    {
      Name: 'UINT_TO_DT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'UINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'DT',
        Documentation: '',
      }
    },
    {
      Name: 'UINT_TO_TIME',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'UINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'TIME',
        Documentation: '',
      }
    },
    {
      Name: 'UDINT_TO_BOOL',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'UDINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'BOOL',
        Documentation: '',
      }
    },
    {
      Name: 'UDINT_TO_SINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'UDINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'SINT',
        Documentation: '',
      }
    },
    {
      Name: 'UDINT_TO_INT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'UDINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'INT',
        Documentation: '',
      }
    },
    {
      Name: 'UDINT_TO_DINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'UDINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'DINT',
        Documentation: '',
      }
    },
    {
      Name: 'UDINT_TO_LINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'UDINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'LINT',
        Documentation: '',
      }
    },
    {
      Name: 'UDINT_TO_USINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'UDINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'USINT',
        Documentation: '',
      }
    },
    {
      Name: 'UDINT_TO_UINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'UDINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'UINT',
        Documentation: '',
      }
    },
    {
      Name: 'UDINT_TO_ULINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'UDINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'ULINT',
        Documentation: '',
      }
    },
    {
      Name: 'UDINT_TO_BYTE',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'UDINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'BYTE',
        Documentation: '',
      }
    },
    {
      Name: 'UDINT_TO_WORD',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'UDINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'WORD',
        Documentation: '',
      }
    },
    {
      Name: 'UDINT_TO_DWORD',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'UDINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'DWORD',
        Documentation: '',
      }
    },
    {
      Name: 'UDINT_TO_LWORD',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'UDINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'LWORD',
        Documentation: '',
      }
    },
    {
      Name: 'UDINT_TO_REAL',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'UDINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'REAL',
        Documentation: '',
      }
    },
    {
      Name: 'UDINT_TO_LREAL',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'UDINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'LREAL',
        Documentation: '',
      }
    },
    {
      Name: 'UDINT_TO_STRING',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'UDINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'STRING',
        Documentation: '',
      }
    },
    {
      Name: 'UDINT_TO_DATE',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'UDINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'DATE',
        Documentation: '',
      }
    },
    {
      Name: 'UDINT_TO_TOD',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'UDINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'TOD',
        Documentation: '',
      }
    },
    {
      Name: 'UDINT_TO_DT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'UDINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'DT',
        Documentation: '',
      }
    },
    {
      Name: 'UDINT_TO_TIME',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'UDINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'TIME',
        Documentation: '',
      }
    },
    {
      Name: 'ULINT_TO_BOOL',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'ULINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'BOOL',
        Documentation: '',
      }
    },
    {
      Name: 'ULINT_TO_SINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'ULINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'SINT',
        Documentation: '',
      }
    },
    {
      Name: 'ULINT_TO_INT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'ULINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'INT',
        Documentation: '',
      }
    },
    {
      Name: 'ULINT_TO_DINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'ULINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'DINT',
        Documentation: '',
      }
    },
    {
      Name: 'ULINT_TO_LINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'ULINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'LINT',
        Documentation: '',
      }
    },
    {
      Name: 'ULINT_TO_USINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'ULINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'USINT',
        Documentation: '',
      }
    },
    {
      Name: 'ULINT_TO_UINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'ULINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'UINT',
        Documentation: '',
      }
    },
    {
      Name: 'ULINT_TO_UDINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'ULINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'UDINT',
        Documentation: '',
      }
    },
    {
      Name: 'ULINT_TO_BYTE',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'ULINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'BYTE',
        Documentation: '',
      }
    },
    {
      Name: 'ULINT_TO_WORD',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'ULINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'WORD',
        Documentation: '',
      }
    },
    {
      Name: 'ULINT_TO_DWORD',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'ULINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'DWORD',
        Documentation: '',
      }
    },
    {
      Name: 'ULINT_TO_LWORD',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'ULINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'LWORD',
        Documentation: '',
      }
    },
    {
      Name: 'ULINT_TO_REAL',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'ULINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'REAL',
        Documentation: '',
      }
    },
    {
      Name: 'ULINT_TO_LREAL',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'ULINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'LREAL',
        Documentation: '',
      }
    },
    {
      Name: 'ULINT_TO_STRING',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'ULINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'STRING',
        Documentation: '',
      }
    },
    {
      Name: 'ULINT_TO_DATE',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'ULINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'DATE',
        Documentation: '',
      }
    },
    {
      Name: 'ULINT_TO_TOD',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'ULINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'TOD',
        Documentation: '',
      }
    },
    {
      Name: 'ULINT_TO_DT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'ULINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'DT',
        Documentation: '',
      }
    },
    {
      Name: 'ULINT_TO_TIME',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'ULINT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'TIME',
        Documentation: '',
      }
    },
    {
      Name: 'BYTE_TO_BOOL',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'BYTE',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'BOOL',
        Documentation: '',
      }
    },
    {
      Name: 'BYTE_TO_SINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'BYTE',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'SINT',
        Documentation: '',
      }
    },
    {
      Name: 'BYTE_TO_INT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'BYTE',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'INT',
        Documentation: '',
      }
    },
    {
      Name: 'BYTE_TO_DINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'BYTE',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'DINT',
        Documentation: '',
      }
    },
    {
      Name: 'BYTE_TO_LINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'BYTE',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'LINT',
        Documentation: '',
      }
    },
    {
      Name: 'BYTE_TO_USINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'BYTE',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'USINT',
        Documentation: '',
      }
    },
    {
      Name: 'BYTE_TO_UINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'BYTE',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'UINT',
        Documentation: '',
      }
    },
    {
      Name: 'BYTE_TO_UDINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'BYTE',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'UDINT',
        Documentation: '',
      }
    },
    {
      Name: 'BYTE_TO_ULINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'BYTE',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'ULINT',
        Documentation: '',
      }
    },
    {
      Name: 'BYTE_TO_WORD',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'BYTE',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'WORD',
        Documentation: '',
      }
    },
    {
      Name: 'BYTE_TO_DWORD',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'BYTE',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'DWORD',
        Documentation: '',
      }
    },
    {
      Name: 'BYTE_TO_LWORD',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'BYTE',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'LWORD',
        Documentation: '',
      }
    },
    {
      Name: 'BYTE_TO_REAL',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'BYTE',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'REAL',
        Documentation: '',
      }
    },
    {
      Name: 'BYTE_TO_LREAL',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'BYTE',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'LREAL',
        Documentation: '',
      }
    },
    {
      Name: 'BYTE_TO_STRING',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'BYTE',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'STRING',
        Documentation: '',
      }
    },
    {
      Name: 'BYTE_TO_DATE',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'BYTE',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'DATE',
        Documentation: '',
      }
    },
    {
      Name: 'BYTE_TO_TOD',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'BYTE',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'TOD',
        Documentation: '',
      }
    },
    {
      Name: 'BYTE_TO_DT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'BYTE',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'DT',
        Documentation: '',
      }
    },
    {
      Name: 'BYTE_TO_TIME',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'BYTE',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'TIME',
        Documentation: '',
      }
    },
    {
      Name: 'WORD_TO_BOOL',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'WORD',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'BOOL',
        Documentation: '',
      }
    },
    {
      Name: 'WORD_TO_SINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'WORD',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'SINT',
        Documentation: '',
      }
    },
    {
      Name: 'WORD_TO_INT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'WORD',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'INT',
        Documentation: '',
      }
    },
    {
      Name: 'WORD_TO_DINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'WORD',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'DINT',
        Documentation: '',
      }
    },
    {
      Name: 'WORD_TO_LINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'WORD',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'LINT',
        Documentation: '',
      }
    },
    {
      Name: 'WORD_TO_USINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'WORD',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'USINT',
        Documentation: '',
      }
    },
    {
      Name: 'WORD_TO_UINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'WORD',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'UINT',
        Documentation: '',
      }
    },
    {
      Name: 'WORD_TO_UDINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'WORD',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'UDINT',
        Documentation: '',
      }
    },
    {
      Name: 'WORD_TO_ULINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'WORD',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'ULINT',
        Documentation: '',
      }
    },
    {
      Name: 'WORD_TO_BYTE',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'WORD',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'BYTE',
        Documentation: '',
      }
    },
    {
      Name: 'WORD_TO_DWORD',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'WORD',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'DWORD',
        Documentation: '',
      }
    },
    {
      Name: 'WORD_TO_LWORD',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'WORD',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'LWORD',
        Documentation: '',
      }
    },
    {
      Name: 'WORD_TO_REAL',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'WORD',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'REAL',
        Documentation: '',
      }
    },
    {
      Name: 'WORD_TO_LREAL',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'WORD',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'LREAL',
        Documentation: '',
      }
    },
    {
      Name: 'WORD_TO_STRING',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'WORD',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'STRING',
        Documentation: '',
      }
    },
    {
      Name: 'WORD_TO_DATE',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'WORD',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'DATE',
        Documentation: '',
      }
    },
    {
      Name: 'WORD_TO_TOD',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'WORD',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'TOD',
        Documentation: '',
      }
    },
    {
      Name: 'WORD_TO_DT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'WORD',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'DT',
        Documentation: '',
      }
    },
    {
      Name: 'WORD_TO_TIME',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'WORD',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'TIME',
        Documentation: '',
      }
    },
    {
      Name: 'DWORD_TO_BOOL',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'DWORD',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'BOOL',
        Documentation: '',
      }
    },
    {
      Name: 'DWORD_TO_SINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'DWORD',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'SINT',
        Documentation: '',
      }
    },
    {
      Name: 'DWORD_TO_INT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'DWORD',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'INT',
        Documentation: '',
      }
    },
    {
      Name: 'DWORD_TO_DINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'DWORD',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'DINT',
        Documentation: '',
      }
    },
    {
      Name: 'DWORD_TO_LINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'DWORD',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'LINT',
        Documentation: '',
      }
    },
    {
      Name: 'DWORD_TO_USINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'DWORD',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'USINT',
        Documentation: '',
      }
    },
    {
      Name: 'DWORD_TO_UINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'DWORD',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'UINT',
        Documentation: '',
      }
    },
    {
      Name: 'DWORD_TO_UDINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'DWORD',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'UDINT',
        Documentation: '',
      }
    },
    {
      Name: 'DWORD_TO_ULINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'DWORD',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'ULINT',
        Documentation: '',
      }
    },
    {
      Name: 'DWORD_TO_BYTE',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'DWORD',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'BYTE',
        Documentation: '',
      }
    },
    {
      Name: 'DWORD_TO_WORD',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'DWORD',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'WORD',
        Documentation: '',
      }
    },
    {
      Name: 'DWORD_TO_LWORD',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'DWORD',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'LWORD',
        Documentation: '',
      }
    },
    {
      Name: 'DWORD_TO_REAL',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'DWORD',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'REAL',
        Documentation: '',
      }
    },
    {
      Name: 'DWORD_TO_LREAL',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'DWORD',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'LREAL',
        Documentation: '',
      }
    },
    {
      Name: 'DWORD_TO_STRING',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'DWORD',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'STRING',
        Documentation: '',
      }
    },
    {
      Name: 'DWORD_TO_DATE',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'DWORD',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'DATE',
        Documentation: '',
      }
    },
    {
      Name: 'DWORD_TO_TOD',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'DWORD',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'TOD',
        Documentation: '',
      }
    },
    {
      Name: 'DWORD_TO_DT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'DWORD',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'DT',
        Documentation: '',
      }
    },
    {
      Name: 'DWORD_TO_TIME',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'DWORD',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'TIME',
        Documentation: '',
      }
    },
    {
      Name: 'LWORD_TO_BOOL',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'LWORD',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'BOOL',
        Documentation: '',
      }
    },
    {
      Name: 'LWORD_TO_SINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'LWORD',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'SINT',
        Documentation: '',
      }
    },
    {
      Name: 'LWORD_TO_INT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'LWORD',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'INT',
        Documentation: '',
      }
    },
    {
      Name: 'LWORD_TO_DINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'LWORD',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'DINT',
        Documentation: '',
      }
    },
    {
      Name: 'LWORD_TO_LINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'LWORD',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'LINT',
        Documentation: '',
      }
    },
    {
      Name: 'LWORD_TO_USINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'LWORD',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'USINT',
        Documentation: '',
      }
    },
    {
      Name: 'LWORD_TO_UINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'LWORD',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'UINT',
        Documentation: '',
      }
    },
    {
      Name: 'LWORD_TO_UDINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'LWORD',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'UDINT',
        Documentation: '',
      }
    },
    {
      Name: 'LWORD_TO_ULINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'LWORD',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'ULINT',
        Documentation: '',
      }
    },
    {
      Name: 'LWORD_TO_BYTE',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'LWORD',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'BYTE',
        Documentation: '',
      }
    },
    {
      Name: 'LWORD_TO_WORD',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'LWORD',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'WORD',
        Documentation: '',
      }
    },
    {
      Name: 'LWORD_TO_DWORD',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'LWORD',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'DWORD',
        Documentation: '',
      }
    },
    {
      Name: 'LWORD_TO_REAL',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'LWORD',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'REAL',
        Documentation: '',
      }
    },
    {
      Name: 'LWORD_TO_LREAL',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'LWORD',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'LREAL',
        Documentation: '',
      }
    },
    {
      Name: 'LWORD_TO_STRING',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'LWORD',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'STRING',
        Documentation: '',
      }
    },
    {
      Name: 'LWORD_TO_DATE',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'LWORD',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'DATE',
        Documentation: '',
      }
    },
    {
      Name: 'LWORD_TO_TOD',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'LWORD',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'TOD',
        Documentation: '',
      }
    },
    {
      Name: 'LWORD_TO_DT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'LWORD',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'DT',
        Documentation: '',
      }
    },
    {
      Name: 'LWORD_TO_TIME',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'LWORD',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'TIME',
        Documentation: '',
      }
    },
    {
      Name: 'REAL_TO_SINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'REAL',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'SINT',
        Documentation: '',
      }
    },
    {
      Name: 'REAL_TO_INT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'REAL',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'INT',
        Documentation: '',
      }
    },
    {
      Name: 'REAL_TO_DINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'REAL',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'DINT',
        Documentation: '',
      }
    },
    {
      Name: 'REAL_TO_LINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'REAL',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'LINT',
        Documentation: '',
      }
    },
    {
      Name: 'REAL_TO_USINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'REAL',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'USINT',
        Documentation: '',
      }
    },
    {
      Name: 'REAL_TO_UINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'REAL',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'UINT',
        Documentation: '',
      }
    },
    {
      Name: 'REAL_TO_UDINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'REAL',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'UDINT',
        Documentation: '',
      }
    },
    {
      Name: 'REAL_TO_ULINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'REAL',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'ULINT',
        Documentation: '',
      }
    },
    {
      Name: 'REAL_TO_BYTE',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'REAL',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'BYTE',
        Documentation: '',
      }
    },
    {
      Name: 'REAL_TO_WORD',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'REAL',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'WORD',
        Documentation: '',
      }
    },
    {
      Name: 'REAL_TO_DWORD',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'REAL',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'DWORD',
        Documentation: '',
      }
    },
    {
      Name: 'REAL_TO_LWORD',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'REAL',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'LWORD',
        Documentation: '',
      }
    },
    {
      Name: 'REAL_TO_LREAL',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'REAL',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'LREAL',
        Documentation: '',
      }
    },
    {
      Name: 'REAL_TO_STRING',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'REAL',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'STRING',
        Documentation: '',
      }
    },
    {
      Name: 'REAL_TO_DATE',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'REAL',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'DATE',
        Documentation: '',
      }
    },
    {
      Name: 'REAL_TO_TOD',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'REAL',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'TOD',
        Documentation: '',
      }
    },
    {
      Name: 'REAL_TO_DT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'REAL',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'DT',
        Documentation: '',
      }
    },
    {
      Name: 'REAL_TO_TIME',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'REAL',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'TIME',
        Documentation: '',
      }
    },
    {
      Name: 'LREAL_TO_BOOL',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'LREAL',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'BOOL',
        Documentation: '',
      }
    },
    {
      Name: 'LREAL_TO_SINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'LREAL',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'SINT',
        Documentation: '',
      }
    },
    {
      Name: 'LREAL_TO_INT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'LREAL',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'INT',
        Documentation: '',
      }
    },
    {
      Name: 'LREAL_TO_DINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'LREAL',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'DINT',
        Documentation: '',
      }
    },
    {
      Name: 'LREAL_TO_LINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'LREAL',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'LINT',
        Documentation: '',
      }
    },
    {
      Name: 'LREAL_TO_USINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'LREAL',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'USINT',
        Documentation: '',
      }
    },
    {
      Name: 'LREAL_TO_UINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'LREAL',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'UINT',
        Documentation: '',
      }
    },
    {
      Name: 'LREAL_TO_UDINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'LREAL',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'UDINT',
        Documentation: '',
      }
    },
    {
      Name: 'LREAL_TO_ULINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'LREAL',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'ULINT',
        Documentation: '',
      }
    },
    {
      Name: 'LREAL_TO_BYTE',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'LREAL',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'BYTE',
        Documentation: '',
      }
    },
    {
      Name: 'LREAL_TO_WORD',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'LREAL',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'WORD',
        Documentation: '',
      }
    },
    {
      Name: 'LREAL_TO_DWORD',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'LREAL',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'DWORD',
        Documentation: '',
      }
    },
    {
      Name: 'LREAL_TO_LWORD',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'LREAL',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'LWORD',
        Documentation: '',
      }
    },
    {
      Name: 'LREAL_TO_REAL',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'LREAL',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'REAL',
        Documentation: '',
      }
    },
    {
      Name: 'LREAL_TO_STRING',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'LREAL',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'STRING',
        Documentation: '',
      }
    },
    {
      Name: 'LREAL_TO_DATE',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'LREAL',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'DATE',
        Documentation: '',
      }
    },
    {
      Name: 'LREAL_TO_TOD',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'LREAL',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'TOD',
        Documentation: '',
      }
    },
    {
      Name: 'LREAL_TO_DT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'LREAL',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'DT',
        Documentation: '',
      }
    },
    {
      Name: 'LREAL_TO_TIME',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'LREAL',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'TIME',
        Documentation: '',
      }
    },
    {
      Name: 'STRING_TO_BOOL',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'STRING',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'BOOL',
        Documentation: '',
      }
    },
    {
      Name: 'STRING_TO_SINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'STRING',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'SINT',
        Documentation: '',
      }
    },
    {
      Name: 'STRING_TO_INT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'STRING',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'INT',
        Documentation: '',
      }
    },
    {
      Name: 'STRING_TO_DINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'STRING',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'DINT',
        Documentation: '',
      }
    },
    {
      Name: 'STRING_TO_LINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'STRING',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'LINT',
        Documentation: '',
      }
    },
    {
      Name: 'STRING_TO_USINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'STRING',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'USINT',
        Documentation: '',
      }
    },
    {
      Name: 'STRING_TO_UINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'STRING',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'UINT',
        Documentation: '',
      }
    },
    {
      Name: 'STRING_TO_UDINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'STRING',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'UDINT',
        Documentation: '',
      }
    },
    {
      Name: 'STRING_TO_ULINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'STRING',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'ULINT',
        Documentation: '',
      }
    },
    {
      Name: 'STRING_TO_BYTE',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'STRING',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'BYTE',
        Documentation: '',
      }
    },
    {
      Name: 'STRING_TO_WORD',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'STRING',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'WORD',
        Documentation: '',
      }
    },
    {
      Name: 'STRING_TO_DWORD',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'STRING',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'DWORD',
        Documentation: '',
      }
    },
    {
      Name: 'STRING_TO_LWORD',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'STRING',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'LWORD',
        Documentation: '',
      }
    },
    {
      Name: 'STRING_TO_REAL',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'STRING',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'REAL',
        Documentation: '',
      }
    },
    {
      Name: 'STRING_TO_LREAL',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'STRING',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'LREAL',
        Documentation: '',
      }
    },
    {
      Name: 'STRING_TO_DATE',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'STRING',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'DATE',
        Documentation: '',
      }
    },
    {
      Name: 'STRING_TO_TOD',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'STRING',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'TOD',
        Documentation: '',
      }
    },
    {
      Name: 'STRING_TO_DT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'STRING',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'DT',
        Documentation: '',
      }
    },
    {
      Name: 'STRING_TO_TIME',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'STRING',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'TIME',
        Documentation: '',
      }
    },
    {
      Name: 'DATE_TO_BOOL',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'DATE',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'BOOL',
        Documentation: '',
      }
    },
    {
      Name: 'DATE_TO_SINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'DATE',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'SINT',
        Documentation: '',
      }
    },
    {
      Name: 'DATE_TO_INT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'DATE',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'INT',
        Documentation: '',
      }
    },
    {
      Name: 'DATE_TO_DINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'DATE',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'DINT',
        Documentation: '',
      }
    },
    {
      Name: 'DATE_TO_LINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'DATE',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'LINT',
        Documentation: '',
      }
    },
    {
      Name: 'DATE_TO_USINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'DATE',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'USINT',
        Documentation: '',
      }
    },
    {
      Name: 'DATE_TO_UINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'DATE',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'UINT',
        Documentation: '',
      }
    },
    {
      Name: 'DATE_TO_UDINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'DATE',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'UDINT',
        Documentation: '',
      }
    },
    {
      Name: 'DATE_TO_ULINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'DATE',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'ULINT',
        Documentation: '',
      }
    },
    {
      Name: 'DATE_TO_BYTE',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'DATE',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'BYTE',
        Documentation: '',
      }
    },
    {
      Name: 'DATE_TO_WORD',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'DATE',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'WORD',
        Documentation: '',
      }
    },
    {
      Name: 'DATE_TO_DWORD',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'DATE',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'DWORD',
        Documentation: '',
      }
    },
    {
      Name: 'DATE_TO_LWORD',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'DATE',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'LWORD',
        Documentation: '',
      }
    },
    {
      Name: 'DATE_TO_REAL',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'DATE',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'REAL',
        Documentation: '',
      }
    },
    {
      Name: 'DATE_TO_LREAL',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'DATE',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'LREAL',
        Documentation: '',
      }
    },
    {
      Name: 'DATE_TO_STRING',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'DATE',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'STRING',
        Documentation: '',
      }
    },
    {
      Name: 'DATE_TO_TOD',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'DATE',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'TOD',
        Documentation: '',
      }
    },
    {
      Name: 'DATE_TO_DT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'DATE',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'DT',
        Documentation: '',
      }
    },
    {
      Name: 'DATE_TO_TIME',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'DATE',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'TIME',
        Documentation: '',
      }
    },
    {
      Name: 'TOD_TO_BOOL',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'TOD',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'BOOL',
        Documentation: '',
      }
    },
    {
      Name: 'TOD_TO_SINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'TOD',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'SINT',
        Documentation: '',
      }
    },
    {
      Name: 'TOD_TO_INT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'TOD',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'INT',
        Documentation: '',
      }
    },
    {
      Name: 'TOD_TO_DINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'TOD',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'DINT',
        Documentation: '',
      }
    },
    {
      Name: 'TOD_TO_LINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'TOD',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'LINT',
        Documentation: '',
      }
    },
    {
      Name: 'TOD_TO_USINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'TOD',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'USINT',
        Documentation: '',
      }
    },
    {
      Name: 'TOD_TO_UINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'TOD',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'UINT',
        Documentation: '',
      }
    },
    {
      Name: 'TOD_TO_UDINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'TOD',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'UDINT',
        Documentation: '',
      }
    },
    {
      Name: 'TOD_TO_ULINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'TOD',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'ULINT',
        Documentation: '',
      }
    },
    {
      Name: 'TOD_TO_BYTE',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'TOD',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'BYTE',
        Documentation: '',
      }
    },
    {
      Name: 'TOD_TO_WORD',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'TOD',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'WORD',
        Documentation: '',
      }
    },
    {
      Name: 'TOD_TO_DWORD',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'TOD',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'DWORD',
        Documentation: '',
      }
    },
    {
      Name: 'TOD_TO_LWORD',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'TOD',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'LWORD',
        Documentation: '',
      }
    },
    {
      Name: 'TOD_TO_REAL',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'TOD',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'REAL',
        Documentation: '',
      }
    },
    {
      Name: 'TOD_TO_LREAL',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'TOD',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'LREAL',
        Documentation: '',
      }
    },
    {
      Name: 'TOD_TO_STRING',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'TOD',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'STRING',
        Documentation: '',
      }
    },
    {
      Name: 'TOD_TO_DATE',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'TOD',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'DATE',
        Documentation: '',
      }
    },
    {
      Name: 'TOD_TO_DT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'TOD',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'DT',
        Documentation: '',
      }
    },
    {
      Name: 'TOD_TO_TIME',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'TOD',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'TIME',
        Documentation: '',
      }
    },
    {
      Name: 'DT_TO_BOOL',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'DT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'BOOL',
        Documentation: '',
      }
    },
    {
      Name: 'DT_TO_SINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'DT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'SINT',
        Documentation: '',
      }
    },
    {
      Name: 'DT_TO_INT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'DT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'INT',
        Documentation: '',
      }
    },
    {
      Name: 'DT_TO_DINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'DT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'DINT',
        Documentation: '',
      }
    },
    {
      Name: 'DT_TO_LINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'DT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'LINT',
        Documentation: '',
      }
    },
    {
      Name: 'DT_TO_USINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'DT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'USINT',
        Documentation: '',
      }
    },
    {
      Name: 'DT_TO_UINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'DT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'UINT',
        Documentation: '',
      }
    },
    {
      Name: 'DT_TO_UDINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'DT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'UDINT',
        Documentation: '',
      }
    },
    {
      Name: 'DT_TO_ULINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'DT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'ULINT',
        Documentation: '',
      }
    },
    {
      Name: 'DT_TO_BYTE',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'DT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'BYTE',
        Documentation: '',
      }
    },
    {
      Name: 'DT_TO_WORD',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'DT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'WORD',
        Documentation: '',
      }
    },
    {
      Name: 'DT_TO_DWORD',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'DT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'DWORD',
        Documentation: '',
      }
    },
    {
      Name: 'DT_TO_LWORD',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'DT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'LWORD',
        Documentation: '',
      }
    },
    {
      Name: 'DT_TO_REAL',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'DT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'REAL',
        Documentation: '',
      }
    },
    {
      Name: 'DT_TO_LREAL',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'DT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'LREAL',
        Documentation: '',
      }
    },
    {
      Name: 'DT_TO_STRING',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'DT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'STRING',
        Documentation: '',
      }
    },
    {
      Name: 'DT_TO_DATE',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'DT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'DATE',
        Documentation: '',
      }
    },
    {
      Name: 'DT_TO_TOD',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'DT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'TOD',
        Documentation: '',
      }
    },
    {
      Name: 'DT_TO_TIME',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'DT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'TIME',
        Documentation: '',
      }
    },
    {
      Name: 'TIME_TO_BOOL',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'TIME',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'BOOL',
        Documentation: '',
      }
    },
    {
      Name: 'TIME_TO_SINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'TIME',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'SINT',
        Documentation: '',
      }
    },
    {
      Name: 'TIME_TO_INT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'TIME',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'INT',
        Documentation: '',
      }
    },
    {
      Name: 'TIME_TO_DINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'TIME',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'DINT',
        Documentation: '',
      }
    },
    {
      Name: 'TIME_TO_LINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'TIME',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'LINT',
        Documentation: '',
      }
    },
    {
      Name: 'TIME_TO_USINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'TIME',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'USINT',
        Documentation: '',
      }
    },
    {
      Name: 'TIME_TO_UINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'TIME',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'UINT',
        Documentation: '',
      }
    },
    {
      Name: 'TIME_TO_UDINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'TIME',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'UDINT',
        Documentation: '',
      }
    },
    {
      Name: 'TIME_TO_ULINT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'TIME',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'ULINT',
        Documentation: '',
      }
    },
    {
      Name: 'TIME_TO_BYTE',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'TIME',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'BYTE',
        Documentation: '',
      }
    },
    {
      Name: 'TIME_TO_WORD',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'TIME',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'WORD',
        Documentation: '',
      }
    },
    {
      Name: 'TIME_TO_DWORD',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'TIME',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'DWORD',
        Documentation: '',
      }
    },
    {
      Name: 'TIME_TO_LWORD',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'TIME',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'LWORD',
        Documentation: '',
      }
    },
    {
      Name: 'TIME_TO_REAL',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'TIME',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'REAL',
        Documentation: '',
      }
    },
    {
      Name: 'TIME_TO_LREAL',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'TIME',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'LREAL',
        Documentation: '',
      }
    },
    {
      Name: 'TIME_TO_STRING',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'TIME',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'STRING',
        Documentation: '',
      }
    },
    {
      Name: 'TIME_TO_DATE',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'TIME',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'DATE',
        Documentation: '',
      }
    },
    {
      Name: 'TIME_TO_TOD',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'TIME',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'TOD',
        Documentation: '',
      }
    },
    {
      Name: 'TIME_TO_DT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'TIME',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'DT',
        Documentation: '',
      }
    },
  ],
  Math: [
    {
      Name: 'ADD',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN1',
          DataType: 'ANY_NUM',
          Class: 'Input',
          Documentation: '',
        },
        {
          Name: 'IN2',
          DataType: 'ANY_NUM',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'ANY_NUM',
        Documentation: '',
      }
    },
    {
      Name: 'MUL',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN1',
          DataType: 'ANY_NUM',
          Class: 'Input',
          Documentation: '',
        },
        {
          Name: 'IN2',
          DataType: 'ANY_NUM',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'ANY_NUM',
        Documentation: '',
      }
    },
    {
      Name: 'SUB',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN1',
          DataType: 'ANY_NUM',
          Class: 'Input',
          Documentation: '',
        },
        {
          Name: 'IN2',
          DataType: 'ANY_NUM',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'ANY_NUM',
        Documentation: '',
      }
    },
    {
      Name: 'DIV',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN1',
          DataType: 'ANY_NUM',
          Class: 'Input',
          Documentation: '',
        },
        {
          Name: 'IN2',
          DataType: 'ANY_NUM',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'ANY_NUM',
        Documentation: '',
      }
    },
    {
      Name: 'MOD',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN1',
          DataType: 'ANY_INT',
          Class: 'Input',
          Documentation: '',
        },
        {
          Name: 'IN2',
          DataType: 'ANY_INT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'ANY_INT',
        Documentation: '',
      }
    },
    {
      Name: 'EXPT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN1',
          DataType: 'ANY_REAL',
          Class: 'Input',
          Documentation: '',
        },
        {
          Name: 'IN2',
          DataType: 'ANY_REAL',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'ANY_REAL',
        Documentation: '',
      }
    },
    {
      Name: 'MOVE',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'ANY',
          Class: 'Input',
          Documentation: '',
        },
      ],
      Return: {
        Name: 'OUT',
        DataType: 'ANY',
        Documentation: '',
      }
    },
    {
      Name: 'ABS',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'ANY_NUM',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'ANY_NUM',
        Documentation: '',
      }
    },
    {
      Name: 'SQRT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'ANY_REAL',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'ANY_REAL',
        Documentation: '',
      }
    },
    {
      Name: 'LN',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'ANY_REAL',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'ANY_REAL',
        Documentation: '',
      }
    },
    {
      Name: 'LOG',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'ANY_REAL',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'ANY_REAL',
        Documentation: '',
      }
    },
    {
      Name: 'EXP',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'ANY_REAL',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'ANY_REAL',
        Documentation: '',
      }
    },
    {
      Name: 'SIN',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'ANY_REAL',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'ANY_REAL',
        Documentation: '',
      }
    },
    {
      Name: 'COS',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'ANY_REAL',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'ANY_REAL',
        Documentation: '',
      }
    },
    {
      Name: 'TAN',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'ANY_REAL',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'ANY_REAL',
        Documentation: '',
      }
    },
    {
      Name: 'ASIN',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'ANY_REAL',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'ANY_REAL',
        Documentation: '',
      }
    },
    {
      Name: 'ACOS',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'ANY_REAL',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'ANY_REAL',
        Documentation: '',
      }
    },
    {
      Name: 'ATAN',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'ANY_REAL',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'ANY_REAL',
        Documentation: '',
      }
    },
    {
      Name: 'INTEGRAL',
      Language: 'ST',
      POUType: 'FunctionBlock',
      Variables: [
        {
          Name: 'RUN',
          DataType: 'BOOL',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'R1',
          DataType: 'BOOL',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'XIN',
          DataType: 'REAL',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'X0',
          DataType: 'REAL',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'CYCLE',
          DataType: 'TIME',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'Q',
          DataType: 'BOOL',
          Class: 'Output',
          InitialValue: 'FALSE',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'XOUT',
          DataType: 'REAL',
          Class: 'Output',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
      ],
      Code: 'Q := NOT R1;\nIF R1 THEN XOUT := X0;ELSIF RUN THEN XOUT := XOUT + XIN * TIME_TO_REAL(CYCLE);\nEND_IF;'
    },
    {
      Name: 'DERIVATIVE',
      Language: 'ST',
      POUType: 'FunctionBlock',
      Variables: [
        {
          Name: 'RUN',
          DataType: 'BOOL',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'XIN',
          DataType: 'REAL',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'CYCLE',
          DataType: 'TIME',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'XOUT',
          DataType: 'REAL',
          Class: 'Output',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'X1',
          DataType: 'REAL',
          Class: 'Local',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'X2',
          DataType: 'REAL',
          Class: 'Local',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'X3',
          DataType: 'REAL',
          Class: 'Local',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
      ],
      Code: 'IF RUN THEN\n  XOUT := (3.0 * (XIN - X3) + X1 - X2)\n          / (10.0 * TIME_TO_REAL(CYCLE));\n  X3 := X2;\n  X2 := X1;\n  X1 := XIN;\nELSE\n  XOUT := 0.0;\n  X1 := XIN;\n  X2 := XIN;\n  X3 := XIN;\nEND_IF;'
    },
  ],
  Time: [
    {
      Name: 'TP',
      Language: 'ST',
      POUType: 'FunctionBlock',
      Variables: [
        {
          Name: 'IN',
          DataType: 'BOOL',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'PT',
          DataType: 'TIME',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'Q',
          DataType: 'BOOL',
          Class: 'Output',
          InitialValue: 'FALSE',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'ET',
          DataType: 'TIME',
          Class: 'Output',
          InitialValue: 'T#0S',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'STATE',
          DataType: 'SINT',
          Class: 'Local',
          InitialValue: '0',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'PREV_IN',
          DataType: 'BOOL',
          Class: 'Local',
          InitialValue: 'FALSE',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'CURRENT_TIME',
          DataType: 'TIME',
          Class: 'Local',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'START_TIME',
          DataType: 'TIME',
          Class: 'Local',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
      ],
      Code: '{__SET_VAR(data__->,CURRENT_TIME,,__CURRENT_TIME)}\n\nIF ((STATE = 0) AND NOT(PREV_IN) AND IN)   (* found rising edge on IN *)\nTHEN\n  (* start timer... *)\n  STATE := 1;\n  Q := TRUE;\n  START_TIME := CURRENT_TIME;\n\nELSIF (STATE = 1)\nTHEN\n  IF ((START_TIME + PT) <= CURRENT_TIME)\n  THEN\n    STATE := 2;\n    Q := FALSE;\n    ET := PT;\n  ELSE\n    ET := CURRENT_TIME - START_TIME;\n  END_IF;\nEND_IF;\n\nIF ((STATE = 2) AND NOT(IN))\nTHEN\n  ET := T#0s;\n  STATE := 0;\nEND_IF;\n\nPREV_IN := IN;'
    },
    {
      Name: 'TON',
      Language: 'ST',
      POUType: 'FunctionBlock',
      Variables: [
        {
          Name: 'IN',
          DataType: 'BOOL',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'PT',
          DataType: 'TIME',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'Q',
          DataType: 'BOOL',
          Class: 'Output',
          InitialValue: 'FALSE',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'ET',
          DataType: 'TIME',
          Class: 'Output',
          InitialValue: 'T#0S',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'STATE',
          DataType: 'SINT',
          Class: 'Local',
          InitialValue: '0',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'PREV_IN',
          DataType: 'BOOL',
          Class: 'Local',
          InitialValue: 'FALSE',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'CURRENT_TIME',
          DataType: 'TIME',
          Class: 'Local',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'START_TIME',
          DataType: 'TIME',
          Class: 'Local',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
      ],
      Code: '{__SET_VAR(data__->,CURRENT_TIME,,__CURRENT_TIME)}\n\nIF ((STATE = 0) AND NOT(PREV_IN) AND IN)   (* found rising edge on IN *)\nTHEN\n  (* start timer... *)\n  STATE := 1;\n  Q := FALSE;\n  START_TIME := CURRENT_TIME;\n\nELSE\n  (* STATE is 1 or 2 !! *)\n  IF (NOT(IN))\n  THEN\n    ET := T#0s;\n    Q := FALSE;\n    STATE := 0;\n\n  ELSIF (STATE = 1)\n  THEN\n    IF ((START_TIME + PT) <= CURRENT_TIME)\n    THEN\n      STATE := 2;\n      Q := TRUE;\n      ET := PT;\n    ELSE\n      ET := CURRENT_TIME - START_TIME;\n    END_IF;\n  END_IF;\n\nEND_IF;\n\nPREV_IN := IN;'
    },
    {
      Name: 'TOF',
      Language: 'ST',
      POUType: 'FunctionBlock',
      Variables: [
        {
          Name: 'IN',
          DataType: 'BOOL',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'PT',
          DataType: 'TIME',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'Q',
          DataType: 'BOOL',
          Class: 'Output',
          InitialValue: 'FALSE',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'ET',
          DataType: 'TIME',
          Class: 'Output',
          InitialValue: 'T#0S',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'STATE',
          DataType: 'SINT',
          Class: 'Local',
          InitialValue: '0',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'PREV_IN',
          DataType: 'BOOL',
          Class: 'Local',
          InitialValue: 'FALSE',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'CURRENT_TIME',
          DataType: 'TIME',
          Class: 'Local',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'START_TIME',
          DataType: 'TIME',
          Class: 'Local',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
      ],
      Code: '{__SET_VAR(data__->,CURRENT_TIME,,__CURRENT_TIME)}\n\nIF ((STATE = 0) AND PREV_IN AND NOT(IN))   (* found falling edge on IN *)\nTHEN\n  (* start timer... *)\n  STATE := 1;\n  START_TIME := CURRENT_TIME;\n\nELSE\n  (* STATE is 1 or 2 !! *)\n  IF (IN)\n  THEN\n    ET := T#0s;\n    STATE := 0;\n\n  ELSIF (STATE = 1)\n  THEN\n    IF ((START_TIME + PT) <= CURRENT_TIME)\n    THEN\n      STATE := 2;\n      ET := PT;\n    ELSE\n      ET := CURRENT_TIME - START_TIME;\n    END_IF;\n  END_IF;\n\nEND_IF;\n\nQ := IN OR (STATE = 1);\nPREV_IN := IN;'
    },
  ],
  Counter: [
    {
      Name: 'CTU',
      Language: 'ST',
      POUType: 'FunctionBlock',
      Variables: [
        {
          Name: 'CU',
          DataType: 'BOOL',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'R',
          DataType: 'BOOL',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'PV',
          DataType: 'INT',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'Q',
          DataType: 'BOOL',
          Class: 'Output',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'CV',
          DataType: 'INT',
          Class: 'Output',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'CU_T',
          DataType: 'R_TRIG',
          Class: 'Local',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
      ],
      Code: 'CU_T(CU);\nIF R THEN CV := 0;\nELSIF CU_T.Q AND (CV < PV)\n  THEN CV := CV+1;\nEND_IF;\nQ := (CV >= PV);'
    },
    {
      Name: 'CTU_DINT',
      Language: 'ST',
      POUType: 'FunctionBlock',
      Variables: [
        {
          Name: 'CU',
          DataType: 'BOOL',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'R',
          DataType: 'BOOL',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'PV',
          DataType: 'DINT',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'Q',
          DataType: 'BOOL',
          Class: 'Output',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'CV',
          DataType: 'DINT',
          Class: 'Output',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'CU_T',
          DataType: 'R_TRIG',
          Class: 'Local',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
      ],
      Code: 'CU_T(CU);\nIF R THEN CV := 0;\nELSIF CU_T.Q AND (CV < PV)\n  THEN CV := CV+1;\nEND_IF;\nQ := (CV >= PV);'
    },
    {
      Name: 'CTU_LINT',
      Language: 'ST',
      POUType: 'FunctionBlock',
      Variables: [
        {
          Name: 'CU',
          DataType: 'BOOL',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'R',
          DataType: 'BOOL',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'PV',
          DataType: 'LINT',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'Q',
          DataType: 'BOOL',
          Class: 'Output',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'CV',
          DataType: 'LINT',
          Class: 'Output',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'CU_T',
          DataType: 'R_TRIG',
          Class: 'Local',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
      ],
      Code: 'CU_T(CU);\nIF R THEN CV := 0;\nELSIF CU_T.Q AND (CV < PV)\n  THEN CV := CV+1;\nEND_IF;\nQ := (CV >= PV);'
    },
    {
      Name: 'CTU_UDINT',
      Language: 'ST',
      POUType: 'FunctionBlock',
      Variables: [
        {
          Name: 'CU',
          DataType: 'BOOL',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'R',
          DataType: 'BOOL',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'PV',
          DataType: 'UDINT',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'Q',
          DataType: 'BOOL',
          Class: 'Output',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'CV',
          DataType: 'UDINT',
          Class: 'Output',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'CU_T',
          DataType: 'R_TRIG',
          Class: 'Local',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
      ],
      Code: 'CU_T(CU);\nIF R THEN CV := 0;\nELSIF CU_T.Q AND (CV < PV)\n  THEN CV := CV+1;\nEND_IF;\nQ := (CV >= PV);'
    },
    {
      Name: 'CTU_ULINT',
      Language: 'ST',
      POUType: 'FunctionBlock',
      Variables: [
        {
          Name: 'CU',
          DataType: 'BOOL',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'R',
          DataType: 'BOOL',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'PV',
          DataType: 'ULINT',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'Q',
          DataType: 'BOOL',
          Class: 'Output',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'CV',
          DataType: 'ULINT',
          Class: 'Output',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'CU_T',
          DataType: 'R_TRIG',
          Class: 'Local',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
      ],
      Code: 'CU_T(CU);\nIF R THEN CV := 0;\nELSIF CU_T.Q AND (CV < PV)\n  THEN CV := CV+1;\nEND_IF;\nQ := (CV >= PV);'
    },
    {
      Name: 'CTD',
      Language: 'ST',
      POUType: 'FunctionBlock',
      Variables: [
        {
          Name: 'CD',
          DataType: 'BOOL',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'LD',
          DataType: 'BOOL',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'PV',
          DataType: 'INT',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'Q',
          DataType: 'BOOL',
          Class: 'Output',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'CV',
          DataType: 'INT',
          Class: 'Output',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'CD_T',
          DataType: 'R_TRIG',
          Class: 'Local',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
      ],
      Code: 'CD_T(CD);\nIF LD THEN CV := PV;\nELSIF CD_T.Q AND (CV > 0)\n  THEN CV := CV-1;\nEND_IF;\nQ := (CV <= 0);'
    },
    {
      Name: 'CTD_DINT',
      Language: 'ST',
      POUType: 'FunctionBlock',
      Variables: [
        {
          Name: 'CD',
          DataType: 'BOOL',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'LD',
          DataType: 'BOOL',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'PV',
          DataType: 'DINT',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'Q',
          DataType: 'BOOL',
          Class: 'Output',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'CV',
          DataType: 'DINT',
          Class: 'Output',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'CD_T',
          DataType: 'R_TRIG',
          Class: 'Local',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
      ],
      Code: 'CD_T(CD);\nIF LD THEN CV := PV;\nELSIF CD_T.Q AND (CV > 0)\n  THEN CV := CV-1;\nEND_IF;\nQ := (CV <= 0);'
    },
    {
      Name: 'CTD_LINT',
      Language: 'ST',
      POUType: 'FunctionBlock',
      Variables: [
        {
          Name: 'CD',
          DataType: 'BOOL',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'LD',
          DataType: 'BOOL',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'PV',
          DataType: 'LINT',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'Q',
          DataType: 'BOOL',
          Class: 'Output',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'CV',
          DataType: 'LINT',
          Class: 'Output',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'CD_T',
          DataType: 'R_TRIG',
          Class: 'Local',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
      ],
      Code: 'CD_T(CD);\nIF LD THEN CV := PV;\nELSIF CD_T.Q AND (CV > 0)\n  THEN CV := CV-1;\nEND_IF;\nQ := (CV <= 0);'
    },
    {
      Name: 'CTD_UDINT',
      Language: 'ST',
      POUType: 'FunctionBlock',
      Variables: [
        {
          Name: 'CD',
          DataType: 'BOOL',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'LD',
          DataType: 'BOOL',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'PV',
          DataType: 'UDINT',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'Q',
          DataType: 'BOOL',
          Class: 'Output',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'CV',
          DataType: 'UDINT',
          Class: 'Output',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'CD_T',
          DataType: 'R_TRIG',
          Class: 'Local',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
      ],
      Code: 'CD_T(CD);\nIF LD THEN CV := PV;\nELSIF CD_T.Q AND (CV > 0)\n  THEN CV := CV-1;\nEND_IF;\nQ := (CV <= 0);'
    },
    {
      Name: 'CTD_ULINT',
      Language: 'ST',
      POUType: 'FunctionBlock',
      Variables: [
        {
          Name: 'CD',
          DataType: 'BOOL',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'LD',
          DataType: 'BOOL',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'PV',
          DataType: 'ULINT',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'Q',
          DataType: 'BOOL',
          Class: 'Output',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'CV',
          DataType: 'ULINT',
          Class: 'Output',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'CD_T',
          DataType: 'R_TRIG',
          Class: 'Local',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
      ],
      Code: 'CD_T(CD);\nIF LD THEN CV := PV;\nELSIF CD_T.Q AND (CV > 0)\n  THEN CV := CV-1;\nEND_IF;\nQ := (CV <= 0);'
    },
    {
      Name: 'CTUD',
      Language: 'ST',
      POUType: 'FunctionBlock',
      Variables: [
        {
          Name: 'CU',
          DataType: 'BOOL',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'CD',
          DataType: 'BOOL',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'R',
          DataType: 'BOOL',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'LD',
          DataType: 'BOOL',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'PV',
          DataType: 'INT',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'QU',
          DataType: 'BOOL',
          Class: 'Output',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'QD',
          DataType: 'BOOL',
          Class: 'Output',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'CV',
          DataType: 'INT',
          Class: 'Output',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'CD_T',
          DataType: 'R_TRIG',
          Class: 'Output',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'CU_T',
          DataType: 'R_TRIG',
          Class: 'Output',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
      ],
      Code: 'CD_T(CD);\nCU_T(CU);\nIF R THEN CV := 0;\nELSIF LD THEN CV := PV;\nELSE\n  IF NOT (CU_T.Q AND CD_T.Q) THEN\n    IF CU_T.Q AND (CV < PV)\n    THEN CV := CV+1;\n    ELSIF CD_T.Q AND (CV > 0)\n    THEN CV := CV-1;\n    END_IF;\n  END_IF;\nEND_IF;\nQU := (CV >= PV);\nQD := (CV <= 0);'
    },
    {
      Name: 'CTUD_DINT',
      Language: 'ST',
      POUType: 'FunctionBlock',
      Variables: [
        {
          Name: 'CU',
          DataType: 'BOOL',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'CD',
          DataType: 'BOOL',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'R',
          DataType: 'BOOL',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'LD',
          DataType: 'BOOL',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'PV',
          DataType: 'DINT',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'QU',
          DataType: 'BOOL',
          Class: 'Output',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'QD',
          DataType: 'BOOL',
          Class: 'Output',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'CV',
          DataType: 'DINT',
          Class: 'Output',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'CD_T',
          DataType: 'R_TRIG',
          Class: 'Output',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'CU_T',
          DataType: 'R_TRIG',
          Class: 'Output',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
      ],
      Code: 'CD_T(CD);\nCU_T(CU);\nIF R THEN CV := 0;\nELSIF LD THEN CV := PV;\nELSE\n  IF NOT (CU_T.Q AND CD_T.Q) THEN\n    IF CU_T.Q AND (CV < PV)\n    THEN CV := CV+1;\n    ELSIF CD_T.Q AND (CV > 0)\n    THEN CV := CV-1;\n    END_IF;\n  END_IF;\nEND_IF;\nQU := (CV >= PV);\nQD := (CV <= 0);'
    },
    {
      Name: 'CTUD_LINT',
      Language: 'ST',
      POUType: 'FunctionBlock',
      Variables: [
        {
          Name: 'CU',
          DataType: 'BOOL',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'CD',
          DataType: 'BOOL',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'R',
          DataType: 'BOOL',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'LD',
          DataType: 'BOOL',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'PV',
          DataType: 'LINT',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'QU',
          DataType: 'BOOL',
          Class: 'Output',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'QD',
          DataType: 'BOOL',
          Class: 'Output',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'CV',
          DataType: 'LINT',
          Class: 'Output',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'CD_T',
          DataType: 'R_TRIG',
          Class: 'Output',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'CU_T',
          DataType: 'R_TRIG',
          Class: 'Output',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
      ],
      Code: 'CD_T(CD);\nCU_T(CU);\nIF R THEN CV := 0;\nELSIF LD THEN CV := PV;\nELSE\n  IF NOT (CU_T.Q AND CD_T.Q) THEN\n    IF CU_T.Q AND (CV < PV)\n    THEN CV := CV+1;\n    ELSIF CD_T.Q AND (CV > 0)\n    THEN CV := CV-1;\n    END_IF;\n  END_IF;\nEND_IF;\nQU := (CV >= PV);\nQD := (CV <= 0);'
    },
    {
      Name: 'CTUD_UDINT',
      Language: 'ST',
      POUType: 'FunctionBlock',
      Variables: [
        {
          Name: 'CU',
          DataType: 'BOOL',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'CD',
          DataType: 'BOOL',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'R',
          DataType: 'BOOL',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'LD',
          DataType: 'BOOL',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'PV',
          DataType: 'UDINT',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'QU',
          DataType: 'BOOL',
          Class: 'Output',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'QD',
          DataType: 'BOOL',
          Class: 'Output',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'CV',
          DataType: 'UDINT',
          Class: 'Output',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'CD_T',
          DataType: 'R_TRIG',
          Class: 'Output',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'CU_T',
          DataType: 'R_TRIG',
          Class: 'Output',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
      ],
      Code: 'CD_T(CD);\nCU_T(CU);\nIF R THEN CV := 0;\nELSIF LD THEN CV := PV;\nELSE\n  IF NOT (CU_T.Q AND CD_T.Q) THEN\n    IF CU_T.Q AND (CV < PV)\n    THEN CV := CV+1;\n    ELSIF CD_T.Q AND (CV > 0)\n    THEN CV := CV-1;\n    END_IF;\n  END_IF;\nEND_IF;\nQU := (CV >= PV);\nQD := (CV <= 0);'
    },
    {
      Name: 'CTUD_ULINT',
      Language: 'ST',
      POUType: 'FunctionBlock',
      Variables: [
        {
          Name: 'CU',
          DataType: 'BOOL',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'CD',
          DataType: 'BOOL',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'R',
          DataType: 'BOOL',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'LD',
          DataType: 'BOOL',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'PV',
          DataType: 'ULINT',
          Class: 'Input',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'QU',
          DataType: 'BOOL',
          Class: 'Output',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'QD',
          DataType: 'BOOL',
          Class: 'Output',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'CV',
          DataType: 'ULINT',
          Class: 'Output',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'CD_T',
          DataType: 'R_TRIG',
          Class: 'Output',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
        {
          Name: 'CU_T',
          DataType: 'R_TRIG',
          Class: 'Output',
          InitialValue: '',
          Address: '',
          Option: '',
          Documentation: '',
        },
      ],
      Code: 'CD_T(CD);\nCU_T(CU);\nIF R THEN CV := 0;\nELSIF LD THEN CV := PV;\nELSE\n  IF NOT (CU_T.Q AND CD_T.Q) THEN\n    IF CU_T.Q AND (CV < PV)\n    THEN CV := CV+1;\n    ELSIF CD_T.Q AND (CV > 0)\n    THEN CV := CV-1;\n    END_IF;\n  END_IF;\nEND_IF;\nQU := (CV >= PV);\nQD := (CV <= 0);'
    },
  ],
  Shift: [
    {
      Name: 'SHL',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'ANY_BIT',
          Class: 'Input',
          Documentation: '',
        },
        {
          Name: 'N',
          DataType: 'ANY_INT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'ANY_BIT',
        Documentation: '',
      }
    },
    {
      Name: 'SHR',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'ANY_BIT',
          Class: 'Input',
          Documentation: '',
        },
        {
          Name: 'N',
          DataType: 'ANY_INT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'ANY_BIT',
        Documentation: '',
      }
    },
    {
      Name: 'ROL',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'ANY_NBIT',
          Class: 'Input',
          Documentation: '',
        },
        {
          Name: 'N',
          DataType: 'ANY_INT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'ANY_NBIT',
        Documentation: '',
      }
    },
    {
      Name: 'ROR',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'ANY_NBIT',
          Class: 'Input',
          Documentation: '',
        },
        {
          Name: 'N',
          DataType: 'ANY_INT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'ANY_NBIT',
        Documentation: '',
      }
    },
  ],
  Selection: [
    {
      Name: 'SEL',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'G',
          DataType: 'BOOL',
          Class: 'Input',
          Documentation: '',
        },
        {
          Name: 'IN0',
          DataType: 'ANY',
          Class: 'Input',
          Documentation: '',
        },
        {
          Name: 'IN1',
          DataType: 'ANY',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'ANY',
        Documentation: '',
      }
    },
    {
      Name: 'MIN',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN1',
          DataType: 'ANY',
          Class: 'Input',
          Documentation: '',
        },
        {
          Name: 'IN2',
          DataType: 'ANY',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'ANY',
        Documentation: '',
      }
    },
    {
      Name: 'MAX',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN1',
          DataType: 'ANY',
          Class: 'Input',
          Documentation: '',
        },
        {
          Name: 'IN2',
          DataType: 'ANY',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'ANY',
        Documentation: '',
      }
    },
    {
      Name: 'LIMIT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'MN',
          DataType: 'ANY',
          Class: 'Input',
          Documentation: '',
        },
        {
          Name: 'IN',
          DataType: 'ANY',
          Class: 'Input',
          Documentation: '',
        },
        {
          Name: 'MX',
          DataType: 'ANY',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'ANY',
        Documentation: '',
      }
    },
    {
      Name: 'MUX',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'K',
          DataType: 'ANY_INT',
          Class: 'Input',
          Documentation: '',
        },
        {
          Name: 'IN0',
          DataType: 'ANY',
          Class: 'Input',
          Documentation: '',
        },
        {
          Name: 'IN1',
          DataType: 'ANY',
          Class: 'Input',
          Documentation: '',
        },
      ],
      Return: {
        Name: 'OUT',
        DataType: 'ANY',
        Documentation: '',
      }
    },
  ],
  Comparators: [
    {
      Name: 'GT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN1',
          DataType: 'ANY',
          Class: 'Input',
          Documentation: '',
        },
        {
          Name: 'IN2',
          DataType: 'ANY',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'BOOL',
        Documentation: '',
      }
    },
    {
      Name: 'GE',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN1',
          DataType: 'ANY',
          Class: 'Input',
          Documentation: '',
        },
        {
          Name: 'IN2',
          DataType: 'ANY',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'BOOL',
        Documentation: '',
      }
    },
    {
      Name: 'EQ',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN1',
          DataType: 'ANY',
          Class: 'Input',
          Documentation: '',
        },
        {
          Name: 'IN2',
          DataType: 'ANY',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'BOOL',
        Documentation: '',
      }
    },
    {
      Name: 'LT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN1',
          DataType: 'ANY',
          Class: 'Input',
          Documentation: '',
        },
        {
          Name: 'IN2',
          DataType: 'ANY',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'BOOL',
        Documentation: '',
      }
    },
    {
      Name: 'LE',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN1',
          DataType: 'ANY',
          Class: 'Input',
          Documentation: '',
        },
        {
          Name: 'IN2',
          DataType: 'ANY',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'BOOL',
        Documentation: '',
      }
    },
    {
      Name: 'NE',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN1',
          DataType: 'ANY',
          Class: 'Input',
          Documentation: '',
        },
        {
          Name: 'IN2',
          DataType: 'ANY',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'BOOL',
        Documentation: '',
      }
    },
  ],
  String: [
    {
      Name: 'LEN',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'STRING',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'INT',
        Documentation: '',
      }
    },
    {
      Name: 'LEFT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'STRING',
          Class: 'Input',
          Documentation: '',
        },
        {
          Name: 'L',
          DataType: 'ANY_INT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'STRING',
        Documentation: '',
      }
    },
    {
      Name: 'RIGHT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'STRING',
          Class: 'Input',
          Documentation: '',
        },
        {
          Name: 'L',
          DataType: 'ANY_INT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'STRING',
        Documentation: '',
      }
    },
    {
      Name: 'MID',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'STRING',
          Class: 'Input',
          Documentation: '',
        },
        {
          Name: 'L',
          DataType: 'ANY_INT',
          Class: 'Input',
          Documentation: '',
        },
        {
          Name: 'P',
          DataType: 'ANY_INT',
          Class: 'Input',
          Documentation: '',
        }
      ],
      Return: {
        Name: 'OUT',
        DataType: 'STRING',
        Documentation: '',
      }
    },
    {
      Name: 'CONCAT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN1',
          DataType: 'STRING',
          Class: 'Input',
          Documentation: '',
        },
        {
          Name: 'IN2',
          DataType: 'STRING',
          Class: 'Input',
          Documentation: '',
        },
      ],
      Return: {
        Name: 'OUT',
        DataType: 'STRING',
        Documentation: '',
      }
    },
    {
      Name: 'CONCAT_DAT_TOD',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN1',
          DataType: 'DATE',
          Class: 'Input',
          Documentation: '',
        },
        {
          Name: 'IN2',
          DataType: 'TOD',
          Class: 'Input',
          Documentation: '',
        },
      ],
      Return: {
        Name: 'OUT',
        DataType: 'DT',
        Documentation: '',
      }
    },
    {
      Name: 'INSERT',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN1',
          DataType: 'STRING',
          Class: 'Input',
          Documentation: '',
        },
        {
          Name: 'IN2',
          DataType: 'STRING',
          Class: 'Input',
          Documentation: '',
        },
        {
          Name: 'P',
          DataType: 'ANY_INT',
          Class: 'Input',
          Documentation: '',
        },
      ],
      Return: {
        Name: 'OUT',
        DataType: 'STRING',
        Documentation: '',
      }
    },
    {
      Name: 'DELETE',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN',
          DataType: 'STRING',
          Class: 'Input',
          Documentation: '',
        },
        {
          Name: 'L',
          DataType: 'ANY_INT',
          Class: 'Input',
          Documentation: '',
        },
        {
          Name: 'P',
          DataType: 'ANY_INT',
          Class: 'Input',
          Documentation: '',
        },
      ],
      Return: {
        Name: 'OUT',
        DataType: 'STRING',
        Documentation: '',
      }
    },
    {
      Name: 'REPLACE',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN1',
          DataType: 'STRING',
          Class: 'Input',
          Documentation: '',
        },
        {
          Name: 'IN2',
          DataType: 'STRING',
          Class: 'Input',
          Documentation: '',
        },
        {
          Name: 'L',
          DataType: 'ANY_INT',
          Class: 'Input',
          Documentation: '',
        },
        {
          Name: 'P',
          DataType: 'ANY_INT',
          Class: 'Input',
          Documentation: '',
        },
      ],
      Return: {
        Name: 'OUT',
        DataType: 'STRING',
        Documentation: '',
      }
    },
    {
      Name: 'FIND',
      POUType: 'StandardFunction',
      Variables: [
        {
          Name: 'IN1',
          DataType: 'STRING',
          Class: 'Input',
          Documentation: '',
        },
        {
          Name: 'IN2',
          DataType: 'STRING',
          Class: 'Input',
          Documentation: '',
        },
      ],
      Return: {
        Name: 'OUT',
        DataType: 'INT',
        Documentation: '',
      }
    },

  ],
};

export default functions;