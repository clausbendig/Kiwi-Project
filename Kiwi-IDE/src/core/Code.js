
/*
* Copyright (C) Claus Bendig
*/

'use strict'

var Code = {
  generate(project) {
    var sourceCode = '';

    // DataType
    if (project.DataTypes.length > 0) {
      sourceCode += 'TYPE\n';
      for(var i = 0; i < project.DataTypes.length; i++) {
        sourceCode += this.DataType(project.DataTypes[i]);
      }
      sourceCode += 'END_TYPE\n\n';
    }

    for(var i = 0; i < project.Programs.length; i++) {
      sourceCode += this.Program(project.Programs[i]);
    }
    for(var i = 0; i < project.Functions.length; i++) {
      sourceCode += this.Function(project.Functions[i]);
    }
    for(var i = 0; i < project.FunctionBlocks.length; i++) {
      sourceCode += this.FunctionBlock(project.FunctionBlocks[i]);
    }
    sourceCode += this.Configuration(project.Resource);
    return sourceCode;
  },
  DataType(dataType) {
    var sourceCode = '';
    switch(dataType.DerivationType) {
      case 'Directly':
        sourceCode += this.Directly(dataType);
        break;
      case 'Subrange':
        sourceCode += this.Subrange(dataType);
        break;
      case 'Array':
        sourceCode += this.Array(dataType);
        break;
      case 'Enumeration':
        sourceCode += this.Enumeration(dataType);
        break;
      case 'Structure':
        sourceCode += this.Structure(dataType);
        break;
    }
    return sourceCode;
  },
  Directly(dataType) {
    var sourceCode = '';
    sourceCode += '  ' + dataType.Name + ' : ' + dataType.BaseType;
    if (dataType.InitialValue != '') {
      sourceCode += ' := ' + dataType.InitialValue + ';'
    } else {
      sourceCode += ';\n'
    }
    return sourceCode;
  },
  Subrange(dataType) {
    var sourceCode = '';
    sourceCode += '  ' + dataType.Name + ' : ' + dataType.BaseType + ' (' + dataType.Range.Lower + '..' + dataType.Range.Upper + ')';
    if (dataType.InitialValue != '') {
      sourceCode += ' := ' + dataType.InitialValue + ';'
    } else {
      sourceCode += ';\n'
    }
    return sourceCode;
  },
  Array(dataType) {
    var sourceCode = '';
    sourceCode += '  ' + dataType.Name + ' : ARRAY [';
    for (var i = 0; i < dataType.Dimensions.length; i++) {
      sourceCode += dataType.Dimensions[i].Lower + '..' + dataType.Dimensions[i].Upper;
      if (i < (dataType.Dimensions.length - 1)) sourceCode += ', ';
    }
    sourceCode += '] OF ' + dataType.BaseType;
    if (dataType.InitialValue != '') {
      sourceCode += ' := ' + dataType.InitialValue + ';\n'
    } else {
      sourceCode += ';\n'
    }
    return sourceCode;
  },
  Enumeration(dataType) {
    var sourceCode = '';
    sourceCode += '  ' + dataType.Name + ' : (';
    for (var i = 0; i < dataType.Members.length; i++) {
      sourceCode += dataType.Members[i].Name;
      if (i < (dataType.Members.length - 1)) sourceCode += ', ';
    }
    sourceCode += ')';
    if (dataType.InitialValue != '') {
      sourceCode += ' := ' + dataType.InitialValue + ';\n'
    } else {
      sourceCode += ';\n'
    }
    return sourceCode;
  },
  Structure(dataType) {
    var sourceCode = '';
    sourceCode += '  ' + dataType.Name + ' : STRUCT\n';
    for (var i = 0; i < dataType.Members.length; i++) {
      sourceCode += '    ' + dataType.Members[i].Name + ' : ' + dataType.Members[i].BaseType;
      if (dataType.Members[i].InitialValue != '') {
        sourceCode += ' := ' + dataType.Members[i].InitialValue + ';'
      } else {
        sourceCode += ';\n'
      }
    }
    sourceCode += '  END_STRUCT;\n';
    return sourceCode;
  },
  Program(pou) {
    var sourceCode = '';
    sourceCode += 'PROGRAM ' + pou.Name + '\n\n';
    sourceCode += this.VariablesTable(pou.Variables, '  ');
    switch(pou.Language) {
      case 'IL':
        sourceCode += this.IL(pou);
        break;
      case 'ST':
        sourceCode += this.ST(pou);
        break;
      case 'FBD':
        sourceCode += this.FBD(pou);
        break;
      case 'LD':
        sourceCode += this.LD(pou);
        break;
      case 'SFC':
        sourceCode += this.SFC(pou);
        break;
    }
    sourceCode += 'END_PROGRAM\n\n';
    return sourceCode;
  },
  Function(pou) {
    var sourceCode = '';
    sourceCode += 'FUNCTION ' + pou.Name + '\n\n';
    sourceCode += this.VariablesTable(pou.Variables, '  ');
    switch(pou.Language) {
      case 'IL':
        sourceCode += this.IL(pou);
        break;
      case 'ST':
        sourceCode += this.ST(pou);
        break;
      case 'FBD':
        sourceCode += this.FBD(pou);
        break;
      case 'LD':
        sourceCode += this.LD(pou);
        break;
      case 'SFC':
        sourceCode += this.SFC(pou);
        break;
    }
    sourceCode += 'END_FUNCTION\n\n';
    return sourceCode;
  },
  FunctionBlock(pou) {
    var sourceCode = '';
    sourceCode += 'FUNCTION_BLOCK ' + pou.Name + '\n\n';
    sourceCode += this.VariablesTable(pou.Variables, '  ');
    switch(pou.Language) {
      case 'IL':
        sourceCode += this.IL(pou);
        break;
      case 'ST':
        sourceCode += this.ST(pou);
        break;
      case 'FBD':
        sourceCode += this.FBD(pou);
        break;
      case 'LD':
        sourceCode += this.LD(pou);
        break;
      case 'SFC':
        sourceCode += this.SFC(pou);
        break;
    }
    sourceCode += 'END_FUNCTION_BLOCK\n\n';
    return sourceCode;
  },
  IL(pou) {
    var sourceCode = '';
    if (pou.Code.SourceCode != '')
      sourceCode += '  ' + pou.Code.SourceCode.replace(new RegExp('\n', 'g'), '\n  ') + '\n\n';
    return sourceCode;
  },
  ST(pou) {
    var sourceCode = '';
    if (pou.Code.SourceCode != '')
    sourceCode += '  ' + pou.Code.SourceCode.replace(new RegExp('\n', 'g'), '\n  ') + '\n\n';
    return sourceCode;
  },
  FBD(pou) {
    var sourceCode = '';
    return sourceCode;
  },
  LD(pou) {
    var sourceCode = '';
    return sourceCode;
  },
  SFC(pou) {
    var sourceCode = '';
    return sourceCode;
  },
  Configuration(resource) {
    var sourceCode = '';
    sourceCode += 'CONFIGURATION MainConfiguration\n';
    sourceCode += '  RESOURCE MainResource ON PLC\n\n'
    sourceCode += this.VariablesTable(resource.Variables, '    ');
    for (var i = 0; i < resource.Tasks.length; i++) {
      if (resource.Tasks[i].Type == 'Periodic') {
        sourceCode += '    TASK ' + resource.Tasks[i].Name + '(INTERVAL := ' + resource.Tasks[i].Cycle + ', PRIORITY := ' + resource.Tasks[i].Priority + ');\n';
      } else {
        sourceCode += '    TASK ' + resource.Tasks[i].Name + '(SINGLE := ' + resource.Tasks[i].Event + ', PRIORITY := ' + resource.Tasks[i].Priority + ');\n';
      }
    }
    for (var i = 0; i < resource.Tasks.length; i++) {
      sourceCode += '    PROGRAM instance' + i + ' WITH ' + resource.Tasks[i].Name + ' : ' + resource.Tasks[i].Program + ';\n'
    }
    sourceCode += '\n';
    sourceCode += '  END_RESOURCE\n';
    sourceCode += 'END_CONFIGURATION';
    return sourceCode;
  },
  VariablesTable(variables, indention) {
    var sourceCode = '';

    var input = _.filter(variables, (variable) => {
      return ((variable.Class == 'Input') && (variable.Option == ''));
    });

    if (input.length > 0) {
      sourceCode += indention + 'VAR_INPUT\n';
      input.forEach((variable) => {
        sourceCode += this.Variable(variable, indention + '  ');
      });
      sourceCode += indention + 'END_VAR\n\n';
    }

    var output = _.filter(variables, (variable) => {
      return ((variable.Class == 'Output') && (variable.Option == ''));
    });

    if (output.length > 0) {
      sourceCode += indention + 'VAR_OUTPUT\n';
      output.forEach((variable) => {
        sourceCode += this.Variable(variable, indention + '  ');
      });
      sourceCode += indention + 'END_VAR\n\n';
    }

    var outputRetain = _.filter(variables, (variable) => {
      return ((variable.Class == 'Output') && (variable.Option == 'Retain'));
    });

    if (outputRetain.length > 0) {
      sourceCode += indention + 'VAR_OUTPUT RETAIN\n';
      outputRetain.forEach((variable) => {
        sourceCode += this.Variable(variable, indention + '  ');
      });
      sourceCode += indention + 'END_VAR\n\n';
    }

    var inOut = _.filter(variables, (variable) => {
      return (variable.Class == 'InOut');
    });

    if (inOut.length > 0) {
      sourceCode += indention + 'VAR_IN_OUT\n';
      inOut.forEach((variable) => {
        sourceCode += this.Variable(variable, indention + '  ');
      });
      sourceCode += indention + 'END_VAR\n\n';
    }

    var external = _.filter(variables, (variable) => {
      return ((variable.Class == 'External') && (variable.Option == ''));
    });

    if (external.length > 0) {
      sourceCode += indention + 'VAR_EXTERNAL\n';
      external.forEach((variable) => {
        sourceCode += this.Variable(variable, indention + '  ');
      });
      sourceCode += indention + 'END_VAR\n\n';
    }

    var local = _.filter(variables, (variable) => {
      return ((variable.Class == 'Local') && (variable.Option == ''));
    });

    if (local.length > 0) {
      sourceCode += indention + 'VAR\n';
      local.forEach((variable) => {
        sourceCode += this.Variable(variable, indention + '  ');
      });
      sourceCode += indention + 'END_VAR\n\n';
    }

    var localConstant = _.filter(variables, (variable) => {
      return ((variable.Class == 'Local') && (variable.Option == 'Constant'));
    });

    if (localConstant.length > 0) {
      sourceCode += indention + 'VAR CONSTANT\n';
      localConstant.forEach((variable) => {
        sourceCode += this.Variable(variable, indention + '  ');
      });
      sourceCode += indention + 'END_VAR\n\n';
    }

    var localRetain = _.filter(variables, (variable) => {
      return ((variable.Class == 'Local') && (variable.Option == 'Retain'));
    });

    if (localRetain.length > 0) {
      sourceCode += indention + 'VAR RETAIN\n';
      localRetain.forEach((variable) => {
        sourceCode += this.Variable(variable, indention + '  ');
      });
      sourceCode += indention + 'END_VAR\n\n';
    }

    var temporary = _.filter(variables, (variable) => {
      return (variable.Class == 'Temporary');
    });

    if (temporary.length > 0) {
      sourceCode += indention + 'VAR_TEMP\n';
      temporary.forEach((variable) => {
        sourceCode += this.Variable(variable, indention + '  ');
      });
      sourceCode += indention + 'END_VAR\n\n';
    }

    var global = _.filter(variables, (variable) => {
      return ((variable.Class == 'Global') && (variable.Option == ''));
    });

    if (global.length > 0) {
      sourceCode += indention + 'VAR_GLOBAL\n';
      global.forEach((variable) => {
        sourceCode += this.Variable(variable, indention + '  ');
      });
      sourceCode += indention + 'END_VAR\n\n';
    }

    var globalConstant = _.filter(variables, (variable) => {
      return ((variable.Class == 'Global') && (variable.Option == 'Constant'));
    });

    if (globalConstant.length > 0) {
      sourceCode += indention + 'VAR_GLOBAL CONSTANT\n';
      globalConstant.forEach((variable) => {
        sourceCode += this.Variable(variable, indention + '  ');
      });
      sourceCode += indention + 'END_VAR\n\n';
    }

    var globalRetain = _.filter(variables, (variable) => {
      return ((variable.Class == 'Global') && (variable.Option == 'Retain'));
    });

    if (globalRetain.length > 0) {
      sourceCode += indention + 'VAR_GLOBAL RETAIN\n';
      globalRetain.forEach((variable) => {
        sourceCode += this.Variable(variable, indention + '  ');
      });
      sourceCode += indention + 'END_VAR\n\n';
    }

    return sourceCode;
  },
  Variable(variable, indention) {
    var sourceCode = '';
    sourceCode += indention + variable.Name;
    if (variable.Address != '') sourceCode += ' AT ' + variable.Address;
    sourceCode += ' : ' + variable.DataType;
    if (variable.InitialValue != '') {
      if (variable.DataType == 'STRING') {
        sourceCode += ' := \'' + variable.InitialValue + '\';\n';
      } else {
        sourceCode += ' := ' + variable.InitialValue + ';\n';
      }
      
    } else {
      sourceCode += ';\n'
    }
    return sourceCode;
  }
};

export default Code;