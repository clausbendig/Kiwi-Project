/*
* Copyright (C) Claus Bendig
*/

'use strict'

import { remote } from 'electron';
import os from 'os';
import path from 'path';

var Paths = {
  ApplicationData: remote.app.getPath('userData'),
  Database: remote.app.getPath('userData') + path.sep + 'Kiwi-IDE.db',
  Home: remote.app.getPath('home')
};

if (os.platform() === 'win32') {
  Paths.Home = remote.app.getPath('home') + path.sep + 'Documents';
}

export default Paths;