/*
* Copyright (C) Claus Bendig
*/

'use strict'

import _ from 'underscore';

const Keywords = {
  DataTypes: [
    'BOOL',
    'SINT',
    'INT',
    'DINT',
    'LINT',
    'USINT',
    'UINT',
    'UDINT',
    'ULINT',
    'BYTE',
    'WORD',
    'DWORD',
    'LWORD',
    'STRING',
    'DATE',
    'TOD',
    'DT',
    'TIME'
  ],
  Type: [
    'TYPE',
    'STRUCT',
    'END_TYPE',
    'END_STRUCT',
    'ARRAY',
    'OF',
    'T',
    'D',
    'TIME_OF_DAY',
    'DATE_AND_TIME'
  ],
  Variable: [
    'VAR',
    'VAR_INPUT',
    'VAR_OUTPUT',
    'VAR_IN_OUT',
    'VAR_TEMP',
    'VAR_EXTERNAL',
    'END_VAR',
    'AT',
    'CONSTANT',
    'RETAIN',
    'NON_RETAIN'
  ],
  Configuration: [
    'CONFIGURATION',
    'RESOURCE',
    'VAR_ACCESS',
    'VAR_CONFIG',
    'VAR_GLOBAL',
    'END_CONFIGURATION',
    'END_RESOURCE',
    'END_VAR',
    'ON',
    'PROGRAM',
    'WITH',
    'READ_ONLY',
    'READ_WRITE',
    'TASK'
  ],
  FBD: [
    'FUNCTION',
    'FUNCTION_BLOCK',
    'PROGRAM',
    'END_FUNCTION',
    'END_FUNCTION_BLOCK',
    'END_PROGRAM',
    'EN',
    'ENO',
    'F_EDGE',
    'R_EDGE'
  ],
  SFC: [
    'ACTION',
    'INITIAL_STEP',
    'STEP',
    'TRANSITION',
    'END_ACTION',
    'END_STEP',
    'END_TRANSITION',
    'FROM',
    'TO'
  ],
  IL: [
    'TRUE',
    'FALSE',
    'LD',
    'LDN',
    'ST',
    'STN',
    'S',
    'R',
    'AND',
    'ANDN',
    'OR',
    'ORN',
    'XOR',
    'XORN',
    'NOT',
    'ADD',
    'SUB',
    'MUL',
    'DIV',
    'MOD',
    'GT',
    'GE',
    'EQ',
    'NE',
    'LE',
    'LT',
    'JMP',
    'JMPC',
    'JMPCN',
    'CAL',
    'CALC',
    'CALCN',
    'RET',
    'RETC',
    'RETCN'
  ],
  ST: [
    'IF',
    'ELSIF',
    'ELSE',
    'CASE',
    'FOR',
    'WHILE',
    'REPEAT',
    'END_IF',
    'END_CASE',
    'END_FOR',
    'END_WHILE',
    'END_REPEAT',
    'TRUE',
    'FALSE',
    'THEN',
    'OF',
    'TO',
    'BY',
    'DO',
    'DO',
    'UNTIL',
    'EXIT',
    'RETURN',
    'NOT',
    'MOD',
    'AND',
    'XOR',
    'OR'
  ],
  Others: [
    'E',
    'MainResource',
    'MainConfiguration'
  ]
};

const All = _.union(
  _.flatten(Object.values(Keywords))
);

Keywords.All = All;

export default Keywords;