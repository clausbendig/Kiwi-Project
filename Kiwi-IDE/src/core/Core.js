/*
* Copyright (C) Claus Bendig
*/

'use strict'

import _ from 'underscore';
import Keywords from './Keywords';
import ProjectManager from './ProjectManager';
import DataType from './DataType';
import Code from './Code';
import Connector from './Connector';
import StandardFunctions from './StandardFunctions';
import { stat } from 'fs';

var Core = {
  install: function(Vue, Store) {
  
    Vue.prototype.$core = {
      StandardFunctions: StandardFunctions,
      PLC: {
        search() {
          return Connector.search();
        },
        connect(address, events) {
          Connector.connect(address, events);
        },
        compile(st, project) {
          Store.state.PLC.Compiling = true;
          Connector.compile(st, project);
        },
        transfer() {
          Store.state.PLC.Transfering = true;
          Connector.transfer();
        },
        disconnect() {
          Connector.disconnect();
        },
        start() {
          Connector.start();
        },
        stop() {
          Connector.stop();
        },
        updateStatus(status) {
          if (status.hasOwnProperty('Connected'))    Store.state.PLC.Connected    = status.Connected;
          if (status.hasOwnProperty('Compiling'))    Store.state.PLC.Compiling    = status.Compiling;
          if (status.hasOwnProperty('Transfering'))  Store.state.PLC.Transfering  = status.Transfering;
          if (status.hasOwnProperty('Running'))      Store.state.PLC.Running      = status.Running;
          if (status.hasOwnProperty('Compiled'))     Store.state.PLC.Compiled     = status.Compiled;
          if (status.hasOwnProperty('hasProgram'))   Store.state.PLC.hasProgram   = status.hasProgram;
          if (status.hasOwnProperty('Blocked'))      Store.state.PLC.Blocked      = status.Blocked;
          if (status.hasOwnProperty('Information'))
          {
            if (status.Information.hasOwnProperty('canDebug'))     Store.state.PLC.Information.canDebug     = status.Information.canDebug;
            if (status.Information.hasOwnProperty('canStartStop')) Store.state.PLC.Information.canStartStop = status.Information.canStartStop;
          }
        },
        get Connected() {
          return Store.state.PLC.Connected;
        },
        get Compiling() {
          return Store.state.PLC.Compiling;
        },
        get Compiled() {
          return Store.state.PLC.Compiled;
        },
        get Running() {
          return Store.state.PLC.Running;
        },
        get Transfering() {
          return Store.state.PLC.Transfering;
        },
        get hasProgram() {
          return Store.state.PLC.hasProgram;
        },
        get Blocked() {
          return Store.state.PLC.Blocked;
        },
        get canStartStop() {
          return Store.state.PLC.Information.canStartStop;
        },
        get canDebug() {
          return Store.state.PLC.Information.canDebug;
        },
        setPLC(information) {
          Store.state.PLC.Information = plc;
        },
        clearPLC() {
          Store.state.PLC.Information = {
            canStartStop: false,
            canDebug: false
          };
        }

      },
      Projects: {
        add(info) {
          return ProjectManager.newProject(info);
        },
        remove(project) {
          return new Promise((resolve, reject) => {
            ProjectManager.deleteProject(project).then(() => {
              Store.state.Projects = ProjectManager.getProjects();
              resolve();
            }).catch(() => {
              reject();
            });
          });
        },
        import(path) {
          return ProjectManager.importProject(path);
        },
        export(project, path) {
          return ProjectManager.exportProject(project, path);
        },
        edit(project) {
          Store.state.CurrentProject = project;
        },
        load(project) {
          return ProjectManager.loadProject(project);
        },
        save(project) {
          return ProjectManager.saveProject(project);
        },
        sorted(filter) {
          Store.state.Projects = ProjectManager.getProjects();
          if (filter == '') {
            return _.sortBy(Store.state.Projects, (project) => {
              return project.Meta.Name;
            });
          } else {
            var filtered = _.filter(Store.state.Projects, (project) => {
              return project.Meta.Name.toLowerCase().includes(filter.toLowerCase());
            });
            return _.sortBy(filtered, (project) => {
              return project.Meta.Name;
            });
          }
        },
        get Total() {
          return this.sorted('').length;
        }
      },
      Project: {
        Code: {
          generateProject() {
            return Code.generate(Store.state.CurrentProject);
          },
          generateProgram(pou) {
            return Code.Program(pou);
          },
          generateFunction(pou) {
            return Code.Function(pou);
          },
          generateFunctionBlock(pou) {
            return Code.FunctionBlock(pou);
          }
        },
        Meta: {
          get() {
            return Store.state.CurrentProject.Meta;
          },
          set(meta) {
            Store.state.CurrentProject.Meta.Name           = meta.Name;
            Store.state.CurrentProject.Meta.Author         = meta.Author;
            Store.state.CurrentProject.Meta.CompanyName    = meta.Name;
            Store.state.CurrentProject.Meta.CompanyURL     = meta.CompanyURL;
            Store.state.CurrentProject.Meta.ProductName    = meta.ProductName;
            Store.state.CurrentProject.Meta.ProductVersion = meta.ProductVersion;
            Store.state.CurrentProject.Meta.Description    = meta.Description;
          }
        },
        Name: {
          isForbidden(name, checkPOUs = false) {
            if ((/^[[A-Za-z_]+[A-Za-z0-9_]*$/.test(name) == false) && (name != '')) {
              return true;
            }
            var upperCaseName = name.toUpperCase();
            var lowerCaseName = name.toLowerCase();
            if (_.indexOf(Keywords.All, upperCaseName) >= 0) {
              return true;
            }
            if (name.startsWith('instance')) return true;

            if (checkPOUs) {
              for (var i = 0; i < Store.state.CurrentProject.DataTypes.length; i++) {
                if (Store.state.CurrentProject.DataTypes[i].Name.toLowerCase() == lowerCaseName) return true;
              }
              for (var i = 0; i < Store.state.CurrentProject.Programs.length; i++) {
                if (Store.state.CurrentProject.Programs[i].Name.toLowerCase() == lowerCaseName) return true;
              }
              for (var i = 0; i < Store.state.CurrentProject.Functions.length; i++) {
                if (Store.state.CurrentProject.Functions[i].Name.toLowerCase() == lowerCaseName) return true;
              }
              for (var i = 0; i < Store.state.CurrentProject.FunctionBlocks.length; i++) {
                if (Store.state.CurrentProject.FunctionBlocks[i].Name.toLowerCase() == lowerCaseName) return true;
              }
            }

            return false;
          }
        },
        Program: {
          add(name, language) {
            var code = {};
            switch(language) {
              case 'IL': 
                code.SourceCode = '';
                break
              
              case 'ST': 
                code.SourceCode = '';
                break

              case 'FBD':
                code.Networks = [];
                break

              case 'LD': 
                code.Networks = [];
                break

              case 'SFC': 
                break
              
            }
            var program = {
              Name: name,
              Language: language,
              POUType: 'Program',
              Variables: [
              ],
              Code: code
            };
            Store.state.CurrentProject.Programs.push(program);
          },
          clone(pou, name) {
            var clone = JSON.parse(JSON.stringify(pou));
            clone.Name = name;
            Store.state.CurrentProject.Programs.push(clone);
          },
          remove(name) {
            var index = _.findIndex(Store.state.CurrentProject.Programs, (pou) => {
              return (pou.Name == name);
            });
            Store.state.CurrentProject.Programs.splice(index, 1);
          },
          getAll() {
            return Store.state.CurrentProject.Programs;
          },
          get(name) {
            var index = _.findIndex(Store.state.CurrentProject.Programs, (pou) => {
              return (pou.Name == name);
            });
            return Store.state.CurrentProject.Programs[index];
          }
        },
        Function: {
          add(name, language) {
            var code = {};
            switch(language) {
              case 'IL': 
                code.SourceCode = '';
                break
              
              case 'ST': 
                code.SourceCode = '';
                break

              case 'FBD': 
                code.Networks = [];
                break

              case 'LD':
                code.Networks = [];
                break

              case 'SFC': 
                break
              
            }
            var func = {
              Name: name,
              Language: language,
              POUType: 'Function',
              Variables: [
              ],
              Return: {
                Name: 'OUT',
                DataType: 'VOID',
                Documentation: '',
              },
              Code: code
            };
            Store.state.CurrentProject.Functions.push(func);
          },
          clone(pou, name) {
            var clone = JSON.parse(JSON.stringify(pou));
            clone.Name = name;
            Store.state.CurrentProject.Functions.push(clone);
          },
          remove(name) {
            var index = _.findIndex(Store.state.CurrentProject.Functions, (pou) => {
              return (pou.Name == name);
            });
            Store.state.CurrentProject.Functions.splice(index, 1);
          },
          getAll() {
            return Store.state.CurrentProject.Functions;
          },
          get(name) {
            var index = _.findIndex(Store.state.CurrentProject.Functions, (pou) => {
              return (pou.Name == name);
            });
            return Store.state.CurrentProject.Functions[index];
          }
        },
        FunctionBlock: {
          add(name, language) {
            var code = {};
            switch(language) {
              case 'IL': 
                code.SourceCode = '';
                break
              
              case 'ST': 
                code.SourceCode = '';
                break

              case 'FBD':
                code.Networks = [];
                break

              case 'LD': 
                code.Networks = [];
                break

              case 'SFC': 
                break
              
            }
            var func = {
              Name: name,
              Language: language,
              POUType: 'FunctionBlock',
              Variables: [
              ],
              Code: code
            };
            Store.state.CurrentProject.FunctionBlocks.push(func);
          },
          clone(pou, name) {
            var clone = JSON.parse(JSON.stringify(pou));
            clone.Name = name;
            Store.state.CurrentProject.FunctionBlocks.push(clone);
          },
          remove(name) {
            var index = _.findIndex(Store.state.CurrentProject.FunctionBlocks, (pou) => {
              return (pou.Name == name);
            });
            Store.state.CurrentProject.FunctionBlocks.splice(index, 1);
          },
          getAll() {
            return Store.state.CurrentProject.FunctionBlocks;
          },
          get(name) {
            var index = _.findIndex(Store.state.CurrentProject.FunctionBlocks, (pou) => {
              return (pou.Name == name);
            });
            return Store.state.CurrentProject.FunctionBlocks[index];
          },
        },
        DataType: {
          Subrange: {
            change(dataType, baseType) {
              dataType.BaseType = baseType;
              dataType.Range = {
                Lower: DataType.Minimum[baseType],
                Upper: DataType.Maximum[baseType]
              };
            }
          },
          Enumeration: {
            add(members) {
              members.push(
                {
                  Name: ''
                }
              );
            },
            moveUp(members, index) {
              return _.moveUp(members, index);
            },
            moveDown(members, index) {
              return _.moveDown(members, index);
            },
            remove(members, index) {
              _.remove(members, index);
            }
          },
          Structure: {
            add(members) {
              members.push(
                {
                  Name: '',
                  BaseType: 'BOOL',
                  InitialValue: ''
                }
              );
            },
            moveUp(members, index) {
              return _.moveUp(members, index);
            },
            moveDown(members, index) {
              return _.moveDown(members, index);
            },
            remove(members, index) {
              _.remove(members, index);
            }
          },
          add(name, derivation) {
            switch(derivation) {
              case 'Directly':
                Store.state.CurrentProject.DataTypes.push({
                  POUType: 'DataType',
                  Name: name,
                  InitialValue: '',
                  DerivationType: 'Directly',
                  BaseType: 'BOOL'
                });
                break;
              case 'Subrange':
                Store.state.CurrentProject.DataTypes.push({
                  POUType: 'DataType',
                  Name: name,
                  InitialValue: '',
                  DerivationType: 'Subrange',
                  BaseType: 'INT',
                  Range: {
                    Lower: -32768,
                    Upper: 32767
                  }
                });
                break;
              case 'Enumeration':
                Store.state.CurrentProject.DataTypes.push({
                  POUType: 'DataType',
                  Name: name,
                  InitialValue: 'Default',
                  DerivationType: 'Enumeration',
                  Members: [
                    { Name: 'Default' }
                  ]
                });
                break;
              case 'Array':
                Store.state.CurrentProject.DataTypes.push({
                  POUType: 'DataType',
                  Name: name,
                  InitialValue: '',
                  DerivationType: 'Array',
                  BaseType: 'BOOL',
                  Dimensions: [
                    {
                      Lower: 0,
                      Upper: 9
                    },
                    {
                      Lower: 10,
                      Upper: 19
                    }
                  ]
                });
                break;
              case 'Structure':
                Store.state.CurrentProject.DataTypes.push({
                  POUType: 'DataType',
                  Name: name,
                  InitialValue: '',
                  DerivationType: 'Structure',
                  Members: [
                    {
                      Name: 'Default',
                      BaseType: 'BOOL',
                      InitialValue: ''
                    },
                  ]
                });
                break;
            }
          },
          changeDerivation(dataType, derivation) {
            switch(derivation) {
              case 'Directly':
                dataType = {
                  POUType: 'DataType',
                  Name: dataType.Name,
                  InitialValue: '',
                  DerivationType: 'Directly',
                  BaseType: 'BOOL'
                };
                break;
              case 'Subrange':
                dataType = {
                  POUType: 'DataType',
                  Name: dataType.Name,
                  InitialValue: '',
                  DerivationType: 'Subrange',
                  BaseType: 'INT',
                  Range: {
                    Lower: -32768,
                    Upper: 32767
                  }
                };
                break;
              case 'Enumeration':
                dataType = {
                  POUType: 'DataType',
                  Name: dataType.Name,
                  InitialValue: 'Default',
                  DerivationType: 'Enumeration',
                  Members: [
                    { Name: 'Default' }
                  ]
                };
                break;
              case 'Array':
                dataType = {
                  POUType: 'DataType',
                  Name: dataType.Name,
                  InitialValue: '',
                  DerivationType: 'Array',
                  BaseType: 'BOOL',
                  Dimensions: [
                    {
                      Lower: 0,
                      Upper: 9
                    },
                    {
                      Lower: 10,
                      Upper: 19
                    }
                  ]
                };
                break;
              case 'Structure':
                dataType = {
                  POUType: 'DataType',
                  Name: dataType.Name,
                  InitialValue: '',
                  DerivationType: 'Structure',
                  Members: [
                    {
                      Name: 'Default',
                      BaseType: 'BOOL',
                      InitialValue: ''
                    },
                  ]
                };
                break;
            }
            return dataType;
          },
          clone(pou, name) {
            var clone = JSON.parse(JSON.stringify(pou));
            clone.Name = name;
            Store.state.CurrentProject.DataTypes.push(clone);
          },
          remove(name) {
            var index = _.findIndex(Store.state.CurrentProject.DataTypes, (pou) => {
              return (pou.Name == name);
            });
            Store.state.CurrentProject.DataTypes.splice(index, 1);
          },
          get(name) {
            var index = _.findIndex(Store.state.CurrentProject.DataTypes, (dt) => {
              return (dt.Name == name);
            });
            return Store.state.CurrentProject.DataTypes[index];
          },
          getAll() {
            return _.sortBy(Store.state.CurrentProject.DataTypes, 'Name');
          },
          set(dataType) {
            var index = _.findIndex(Store.state.CurrentProject.DataTypes, (dt) => {
              return (dt.Name == dataType.Name);
            });
            Store.state.CurrentProject.DataTypes[index] = dataType;
          },
          available(dataType) {
            var available = DataType.available(dataType, Store.state.CurrentProject.DataTypes);
            return _.sortBy(available, 'Name');
          },
          isValid(dataType, value) {
            if (DataType.isBaseType(dataType)) {
              return this.isValidBase(dataType, value);
            } else {
              return true; // Must be changed!!!
            }
            
          },
          isValidBase: DataType.isValidBase,
          beautify: DataType.beautify,
          Minimum: DataType.Minimum,
          Maximum: DataType.Maximum,
          BaseDataTypes: DataType.BaseDataTypes,
        },
        Resource: {
          Task: {
            getAll() {
              return Store.state.CurrentProject.Resource.Tasks;
            },
            add(tasks) {
              tasks.push(
                {
                  Name: '',
                  Priority: 0,
                  Program: '',
                  Type: 'Periodic',
                  Cycle: 'T#20MS',
                  Event: '',
                }
              );
            },
            moveUp(tasks, index) {
              return _.moveUp(tasks, index);
            },
            moveDown(tasks, index) {
              return _.moveDown(tasks, index);
            },
            remove(tasks, index) {
              _.remove(tasks, index);
            }
          },
          Variable: {
            getAll() {
              return Store.state.CurrentProject.Resource.Variables;
            },
            add(variables) {
              variables.push(
                {
                  Name: '',
                  DataType: 'BOOL',
                  Class: 'Global',
                  InitialValue: '',
                  Address: '',
                  Option: '',
                  Documentation: '',
                }
              );
            }
          }
        },
        Variable: {
          add(variables) {
            variables.push(
              {
                Name: '',
                DataType: 'BOOL',
                Class: 'Local',
                InitialValue: '',
                Address: '',
                Option: '',
                Documentation: '',
              }
            );
          },
          moveUp(variables, index) {
            return _.moveUp(variables, index);
          },
          moveDown(variables, index) {
            return _.moveDown(variables, index);
          },
          remove(variables, index) {
            _.remove(variables, index);
          }
        },
        Language: {
          FBD: {
            Network: {
              add(pou) {
                pou.Code.Networks.push({
                  Documentation: '',
                  Blocks: [],
                  Wires: []
                });
              },
              remove(pou, network) {
              },
              moveUp(pou, network) {
              },
              moveDown(pou, network) {
              }
            }
          }
        }
      },
      UI: {
        Console: {
          toggle() {
            if (Store.state.UI.Console.Visible) {
              Store.state.UI.Console.Visible = false;
            } else {
              Store.state.UI.SourceCode.Visible = false;
              Store.state.UI.Console.Visible = true;
            }
          },
          show() {
            Store.state.UI.SourceCode.Visible = false;
            Store.state.UI.Console.Visible = true;
          },
          hide() {
            Store.state.UI.Console.Visible = false;
          },
          get isVisible() {
            return Store.state.UI.Console.Visible;
          }
        },
        SourceCode: {
          toggle() {
            if (Store.state.UI.SourceCode.Visible) {
              Store.state.UI.SourceCode.Visible = false;
            } else {
              Store.state.UI.Console.Visible = false;
              Store.state.UI.SourceCode.Visible = true;
            }
          },
          show() {
            Store.state.UI.Console.Visible = false;
            Store.state.UI.SourceCode.Visible = true;
          },
          hide() {
            Store.state.UI.SourceCode.Visible = false;
          },
          get isVisible() {
            return Store.state.UI.SourceCode.Visible;
          }
        },
        Search: {
          show(type) {
            Store.state.UI.Search.Visible = true;
            Store.state.UI.Search.Type = type;
          },
          hide()  {
            Store.state.UI.Search.Visible = false;
            Store.state.UI.Search.Type = '';
          },
          get Type() {
            return Store.state.UI.Search.Type;
          },
          get Query() {
            return Store.state.UI.Search.Query;
          },
          set Query(query) {
            return Store.state.UI.Search.Query = query;
          },
          get isVisible() {
            return Store.state.UI.Search.Visible;
          }
        },
        get LastPath() {
          return Store.state.App.LastPath;
        },
        set LastPath(lastPath) {
          Store.state.App.LastPath = lastPath;
        }
      }
    };

  }
}

export default Core;