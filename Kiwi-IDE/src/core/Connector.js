import SSDP from 'node-ssdp';
import BlueBird from 'bluebird';
import Request from 'request';
import io from 'socket.io-client';
import _ from 'underscore';
import axios from 'axios';
import * as URI from "uri-js";
import { TouchBarSlider } from 'electron';

var request = BlueBird.promisifyAll(Request, {multiArgs: true});

class Connector {
  constructor() {
    this.SSDPClient = new SSDP.Client({ explicitSocketBind: true });
    this.SSDPClient.on('response', this.onSSDP.bind(this));
    this.Gateways = [];
    this.Socket = null;
    this.PLC = null;
  }

  // Methods.
  async search() {
    try {
      this.Gateways = [];
      this.SSDPClient.search('urn:schemas-upnp-org:service:kiwi-gateway:1');
      await new Promise(r => setTimeout(r, 3000))
      this.SSDPClient.stop();

      let urls = _.uniq(this.Gateways);
      let found = [];

      for(let i = 0; i < urls.length; i++) {
        try {
          let uri = URI.parse(urls[i]);
          let res = await axios({ method: 'get', url: urls[i], timeout: 1000 });
          let plc = res.data;
          found.push({
            PLC: plc,
            UUID: plc.UUID,
            IP: uri.host,
            Address: `${uri.scheme}://${uri.host}:${uri.port}/${plc.UUID}`
          });
        } catch(e) {
        }
      }
      return found;
    } catch(e) {
      return [];
    }
  }

  connect(plc, events) {
    var _this = this;
    this.PLC = plc;
    this.Socket = io(plc.Address, {
      autoConnect: false,
      reconnection: false
    });
    this.Socket.on('blocked', events.onBlocked);
    this.Socket.on('status', events.onStatus);
    this.Socket.on('connect', () => {
      events.onConnect(_this.Socket.connected);
    });
    this.Socket.on('error', () => {
      _this.Socket.disconnect();
    });
    this.Socket.on('disconnect', () => {
      events.onDisconnect();
    });
    this.Socket.on('close-connection', () => {
      _this.Socket.disconnect(true);
    });
    this.Socket.connect();
  }

  disconnect() {
    if (this.Socket) {
      this.Socket.emit('close-connection', {});
      this.Socket.disconnect(true);
    }
  }

  compile(st, project) {
    if (this.Socket) {
      console.log('Compile');
      console.log(st);
      this.Socket.emit('compile', { ST: st, Project: project});
    }
  }

  transfer() {
    if (this.Socket) {
      this.Socket.emit('transfer', {});
    }
  }

  start() {
    if (this.Socket) {
      this.Socket.emit('start', {});
    }
  }

  stop() {
    if (this.Socket) {
      this.Socket.emit('stop', {});
    }
  }

  // Events.
  onSSDP(headers, statusCode, rinfo) {
    this.Gateways.push(headers.LOCATION);
  }
};

export default new Connector();