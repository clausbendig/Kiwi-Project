import Store from './Store';

var Connectors = [];

// {
//   connect(address) {
//     this.WebAPI.connect(address);
//   },
//   disconnect() {
//     this.WebAPI.disconnect();
//   },
//   transfer(code) {
//     this.WebAPI.transfer(code);
//     Store.state.PLC.Compiling = true;
//   },
//   start() {
//   },
//   stop() {
//   },

//   get Connected() {
//     return Store.state.PLC.Connected;
//   },
//   Events: {

//   }
// }

const Events = {
  onConnected: () => {
    Store.state.PLC.Connected = true;
  },
  onDisconnected: () => {
    Store.state.PLC.Connected = false;
  },
  onError: () => {
  },
  onStarted: (data) => {
    Store.state.PLC.Running = true;
  },
  onStopped: (data) => {
    Store.state.PLC.Running = false;
  },
  onCompiled: (data) => {
    Store.state.PLC.Compiling = false;
  },
  onDebugged: (debugData) => {
  },
  onConsole: (consoleData) => {
  },
  onStatus: (statusData) => {
  }
};

const register = function(connector) {
  connect.Events = Events;
  Connectors.push(connector);
};


var PLC = {
  register: register;
};

export default PLC;