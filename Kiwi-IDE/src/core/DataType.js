/*
* Copyright (C) Claus Bendig
*/

'use strict'

import bigInt from 'big-integer';
import { sprintf } from 'sprintf-js';
import _ from 'underscore';

const DataTypes = {
  Void: [
    'VOID'
  ],
  Elementary: [
    'BOOL',
    'SINT',
    'INT',
    'DINT',
    'LINT',
    'USINT',
    'UINT',
    'UDINT',
    'ULINT',
    'BYTE',
    'WORD',
    'DWORD',
    'LWORD',
    'REAL',
    'LREAL'
  ],
  Complex: [
    'STRING',
    'DATE',
    'TOD',
    'DT',
    'TIME'
  ],
  Subrange: [
    'SINT',
    'INT',
    'DINT',
    'LINT',
    'USINT',
    'UINT',
    'UDINT',
    'ULINT'
  ]
};

const Minimum = {
  'SINT' : '-128',
  'INT'  : '-32768',
  'DINT' : '-2147483648',
  'LINT' : '-9223372036854775808',
  'USINT': '0',
  'UINT' : '0',
  'UDINT': '0',
  'ULINT': '0',
  'BYTE' : '0',
  'WORD' : '0',
  'DWORD': '0',
  'LWORD': '0',
  'DATE': {
    'year' : 1990,
    'month': 1,
    'day'  : 1
  },
  'TOD': {
    'hours': 0,
    'minutes': 0,
    'seconds': 0,
    'millis': 0
  },
  'DT': {
    'year' : 1990,
    'month': 1,
    'day'  : 1,
    'hours': 0,
    'minutes': 0,
    'seconds': 0,
    'millis': 0
  },
  'TIME': {
    'days': 0,
    'hours': 0,
    'minutes': 0,
    'seconds': 0,
    'millis': 0,
    'calc': -2147483648
  }
};

const Maximum = {
  'SINT' : '127',
  'INT'  : '32767',
  'DINT' : '2147483647',
  'LINT' : '9223372036854775807',
  'USINT': '255',
  'UINT' : '65535',
  'UDINT': '4294967295',
  'ULINT': '18446744073709551615',
  'BYTE' : '255',
  'WORD' : '65535',
  'DWORD': '4294967295',
  'LWORD': '18446744073709551615',
  'DATE': {
    'year' : 2168,
    'month': 12,
    'day'  : 31
  },
  'TOD': {
    'hours': 23,
    'minutes': 59,
    'seconds': 59,
    'millis': 999
  },
  'DT': {
    'year' : 2089,
    'month': 12,
    'day'  : 31,
    'hours': 23,
    'minutes': 59,
    'seconds': 59,
    'millis': 999
  },
  'TIME': {
    'days': 24,
    'hours': 23,
    'minutes': 59,
    'seconds': 59,
    'millis': 999,
    'calc': 2147483647
  }
};

const Sizes = {
  'SINT' : 1,
  'INT:' : 2,
  'DINT' : 4,
  'LINT' : 8,
  'USINT': 1,
  'UINT' : 2,
  'UDINT': 4,
  'ULINT': 8,
  'BYTE' : 1,
  'WORD' : 2,
  'DWORD': 4,
  'LWORD': 8
};

var daysInMonth = function (m, y) {
  switch (m) {
      case 1 :
          return (y % 4 == 0 && y % 100) || y % 400 == 0 ? 29 : 28;
      case 8 : case 3 : case 5 : case 10 :
          return 30;
      default :
          return 31
  }
};

var isValidDate = function (d, m, y) {
  m = parseInt(m, 10) - 1;
  return m >= 0 && m < 12 && d > 0 && d <= daysInMonth(m, y);
};

const isValidNumber = function(dataType, numberType, value) {
  var numbersFormat = '';
  var numbersMax = 0;

  if (numberType == 2)  {
    numbersFormat = '0-1';
    numbersMax = Sizes[dataType] * 8;
  }
  if (numberType == 8)  {
    numbersFormat = '0-7';
    numbersMax = Sizes[dataType] * 4;
  }
  if (numberType == 16)  {
    numbersFormat = '0-8A-Fa-f';
    numbersMax = Sizes[dataType] * 2;
  }

  var match = null;
  var extracted = null;
  if ((numberType == 2) || (numberType == 8) || (numberType == 16)) {
    var regex = '^(' + dataType + '#){0,1}' + numberType + '#(([' + numbersFormat + '])+){1,' + numbersMax.toString() + '}$';
    match = value.match(regex);
  } else {
    var regex = '^(' + dataType + '#){0,1}(([\+]|[\-]){0,1}(([0-9])+){1,})$';
    match = value.match(regex);
  }

  if (match == null) {
    return false;
  }
  var min = Minimum[dataType];
  var max = Maximum[dataType];
  
  var v = bigInt(match[2], numberType);
  if (v.geq(min) && v.leq(max)) {
    return true;
  } else {
    return false;
  }
};

const Tree = {
  'BOOL': {
    validate: function(value) {
      if ((value.toUpperCase() == 'FALSE') || (value.toUpperCase() == 'TRUE')) {
        return true;
      } else {
        return false;
      }
    },
    beautify: function(value) {
      return value.toUpperCase();
    }
  },
  'SINT': {
    validate: function(value) {
      var dual = isValidNumber('SINT', 2, value.toUpperCase());
      var octal = isValidNumber('SINT', 8, value.toUpperCase());
      var hex = isValidNumber('SINT', 16, value.toUpperCase());
      var dec = isValidNumber('SINT', 10, value.toUpperCase());
      return (dual || octal || hex || dec);
    },
    beautify: function(value) {
      return value.toUpperCase();
    }
  },
  'INT': {
    validate: function(value) {
      var dual = isValidNumber('INT', 2, value.toUpperCase());
      var octal = isValidNumber('INT', 8, value.toUpperCase());
      var hex = isValidNumber('INT', 16, value.toUpperCase());
      var dec = isValidNumber('INT', 10, value.toUpperCase());
      return (dual || octal || hex || dec);
    },
    beautify: function(value) {
      return value.toUpperCase();
    }
  },
  'DINT': {
    validate: function(value) {
      var dual = isValidNumber('DINT', 2, value.toUpperCase());
      var octal = isValidNumber('DINT', 8, value.toUpperCase());
      var hex = isValidNumber('DINT', 16, value.toUpperCase());
      var dec = isValidNumber('DINT', 10, value.toUpperCase());
      return (dual || octal || hex || dec);
    },
    beautify: function(value) {
      return value.toUpperCase();
    }
  },
  'LINT': {
    validate: function(value) {
      var dual = isValidNumber('LINT', 2, value.toUpperCase());
      var octal = isValidNumber('LINT', 8, value.toUpperCase());
      var hex = isValidNumber('LINT', 16, value.toUpperCase());
      var dec = isValidNumber('LINT', 10, value.toUpperCase());
      return (dual || octal || hex || dec);
    },
    beautify: function(value) {
      return value.toUpperCase();
    }
  },
  'USINT': {
    validate: function(value) {
      var dual = isValidNumber('USINT', 2, value.toUpperCase());
      var octal = isValidNumber('USINT', 8, value.toUpperCase());
      var hex = isValidNumber('USINT', 16, value.toUpperCase());
      var dec = isValidNumber('USINT', 10, value.toUpperCase());
      return (dual || octal || hex || dec);
    },
    beautify: function(value) {
      return value.toUpperCase();
    }
  },
  'UINT': {
    validate: function(value) {
      var dual = isValidNumber('UINT', 2, value.toUpperCase());
      var octal = isValidNumber('UINT', 8, value.toUpperCase());
      var hex = isValidNumber('UINT', 16, value.toUpperCase());
      var dec = isValidNumber('UINT', 10, value.toUpperCase());
      return (dual || octal || hex || dec);
    },
    beautify: function(value) {
      return value.toUpperCase();
    }
  },
  'UDINT': {
    validate: function(value) {
      var dual = isValidNumber('UDINT', 2, value.toUpperCase());
      var octal = isValidNumber('UDINT', 8, value.toUpperCase());
      var hex = isValidNumber('UDINT', 16, value.toUpperCase());
      var dec = isValidNumber('UDINT', 10, value.toUpperCase());
      return (dual || octal || hex || dec);
    },
    beautify: function(value) {
      return value.toUpperCase();
    }
  },
  'ULINT': {
    validate: function(value) {
      var dual = isValidNumber('ULINT', 2, value.toUpperCase());
      var octal = isValidNumber('ULINT', 8, value.toUpperCase());
      var hex = isValidNumber('ULINT', 16, value.toUpperCase());
      var dec = isValidNumber('ULINT', 10, value.toUpperCase());
      return (dual || octal || hex || dec);
    },
    beautify: function(value) {
      return value.toUpperCase();
    }
  },
  'BYTE': {
    validate: function(value) {
      var dual = isValidNumber('BYTE', 2, value.toUpperCase());
      var octal = isValidNumber('BYTE', 8, value.toUpperCase());
      var hex = isValidNumber('BYTE', 16, value.toUpperCase());
      var dec = isValidNumber('BYTE', 10, value.toUpperCase());
      return (dual || octal || hex || dec);
    },
    beautify: function(value) {
      return value.toUpperCase();
    }
  },
  'WORD': {
    validate: function(value) {
      var dual = isValidNumber('WORD', 2, value.toUpperCase());
      var octal = isValidNumber('WORD', 8, value.toUpperCase());
      var hex = isValidNumber('WORD', 16, value.toUpperCase());
      var dec = isValidNumber('WORD', 10, value.toUpperCase());
      return (dual || octal || hex || dec);
    },
    beautify: function(value) {
      return value.toUpperCase();
    }
  },
  'DWORD': {
    validate: function(value) {
      var dual = isValidNumber('DWORD', 2, value.toUpperCase());
      var octal = isValidNumber('DWORD', 8, value.toUpperCase());
      var hex = isValidNumber('DWORD', 16, value.toUpperCase());
      var dec = isValidNumber('DWORD', 10, value.toUpperCase());
      return (dual || octal || hex || dec);
    },
    beautify: function(value) {
      return value.toUpperCase();
    }
  },
  'LWORD': {
    validate: function(value) {
      var dual = isValidNumber('LWORD', 2, value.toUpperCase());
      var octal = isValidNumber('LWORD', 8, value.toUpperCase());
      var hex = isValidNumber('LWORD', 16, value.toUpperCase());
      var dec = isValidNumber('LWORD', 10, value.toUpperCase());
      return (dual || octal || hex || dec);
    },
    beautify: function(value) {
      return value.toUpperCase();
    }
  },
  'REAL': {
    validate: function(value) {
      return /^[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?$/.test(value);
    },
    beautify: function(value) {
      return value.toUpperCase();
    }
  },
  'LREAL': {
    validate: function(value) {
      return /^[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?$/.test(value);
    },
    beautify: function(value) {
      return value.toUpperCase();
    }
  },
  'TIME': {
    validate: function(value) {
      var v = value.toUpperCase().match('^((TIME#)|(T#))(\-){0,1}(([0-9]{1,})D){0,1}(([0-9]{1,})H){0,1}(([0-9]{1,})M){0,1}(([0-9]{1,})S){0,1}(([0-9]{1,3})MS){0,1}$');
      if (v == null) {
        return false;
      } else {
        var isNegative = true;
        var hasDays = false;
        var hasHours = false;
        var hasMinutes = false;
        var hasSeconds = false;
        var hasMillis = false;

        var days    = 0;
        var hours   = 0;
        var minutes = 0;
        var seconds = 0;
        var millis  = 0;

        if (v[4] == undefined) isNegative  = false;
        if (v[6] != undefined) {
          hasDays = true;
          days = parseInt(v[6]);
        }
        if (v[8] != undefined) {
          hasHours = true;
          hours = parseInt(v[8]);
        }
        if (v[10] != undefined) {
          hasMinutes = true;
          minutes = parseInt(v[10]);
        } 
        if (v[12] != undefined) {
          hasSeconds = true;
          seconds = parseInt(v[12]);
        }
        if (v[14] != undefined) {
          hasMillis = true;
          millis = parseInt(v[14]);
        } 

        if ((days >= Minimum['TIME']['days']) &&
            (days <= Maximum['TIME']['days']) &&
            (hours >= Minimum['TIME']['hours']) &&
            (hours <= Maximum['TIME']['hours']) &&
            (minutes >= Minimum['TIME']['minutes']) &&
            (minutes <= Maximum['TIME']['minutes']) &&
            (seconds >= Minimum['TIME']['seconds']) &&
            (seconds <= Maximum['TIME']['seconds']) &&
            (millis >= Minimum['TIME']['millis']) &&
            (millis <= Maximum['TIME']['millis'])) {

              var _days = bigInt(days);
              var _hours = bigInt(hours);
              var _minutes = bigInt(minutes);
              var _seconds = bigInt(seconds);
              var _millis = bigInt(millis);

              var calculated = (days * 24 * 60 * 60 * 1000) +
                               (    hours * 60 * 60 * 1000) +
                               (       minutes * 60 * 1000) +
                               (            seconds * 1000) +
                               (                    millis);
              
              if (isNegative) {
                if (-(calculated) < Minimum['TIME']['calc']) return false;
              } else {
                if (calculated > Maximum['TIME']['calc']) return false;
              }

              var test = '';
              test += hasDays    ? '1' : '0';
              test += hasHours   ? '1' : '0';
              test += hasMinutes ? '1' : '0';
              test += hasSeconds ? '1' : '0';
              test += hasMillis  ? '1' : '0';

              return /^[0]{0,}[1]{1,}[0]{0,}$/.test(test);
        }

        return false;
      }
    },
    beautify: function(value) {
      return value.toUpperCase();
    }
  },
  'DATE': {
    validate: function(value) {
      var v = value.toUpperCase().match('^((DATE#)|(D#))([0-9]{4})\-([0-9]{1,2})\-([0-9]{1,2})$');
      if (v == null) {
        return false
      } else {
        var year  = parseInt(v[4]);
        var month = parseInt(v[5]);
        var day   = parseInt(v[6]);
        if ((year >= Minimum['DATE']['year']) &&
            (year <= Maximum['DATE']['year']) &&
            (month >= Minimum['DATE']['month']) &&
            (month <= Maximum['DATE']['month']) &&
            (day >= Minimum['DATE']['day']) &&
            (day <= Maximum['DATE']['day'])) {
              if (isValidDate(day, month, year)) {
                return true;
              } else {
                return false;
              }
            return true;
          } else {
            return false;
          }
      }
    },
    beautify: function(value) {
      var v = value.toUpperCase().match('^((DATE#)|(D#))([0-9]{4})\-([0-9]{1,2})\-([0-9]{1,2})$');
      var prefix = v[1];
      var year  = parseInt(v[4]);
      var month = parseInt(v[5]);
      var day   = parseInt(v[6]);
      return sprintf("%s%d-%02d-%02d", prefix, year, month, day);
    }
  },
  'TOD': {
    validate: function(value) {
      var v = value.toUpperCase().match('^((TIME_OF_DAY#)|(TOD#))([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2})(\.([0-9]{1,3})){0,1}$');
      if (v == null) {
        return false;
      } else {
        var hours = parseInt(v[4]);
        var minutes = parseInt(v[5]);
        var seconds = parseInt(v[6]);
        var millis = 0;
        if (v[8] != undefined) millis = parseInt(v[8]);
        if ((hours >= Minimum['TOD']['hours']) &&
            (hours <= Maximum['TOD']['hours']) &&
            (minutes >= Minimum['TOD']['minutes']) &&
            (minutes <= Maximum['TOD']['minutes']) &&
            (seconds >= Minimum['TOD']['seconds']) &&
            (seconds <= Maximum['TOD']['seconds']) &&
            (millis >= Minimum['TOD']['millis']) &&
            (millis <= Maximum['TOD']['millis'])) {
            return true;
          } else {
            return false;
          }
      }
    },
    beautify: function(value) {
      var v = value.toUpperCase().match('^((TIME_OF_DAY#)|(TOD#))([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2})(\.([0-9]{1,3})){0,1}$');
      var prefix = v[1];
      var hours = parseInt(v[4]);
      var minutes = parseInt(v[5]);
      var seconds = parseInt(v[6]);
      var millis = 0;
      if (v[8] != undefined) millis = parseInt(v[8]);
      if (millis == 0) {
        return sprintf("%s%02d:%02d:%02d", prefix, hours, minutes, seconds);
      } else {
        return sprintf("%s%02d:%02d:%02d.%d", prefix, hours, minutes, seconds, millis);
      }
    }
  },
  'DT': {
    validate: function(value) {
      var v = value.toUpperCase().match('^((DATE_AND_TIME#)|(DT#))([0-9]{4})\-([0-9]{1,2})\-([0-9]{1,2})\-([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2})(\.([0-9]{1,3})){0,1}$')
      if (v == null) {
        return false;
      } else {
        var year  = parseInt(v[4]);
        var month = parseInt(v[5]);
        var day   = parseInt(v[6]);
        var hours = parseInt(v[7]);
        var minutes = parseInt(v[8]);
        var seconds = parseInt(v[9]);
        var millis = 0;
        if (v[11] != undefined) millis = parseInt(v[11]);
        if ((year >= Minimum['DT']['year']) &&
            (year <= Maximum['DT']['year']) &&
            (month >= Minimum['DT']['month']) &&
            (month <= Maximum['DT']['month']) &&
            (day >= Minimum['DT']['day']) &&
            (day <= Maximum['DT']['day']) &&
            (hours >= Minimum['DT']['hours']) &&
            (hours <= Maximum['DT']['hours']) &&
            (minutes >= Minimum['DT']['minutes']) &&
            (minutes <= Maximum['DT']['minutes']) &&
            (seconds >= Minimum['DT']['seconds']) &&
            (seconds <= Maximum['DT']['seconds']) &&
            (millis >= Minimum['DT']['millis']) &&
            (millis <= Maximum['DT']['millis'])) {
            if (isValidDate(day, month, year)) {
              return true;
            } else {
              return false;
            }
          } else {
            return false;
          }
      }
    },
    beautify: function(value) {
      var v = value.toUpperCase().match('^((DATE_AND_TIME#)|(DT#))([0-9]{4})\-([0-9]{1,2})\-([0-9]{1,2})\-([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2})(\.([0-9]{1,3})){0,1}$')
      var prefix = v[1];
      var year  = parseInt(v[4]);
      var month = parseInt(v[5]);
      var day   = parseInt(v[6]);
      var hours = parseInt(v[7]);
      var minutes = parseInt(v[8]);
      var seconds = parseInt(v[9]);
      var millis = 0;
      var millis = 0;
      if (v[11] != undefined) millis = parseInt(v[11]);
      if (millis == 0) {
        return sprintf("%s%d-%02d-%02d-%02d:%02d:%02d", prefix, year, month, day, hours, minutes, seconds);
      } else {
        return sprintf("%s%d-%02d-%02d-%02d:%02d:%02d.%d", prefix, year, month, day, hours, minutes, seconds, millis);
      }
    }
  },
  'STRING': {
    validate: function(value) {
      if (value.includes("'")) {
        return false;
      } else {
        if (value.length() <= 126) {
          return /^[\x00-\x7F]*$/.test(value);
        } else {
          return false;
        }
      }
    },
    beautify: function(value) {
      return value;
    }
  },
};

const isValidBase = function(dataType, value) {
  if (Tree.hasOwnProperty(dataType)) {
    return Tree[dataType].validate(value);
  } else {
    return false;
  }
};

const beautify = function(dataType, value) {
  if (Tree.hasOwnProperty(dataType)) {
    if (isValidBase(dataType, value)) {
      return Tree[dataType].beautify(value);
    } else {
      return value;
    }
  } else {
    return value;
  }
};

const isBaseType = function(dataType) {
  switch(dataType) {
    case 'BOOL':
      return true;
      break;

    case 'SINT':
      return true;
      break;

    case 'INT':
      return true;
      break;

    case 'DINT':
      return true;
      break;

    case 'LINT':
      return true;
      break;

    case 'USINT':
      return true;
      break;

    case 'UINT':
      return true;
      break;

    case 'UDINT':
      return true;
      break;

    case 'ULINT':
      return true;
      break;

    case 'BYTE':
      return true;
      break;

    case 'WORD':
      return true;
      break;

    case 'DWORD':
      return true;
      break;

    case 'LWORD':
      return true;
      break;

    case 'STRING':
      return true;
      break;

    case 'DATE':
      return true;
      break;

    case 'TOD':
      return true;
      break;

    case 'DT':
      return true;
      break;

    case 'TIME':
      return true;
      break;

    default:
      return false;
      break;
  }
};

const isUserDefined = function(dataType) {
  return !isBaseType(dataType);
};

const getUserDefined = function(name, dataTypes) {
  var index = _.findIndex(dataTypes, (dt) => {
    return (dt.Name == name);
  });
  return dataTypes[index];
}

const dataTypeIsDerived = function(lookFor, dataType, parent, dataTypes) {
  switch(dataType.DerivationType) {
    case 'Directly':
      if (isUserDefined(dataType.BaseType)) {
        if (dataType.BaseType == lookFor.Name) {
          return true;
        } else {
          var dt = getUserDefined(dataType.BaseType, dataTypes);
          return dataTypeIsDerived(lookFor, dt, dataType, dataTypes);
        }
      } else {
        return false;
      }
      break;

    case 'Array':
      if (isUserDefined(dataType.BaseType)) {
        if (dataType.BaseType == lookFor.Name) {
          return true;
        } else {
          var dt = getUserDefined(dataType.BaseType, dataTypes);
          return dataTypeIsDerived(lookFor, dt, dataType,dataTypes);
        }
      } else {
        return false;
      }
      break;

    case 'Structure':
      for (var i = 0; i < dataType.Members.length; i++) {
        if (isUserDefined(dataType.Members[i].BaseType)) {
          if (dataType.Members[i].BaseType == lookFor.Name) {
            return true;
          } else {
            var dt = getUserDefined(dataType.Members[i].BaseType, dataTypes);
            if (dataTypeIsDerived(lookFor, dt, dataType, dataTypes)) {
              return true;
            };
          }
        }
      }
      return false;
      break;

    default:
      return false;
      break;
  }
};

const unavailableUserDefined = function(lookFor, dataTypes) {
  var unavailable = [];
  for (var i = 0; i < dataTypes.length; i++) {
    if (dataTypeIsDerived(lookFor, dataTypes[i], null, dataTypes)) {
      unavailable.push(dataTypes[i]);
    };
  }
  return unavailable;
};

const availableUserDefined = function(dataType, dataTypes) {
  
  var unavailable = unavailableUserDefined(dataType, dataTypes);
  unavailable.push(dataType);
  var available = _.difference(dataTypes, unavailable);
  return available;
};

export default {
  isValidBase: isValidBase,
  beautify: beautify,
  Minimum: Minimum,
  Maximum: Maximum,
  BaseDataTypes: DataTypes,
  available: availableUserDefined,
  isBaseType: isBaseType,
  isUserDefined: isUserDefined
};