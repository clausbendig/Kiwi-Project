/*
* Copyright (C) Claus Bendig
*/

import _ from 'underscore';

const move = function (array, fromIndex, toIndex) {
  array.splice(toIndex, 0, array.splice(fromIndex, 1)[0] );
  return array;
};

const moveUp = function(array, index) {
  if ((array.length > 1) && (index > 0)) {
    move(array, index, index - 1);
    return index - 1;
  }
  return index;
};

const moveDown = function(array, index) {
  if ((array.length > 1) && (index < (array.length - 1))) {
    move(array, index, index + 1);
    return index + 1;
  }
  return index;
};

const remove = function(array, index) {
  if ((index >= 0) && (index < array.length) && (array.length > 0)) {
    array.splice(index, 1);
  }
}

const duplicates = function(array, attribute) {
  return _.chain(array).groupBy(attribute).filter(function(v){return v.length > 1}).flatten().value();
};

var EventBus = {
  install: function(Vue) {
    Vue.prototype.$eventBus = new Vue();
  }
}

var UnderscoreMixins = {
  move: move,
  moveUp: moveUp,
  moveDown: moveDown,
  remove: remove,
  duplicates: duplicates
};

export { UnderscoreMixins, EventBus };