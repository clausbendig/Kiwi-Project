/*
* Copyright (C) Claus Bendig
*/

'use strict'

import jetpack from 'fs-jetpack';
import Paths from './Paths'
import _ from 'underscore';

if (!jetpack.exists(Paths.Database)) {
  jetpack.write(Paths.Database, '[]');
  var Projects = [];
} else {
  var json = jetpack.read(Paths.Database);
  var Projects = JSON.parse(json);
}

const saveFile = function() {
  var json = JSON.stringify(Projects)
  jetpack.write(Paths.Database, json);
};

const add = function(project) {
  Projects.push(project);
  saveFile();
};

const remove = function(project) {
  var index = _.findIndex(Projects, (p) => {
    return (project.UUID == p.UUID);
  });
  Projects.splice(index, 1);
  saveFile();
};

const load = function(project) {
  var index = _.findIndex(Projects, (p) => {
    return (project.UUID == p.UUID);
  });
  return Projects[index];
};

const save = function(project) {
  var index = _.findIndex(Projects, (p) => {
    return (project.UUID == p.UUID);
  });
  Projects[index] = project;
  saveFile();
};

const getProjects = function() {
  return JSON.parse(JSON.stringify(Projects));
};

export default {
  getProjects: getProjects,
  add: add,
  remove: remove,
  load: load,
  save: save
};