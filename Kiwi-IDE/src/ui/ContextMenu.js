const contextMenu = function(context, event, callback) {
  var $contextMenu = $(context);

  function getMenuPosition(mouse, direction, scrollDir) {
    var win = $(window)[direction](),
        scroll = $(window)[scrollDir](),
        menu = $(context)[direction](),
        position = mouse + scroll;
                
    if (mouse + menu > win && menu < mouse) 
        position -= menu;
    
    return position;
  }   

  $contextMenu.css({
    display: "block",
    position: "absolute",
    left: getMenuPosition(event.clientX, 'width', 'scrollLeft'),
    top: getMenuPosition(event.clientY, 'height', 'scrollTop')
  });

  $contextMenu.on("click", "a", function(e) {
    $contextMenu.off("click", "a");
    $contextMenu.off("body");
    $contextMenu.hide();
    callback(e);
  });

  $('body').click(function () {
    $contextMenu.off("click", "a");
    $contextMenu.off("body");
    $(context).hide();
  });
};

export default contextMenu;
