/*
* Copyright (C) Claus Bendig
*/

const messages = {
    Projects: {
        Projects: 'Projekte',
        SearchProject: 'Suche Projekt',
        New: 'Neu',
        Name: 'Name',
        Edit: 'Bearbeiten',
        Export: 'Exportieren',
        Delete: 'Löschen',
        ImportProject: 'Importiere Projekt',
        ProjectName: 'Projektname *',
        Author: 'Autor',
        Company: 'Firma *',
        CompanyURL: 'Webadresse',
        ProductName: 'Produktname *',
        ProductVersion: 'Produktversion *',
        Description: 'Beschreibung',
        CreateProject: 'Erstelle Projekt'
    },
    Editor: {
        Console: {
            Title: 'Konsole'
        },
        SourceCode: {
            Title: 'Quelltext'
        },
        Navigation: {
            Add: 'Hinzufügen',
            OrganizationUnits: 'Organisationsbausteine',
            Parametrisation: 'Parametrisierung',
            Programs: 'Programme',
            Functions: 'Funktionen',
            DataTypes: 'Datentypen',
            Properties: 'Eigenschaften',
            Hardware: 'Hardware',
            Tasks: 'Aufgaben',
            GlobalVariables: 'Globale Variablen',
            Rename: 'Umbenennen',
            Clone: 'Klonen',
            Remove: 'Entfernen',
            IL: 'AWL',
            ST: 'ST',
            LD: 'KOP',
            FBD: 'FUP',
            SFC: 'AS'
        },
        Filemenu: {
            Searchbar: {
                SearchFunctions: 'Suche Funktion',
                SearchHardware: 'Suche Hardware',
            },
            ConnectToPLC: 'Mit SPS verbinden',
            DisconnectFromPLC: 'SPS-Verbindung trennen',
            BuildProject: 'Projekt kompilieren',
            TransferProject: 'Projekt übertragen',
            CompileProject: 'Projekt kompilieren',
            RunPLC: 'SPS starten',
            StopPLC: 'SPS stoppen',
            SaveProject: 'Projekt speichern',
            CloseProject: 'Projekt schließen',
            AddProgram: 'Programm hinzufügen',
            AddFunction: 'Funktion hinzufügen',
            AddDataType: 'Datentyp hinzufügen',
            AddResource: 'Ressource hinzufügen',
            ShowConsole: 'Zeige Konsole',
            HideConsole: 'Verstecke Konsole',
            ShowSourceCode: 'Zeige Quelltext',
            HideSourceCode: 'Verstecke Quelltext',
            FullScreen: 'Vollbild'
        },
        Properties: {
            Properties: 'Eigenschaften',
            ProjectName: 'Projektname *',
            Author: 'Autor',
            Company: 'Firma *',
            CompanyURL: 'Webadresse',
            ProductName: 'Produktname *',
            ProductVersion: 'Produktversion *',
            Description: 'Beschreibung',
            Apply: 'Übernehmen'
        },
        Hardware: {
            Hardware: 'Hardware',
            AddInterface: 'Schnittstelle hinzufügen',
            DragCPUHere: 'CPU hier hineinziehen.',
            RemoveCPU: 'CPU entfernen',
            ConfigureCPU: "CPU konfigurieren"
        },
        Sidebar: {
            Hardware: {
                Hardware: 'Hardware',
                CPUs: 'CPUs',
                Modules: 'Module'
            },
            Functions: {
                Standard: 'Standart',
                UserDefined: 'Benutzerdefiniert',
                Functions: 'Funktionen',
                FunctionBlocks: 'Funktionsblöcke',
                Bit: 'Bit',
                Time: 'Zeiten',
                Counter: 'Zähler',
                Math: 'Mathe',
                Comparators: 'Vergleicher',
                Shift: 'Verschieben',
                Selection: 'Auswahl',
                String: 'Zeichenketten',
                Regulation: 'Regelung',
                Additional: 'Zusätzliche',
                TypeConversion: 'Typumwandlung',
            },
        },
        Tasks: {
            Title: 'Aufgaben'
        },
        GlobalVariables: {
            Title: 'Globale Variablen'
        },
        DataType: {
            Title: 'Datentyp',
            Directly: 'Direkt',
            Subrange: 'Teilbereich',
            Enumeration: 'Aufzählung',
            Array: 'Array',
            Structure: 'Struktur',
            Derived: {
                Directly: {
                    VoidDataTypes: 'Kein Rückgabe',
                    ElementaryDataTypes: 'Elementare Datentypen',
                    ComplexDataTypes: 'Komplexe Datentypen',
                    UserDefinedDataTypes: 'Benutzerdefinierte Datentypen',
                    BaseDataType: 'Basis-Datentyp'
                },
                Subrange: {
                    BaseDataType: 'Basis-Datentyp',
                    IntegerDataTypes: 'Ganzzahlige Datentypen',
                    Minimum: 'Minimal',
                    Maximum: 'Maximal'
                },
                Array: {
                    BaseDataType: 'Basis-Datentyp',
                    Ranges: 'Bereiche'               
                }
            },
        },
        VariablesTable: {
            Title: 'Variablen'
        },
        Configuration: {
            Title: 'Konfiguration',
            GlobalVariables: 'Globale Variablen',
            Task: 'Aufgabe',
            Periodic: 'Zyklisch',
            Event: 'Ereignisgesteuert',
            Remove: 'Entfernen'
        }
    },
    Dialog: {
        Import: {
            AllProjectFiles: 'Alle Projektdateien'
        },
        EditImportedProject: {
            Title: 'Projekt bearbeiten',
            Body: 'Wollen Sie das importierte Projekt "{name}" bearbeiten?',
            ButtonNo: 'Nein',
            ButtonYes: 'Ja'
        },
        ProjectCorrupted: {
            Title: 'Projekt nicht bearbeitbar',
            Body: 'Konnte das Projekt "{name}" nicht öffnen, da das Projekt beschädigt ist.',
            ButtonOK: 'OK'
        },
        CouldntImport: {
            Title: 'Importfehler',
            Body: 'Konnte das Projekt "{file}" nicht importieren.',
            ButtonOK: 'OK'
        },
        CouldntCreateProject: {
            Title: 'Erstellungsfehler',
            Body: 'Konnte das Projekt "{name}" nicht erstellen.',
            ButtonOK: 'OK'
        },
        CouldntExport: {
            Title: 'Exportfehler',
            Body: 'Konnte das Projekt "{name}" nicht nach "{file}" exportieren.',
            ButtonOK: 'OK'
        },
        DeleteProject: {
            Title: 'Projekt löschen',
            Body: 'Wollen Sie wirklich das Projekt "{name}" löschen?',
            ButtonCancel: 'Abbrechen',
            ButtonDelete: 'Löschen'
        },
        CloseProject: {
            Title: 'Projekt schließen',
            Body: 'Wollen Sie die Änderung am Projekt speichern?',
            ButtonCancel: 'Abbrechen',
            ButtonNo: 'Nein',
            ButtonYes: 'Ja'
        },
        RemoveCPU: {
            Title: 'CPU entfernen',
            Body: 'Wollen Sie die CPU entfernen?',
            ButtonNo: 'Nein',
            ButtonYes: 'Ja'
        },
        AddInterface: {
            Title: 'Schnittstelle hinzufügen',
            Body: 'Welche Schnittstelle möchten Sie hinzufügen?',
            Interface: 'Schnittstelle',
            Protocol: 'Protokoll',
            ButtonCancel: 'Abbrechen',
            ButtonAdd: 'Hinzufügen'
        },
        RemoveInterface: {
            Title: 'Schnittstelle entfernen',
            Body: 'Wollen Sie die Schnittstelle "{name}" entfernen?',
            ButtonNo: 'Nein',
            ButtonYes: 'Ja'
        },
        NewProject: {
            Title: 'Programm',
            ButtonCancel: 'Abbrechen',
            ButtonApply: 'Übernehmen',
            Name: 'Name',
            Language: 'Sprache',
            IL: 'AWL',
            ST: 'ST',
            LD: 'KOP',
            FBD: 'FUP',
            SFC: 'AS'
        },
        AddProgram: {
            Title: 'Programm hinzufügen',
            ButtonCancel: 'Abbrechen',
            ButtonAdd: 'Hinzufügen',
            Name: 'Name',
            Language: 'Sprache',
            IL: 'AWL',
            ST: 'ST',
            LD: 'KOP',
            FBD: 'FUP',
            SFC: 'AS'
        },
        AddFunction: {
            Title: 'Funktion hinzufügen',
            ButtonCancel: 'Abbrechen',
            ButtonAdd: 'Hinzufügen',
            Name: 'Name',
            Language: 'Sprache',
            FunctionType: 'Funktionstyp',
            IL: 'AWL',
            ST: 'ST',
            LD: 'KOP',
            FBD: 'FUP',
            SFC: 'AS',
            Function: 'Funktion',
            FunctionBlock: 'Funktionsblock'
        },
        AddDataType: {
            Title: 'Datentyp hinzufügen',
            ButtonCancel: 'Abbrechen',
            ButtonAdd: 'Hinzufügen',
            Name: 'Name',
            Type: 'Typ',
            Directly: 'Direkt',
            Subrange: 'Teilbereich',
            Enumeration: 'Aufzählung',
            Array: 'Array',
            Structure: 'Struktur',
        },
        POURename: {
            TitleProgram: 'Programm umbenennen',
            TitleFunction: 'Funktion umbenennen',
            TitleFunctionBlock: 'Funktionsblock umbenennen',
            TitleDataType: 'Datentyp umbenennen',
            ButtonCancel: 'Abbrechen',
            ButtonRename: 'Umbenennen',
            Name: 'Name'
        },
        POUClone: {
            TitleProgram: 'Programm klonen',
            TitleFunction: 'Funktion klonen',
            TitleFunctionBlock: 'Funktionsblock klonen',
            TitleDataType: 'Datentyp klonen',
            ButtonCancel: 'Abbrechen',
            ButtonClone: 'Klonen',
            Name: 'Name'
        },
        POURemove: {
            TitleProgram: 'Programm entfernen',
            TitleFunction: 'Funktion entfernen',
            TitleFunctionBlock: 'Funktionsblock entfernen',
            TitleDataType: 'Datentyp entfernen',
            BodyProgram: 'Wollen Sie wirklich das Programm "{name}" löschen?',
            BodyFunction: 'Wollen Sie wirklich die Funktion "{name}" löschen?',
            BodyFunctionBlock: 'Wollen Sie wirklich den Funktionsblock "{name}" löschen?',
            BodyDataType: 'Wollen Sie wirklich den Datentyp "{name}" löschen?',
            ButtonCancel: 'Abbrechen',
            ButtonRemove: 'Entfernen',
        },
        ConnectToPLC: {
            Title: 'Mit SPS verbinden',
            Network: 'Netzwerk',
            PLC: 'SPS',
            Address: 'Adresse',
            UUID: 'UUID',
            ButtonCancel: 'Abbrechen',
            ButtonConnect: 'Verbinden',
        }
    },
    Table: {
        Task: {
            Name: 'Name',
            Program: 'Programm',
            Type: 'Typ',
            IntervalEvent: 'Intervall/Ereignis',
            Priority: 'Priorität',
            Periodic: 'Zyklisch',
            Event: "Ereignisgesteuert" 
        },
        Structure: {
            ElementaryDataTypes: 'Elementare Datentypen',
            ComplexDataTypes: 'Komplexe Datentypen',
            UserDefinedDataTypes: 'Benutzerdefinierte Datentypen',
            Name: 'Name',
            DataType: 'Datentyp',
            InitialValue: 'Initialwert'
        },
        Enumeration: {
            Name: 'Name',
            Standart: '#'
        },
        Variables: {
            Name: 'Name',
            DataType: 'Datentyp',
            Class: 'Klasse',
            Address: 'Adresse',
            Option: 'Option',
            InitialValue: 'Startwert',
            Comment: 'Kommentar',
        }
    },
    Element: {
        Option: {
            Constant: 'Constant',
            Retain: 'Retain',
            NonRetain: 'Non-Retain',
            Persistent: 'Persistent',
            NonPersistent: 'Non-Persistent'
        },
        Class: {
            Input: 'Eingang',
            Output: 'Ausgang',
            InputOutput: 'Ein- und Ausgang',
            External: 'Extern',
            Local: 'Lokal',
            Temporary: 'Temporär',
            Global: 'Global'
        }
    },
    About: {
        Title: 'Über',
        KiwiIDE: 'Kiwi-IDE',
        Third: 'Dritte',
        Text: 'Kiwi-IDE ist eine Entwicklungsumgebung um IEC61131-3 Anwendungen zu entwickeln.',
    },
    Notification: {
        POURemoved: {
            Program: 'Das Programm "{name}" wurde entfernt.',
            Function: 'Die Funktion "{name} wurde entfernt.',
            FunctionBlock: 'Der Funktionsblock "{name}" wurde entfernt.',
            DataType: 'Der Datentyp "{name}" wurde entfernt.',
        },
        PLC: {
            Connected: 'SPS-Verbindung wurde hergestellt.',
            Disconnected: 'SPS-Verbindung wurde getrennt.',
            CouldntConnect: 'Konnte keine SPS-Verbindung herstellen',
            SomebodyAlreadyConnected: 'Jemand anderes ist schon mit der SPS verbunden.',
            CompilingFailed: 'Projekt-Kompilierung fehlgeschlagen.',
            CompilingSuccessful: 'Projekt wurde kompiliert.',
            TransferingFailed: 'Projekt-Übertragung fehlgeschlagen.',
            TransferingSuccessful: 'Projekt wurde übertragen.',
        }
    }
};

export default messages;