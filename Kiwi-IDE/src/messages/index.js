/*
* Copyright (C) Claus Bendig
*/

'use strict'

import en from './en';
import de from './de';

const messages = {
  en: en,
  de: de
};

export default messages;