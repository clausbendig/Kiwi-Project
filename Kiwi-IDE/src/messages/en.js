/*
* Copyright (C) Claus Bendig
*/

const messages = {
    Projects: {
        Projects: 'Projects',
        SearchProject: 'Search project',
        New: 'New',
        Name: 'Name',
        Edit: 'Edit',
        Export: 'Export',
        Delete: 'Delete',
        ImportProject: 'Import project',
        ProjectName: 'Project-Name *',
        Author: 'Author',
        Company: 'Company *',
        CompanyURL: 'Company-URL',
        ProductName: 'Product-Name *',
        ProductVersion: 'Product-Version *',
        Description: 'Description',
        CreateProject: 'CreateProject'
    },
    Editor: {
        Console: {
            Title: 'Console'
        },
        SourceCode: {
            Title: 'Source code'
        },
        Navigation: {
            Add: 'Add',
            OrganizationUnits: 'Organization Units',
            Parametrisation: 'Parametrisation',
            Programs: 'Programs',
            Functions: 'Functions',
            DataTypes: 'Data types',
            Properties: 'Properties',
            Hardware: 'Hardware',
            Tasks: 'Tasks',
            GlobalVariables: 'Globale variables',
            Rename: 'Rename',
            Clone: 'Clone',
            Remove: 'Remove',
            IL: 'IL',
            ST: 'ST',
            LD: 'LD',
            FBD: 'FBD',
            SFC: 'SFC'
        },
        Filemenu: {
            Searchbar: {
                SearchFunctions: 'Search functions',
                SearchHardware: 'Search hardware',
            },
            ConnectToPLC: 'Connect to PLC',
            DisconnectFromPLC: 'Disconnect from PLC',
            BuildProject: 'Build project',
            TransferProject: 'Transfer project',
            CompileProject: 'Compile project',
            RunPLC: 'Run PLC',
            StopPLC: 'Stop PLC',
            SaveProject: 'Save project',
            CloseProject: 'Close project',
            AddProgram: 'Add program',
            AddFunction: 'Add function',
            AddDataType: 'Add data type',
            AddResource: 'Add resource',
            ShowConsole: 'Show console',
            HideConsole: 'Hide console',
            ShowSourceCode: 'Show source code',
            HideSourceCode: 'Hide source code',
            FullScreen: 'Full screen'
        },
        Properties: {
            Properties: 'Properties',
            ProjectName: 'Project-Name *',
            Author: 'Author',
            Company: 'Company *',
            CompanyURL: 'Company-URL',
            ProductName: 'Product-Name *',
            ProductVersion: 'Product-Version *',
            Description: 'Description',
            Apply: 'CreateProject'
        },
        Hardware: {
            Hardware: 'Hardware',
            AddInterface: 'Add interface',
            DragCPUHere: 'Drag CPU in here.',
            RemoveCPU: 'Remove CPU',
            ConfigureCPU: "Configure CPU"
        },
        Sidebar: {
            Hardware: {
                Hardware: 'Hardware',
                CPUs: 'CPUs',
                Modules: 'Module'
            },
            Functions: {
                Standard: 'Standard',
                UserDefined: 'User defined',
                Functions: 'Functions',
                FunctionBlocks: 'Function blocks',
                Bit: 'Bit',
                Time: 'Time',
                Counter: 'Counter',
                Math: 'Math',
                Comparators: 'Comparators',
                Shift: 'Shift',
                Selection: 'Selection',
                String: 'String',
                Regulation: 'Regulation',
                Additional: 'Additional',
                TypeConversion: 'Type conversion',
            },
        },
        Tasks: {
            Title: 'Tasks'
        },
        GlobalVariables: {
            Title: 'Global variables'
        },
        DataType: {
            Title: 'Data type',
            Directly: 'Directly',
            Subrange: 'Subrange',
            Enumeration: 'Enumeration',
            Array: 'Array',
            Structure: 'Structure',
            Derived: {
                Directly: {
                    VoidDataTypes: 'No return',
                    ElementaryDataTypes: 'Elementary data types',
                    ComplexDataTypes: 'Complex data types',
                    UserDefinedDataTypes: 'User defined data types',
                    BaseDataType: 'Base data type',
                },
                Subrange: {
                    BaseDataType: 'Base data type',
                    IntegerDataTypes: 'Integer data type',
                    Minimum: 'Minimum',
                    Maximum: 'Maximum'
                },
                Array: {
                    BaseDataType: 'Base data type',
                    Ranges: 'Ranges'               
                }
            }
        },
        VariablesTable: {
            Title: 'Variables'
        },
        Configuration: {
            Title: 'Configuration',
            GlobalVariables: 'Global variables',
            Task: 'Task',
            Periodic: 'Periodic',
            Event: 'Event driven',
            Remove: 'Remove'
        }
    },
    Dialog: {
        Import: {
            AllProjectFiles: 'All Project Files'
        },
        EditImportedProject: {
            Title: 'Edit project',
            Body: 'Do you want to edit the imported project "{name}"?',
            ButtonNo: 'No',
            ButtonYes: 'Yes'
        },
        ProjectCorrupted: {
            Title: 'Project not editable',
            Body: 'Couldn\'t open the project "{name}", because it is corrupted.',
            ButtonOK: 'OK'
        },
        CouldntCreateProject: {
            Title: 'Creation error',
            Body: 'Couldn\'t create the project "{name}".',
            ButtonOK: 'OK'
        },
        CouldntImport: {
            Title: 'Import error',
            Body: 'Couldn\'t import the project "{file}".',
            ButtonOK: 'OK'
        },
        CouldntExport: {
            Title: 'Export error',
            Body: 'Couldn\'t export the project "{name}" to "{file}".',
            ButtonOK: 'OK'
        },
        DeleteProject: {
            Title: 'Delete project',
            Body: 'Do you really want to delete the project "{name}" ?',
            ButtonCancel: 'Cancel',
            ButtonDelete: 'Delete',
        },
        CloseProject: {
            Title: 'Close project',
            Body: 'Do you want to save the project changes?',
            ButtonCancel: 'Cancel',
            ButtonNo: 'No',
            ButtonYes: 'Yes'
        },
        RemoveCPU: {
            Title: 'Remove CPU',
            Body: 'Do you want to remove the CPU?',
            ButtonNo: 'No',
            ButtonYes: 'Yes'
        },
        AddInterface: {
            Title: 'Add interface',
            Body: 'Which interface do you like to add?',
            Interface: 'Interface',
            Protocol: 'Protocol',
            ButtonCancel: 'Cancel',
            ButtonAdd: 'Add'               
        },
        RemoveInterface: {
            Title: 'Remove interface',
            Body: 'Do you want to remove the interface "{name}" ?',
            ButtonNo: 'No',
            ButtonYes: 'Yes'
        },
        NewProject: {
            Title: 'Program',
            ButtonCancel: 'Cancel',
            ButtonApply: 'Apply',
            Name: 'Name',
            Language: 'Language',
            IL: 'IL',
            ST: 'ST',
            LD: 'LD',
            FBD: 'FBD',
            SFC: 'SFC'
        },
        AddProgram: {
            Title: 'Add program',
            ButtonCancel: 'Cancel',
            ButtonAdd: 'Add',
            Name: 'Name',
            Language: 'Language',
            IL: 'IL',
            ST: 'ST',
            LD: 'LD',
            FBD: 'FBD',
            SFC: 'SFC'
        },
        AddFunction: {
            Title: 'Add function',
            ButtonCancel: 'Cancel',
            ButtonAdd: 'Add',
            Name: 'Name',
            Language: 'Language',
            FunctionType: 'Function type',
            IL: 'IL',
            ST: 'ST',
            LD: 'LD',
            FBD: 'FBD',
            SFC: 'SFC',
            Function: 'Function',
            FunctionBlock: 'Function-Block'
        },
        AddDataType: {
            Title: 'Add data type',
            ButtonCancel: 'Cancel',
            ButtonAdd: 'Add',
            Name: 'Name',
            Type: 'Type',
            Directly: 'Directly',
            Subrange: 'Subrange',
            Enumeration: 'Enumeration',
            Array: 'Array',
            Structure: 'Structure' 
        },
        POURename: {
            TitleProgram: 'Rename program',
            TitleFunction: 'Rename function',
            TitleFunctionBlock: 'Rename function-block',
            TitleDataType: 'Rename data type',
            ButtonCancel: 'Cancel',
            ButtonRename: 'Rename',
            Name: 'Name'
        },
        POUClone: {
            TitleProgram: 'Clone program',
            TitleFunction: 'Clone function',
            TitleFunctionBlock: 'Rename function-block',
            TitleDataType: 'Rename data-type',
            ButtonCancel: 'Cancel',
            ButtonClone: 'Clone',
            Name: 'Name'
        },
        POURemove: {
            TitleProgram: 'Remove program',
            TitleFunction: 'Remove function',
            TitleFunctionBlock: 'Remove function-block',
            TitleDataType: 'Remove data-type',
            BodyProgram: 'Do you really want to remove the program "{name}" ?',
            BodyFunction: 'Do you really want to remove the function "{name}" ?',
            BodyFunctionBlock: 'Do you really want to remove the function-block "{name}" ?',
            BodyDataType: 'Do you really want to remove the data type "{name}" ?',
            ButtonCancel: 'Cancel',
            ButtonRefresh: 'Refresh',
            ButtonRemove: 'Remove',
        },
        ConnectToPLC: {
            Title: 'Connect to PLC',
            Network: 'Network',
            PLC: 'PLC',
            Address: 'Address',
            UUID: 'UUID',
            ButtonCancel: 'Cancel',
            ButtonConnect: 'Connect',
        }
    },
    Table: {
        Task: {
            Name: 'Name',
            Program: 'Program',
            Type: 'Type',
            IntervalEvent: 'Interval/Event',
            Priority: 'Priority',
            Periodic: 'Periodic',
            Event: "Event driven"
        },
        Structure: {
            ElementaryDataTypes: 'Elementary data types',
            ComplexDataTypes: 'Complex data types',
            UserDefinedDataTypes: 'User defined data types',
            Name: 'Name',
            DataType: 'Data type',
            InitialValue: 'Initial value'
        },
        Enumeration: {
            Name: 'Name',
            Standart: '#'
        },
        Variables: {
            Name: 'Name',
            DataType: 'Data type',
            Class: 'Class',
            Address: 'Address',
            Option: 'Option',
            InitialValue: 'Initial value',
            Comment: 'Comment'
        }
    },
    Element: {
        Option: {
            Constant: 'Constant',
            Retain: 'Retain',
            NonRetain: 'Non-Retain',
            Persistent: 'Persistent',
            NonPersistent: 'Non-Persistent'
        },
        Class: {
            Input: 'Input',
            Output: 'Output',
            InputOutput: 'Input and Output',
            External: 'External',
            Local: 'Local',
            Temporary: 'Temporary',
            Global: 'Global'
        }
    },
    About: {
        Title: 'About',
        KiwiIDE: 'Kiwi-IDE',
        Third: 'Third',
        Text: 'Kiwi-IDE is a development environment to develop IEC61131-1 applications.',
    },
    Notification: {
        POURemoved: {
            Program: 'The program "{name}" was removed.',
            Function: 'The function "{name} was removed.',
            FunctionBlock: 'The funktion block "{name}" was removed.',
            DataType: 'The data type "{name}" was removed.',
        },
        PLC: {
            Connected: 'PLC connection was established.',
            Disconnected: 'PLC connection was terminated.',
            CouldntConnect: 'Couldn\'t connect to PLC',
            SomebodyAlreadyConnected: 'Somebody is already connected with the PLC.',
            CompilingFailed: 'Project compiling failed.',
            CompilingSuccessful: 'Compiling successful.',
            TransferingFailed: 'Transfering project failed.',
            TransferingSuccessful: 'Transfering project was successfully.',
        }
    }
};

export default messages;