/*
* Copyright (C) Claus Bendig
*/

'use strict'

// Import CSS files.
// import './../node_modules/jquery.tabulator/dist/css/bootstrap/tabulator_bootstrap.css';
// import './../node_modules/bootstrap-table/dist/bootstrap-table.css';
// import './../node_modules/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css';
import 'codemirror/lib/codemirror.css';

import '@fortawesome/fontawesome-free/css/all.css';
import 'roboto-fontface/css/roboto/roboto-fontface.css';
import 'vue2-animate/dist/vue2-animate.css';
import './design/app.less';

// Import third JS files.
import { remote } from 'electron';
import 'bootstrap3';
import 'bootstrap-select';
import 'bootstrap-notify';
import Vue from 'vue';
import VueRouter from 'vue-router';
import VueI18N from 'vue-i18n';
import VueDragAndDrop from 'vue-drag-drop';
import VeeValidate from 'vee-validate';
import VueCodemirror from 'vue-codemirror';
import VueProgressBar from 'vue-progressbar';
import Monaco from './third/monaco-editor-iec61131/Monaco.vue';

// Use the Vue development tool in development mode.
if (process.env.NODE_ENV == 'development') {
  Vue.config.devtools = true;
  // Vue.config.debug = false;
  Vue.config.silent = true;
}

// Import translations.
import messages from './messages';

// Import Project-Manager.
import ProjectManager from './core/ProjectManager';

// Import UI elements.
//
// Data types.
import ArrayDataType       from './ui/controls/data-types/Array.vue';
import DirectlyDataType    from './ui/controls/data-types/Directly.vue';
import EnumerationDataType from './ui/controls/data-types/Enumeration.vue';
import StructureDataType   from './ui/controls/data-types/Structure.vue';
import SubrangeDataType    from './ui/controls/data-types/Subrange.vue';

// Languages.
import FunctionBlockDiagram    from './ui/controls/languages/FunctionBlockDiagram.vue';
import InstructionList         from './ui/controls/languages/InstructionList.vue';
import LadderDiagram           from './ui/controls/languages/LadderDiagram.vue';
import SequentialFunctionChart from './ui/controls/languages/SequentialFunctionChart.vue';
import StructuredText          from './ui/controls/languages/StructuredText.vue';

// Tables.
import BaseTable              from './ui/controls/tables/BaseTable.vue';
import BaseTableHeader        from './ui/controls/tables/BaseTableHeader.vue';
import BaseTableBody          from './ui/controls/tables/BaseTableBody.vue';
import EnumerationTable       from './ui/controls/tables/EnumerationTable.vue';
import StructureTable         from './ui/controls/tables/StructureTable.vue';
import TaskTable              from './ui/controls/tables/TaskTable.vue';
import VariablesTable         from './ui/controls/tables/VariablesTable.vue';
import NetworkPLCTable        from './ui/controls/tables/NetworkPLCTable.vue';

// Import layout controls.
import FileMenu          from './ui/controls/layout/FileMenu.vue';
import Navigation        from './ui/controls/layout/Navigation.vue';
import NewProject        from './ui/controls/layout/NewProject.vue';
import Projects          from './ui/controls/layout/Projects.vue';
import Sidebar           from './ui/controls/layout/Sidebar.vue';
import HardwareSidebar   from './ui/controls/layout/Hardware.vue';
import Functions         from './ui/controls/layout/Functions.vue';

// Import single elements.
import Selection              from './ui/controls/single-elements/Selection.vue';
import Name                   from './ui/controls/single-elements/Name.vue';
import InitialValue           from './ui/controls/single-elements/InitialValue.vue';
import DataTypeSingleElement  from './ui/controls/single-elements/DataType.vue';
import Priority               from './ui/controls/single-elements/Priority.vue';
import Member                 from './ui/controls/single-elements/Member.vue';
import Documentation          from './ui/controls/single-elements/Documentation.vue';
import Option                 from './ui/controls/single-elements/Option.vue';
import Address                from './ui/controls/single-elements/Address.vue';
import Class                  from './ui/controls/single-elements/Class.vue';
import NetworkFBD             from './ui/controls/single-elements/NetworkFBD.vue';
import BlockFBD               from './ui/controls/single-elements/BlockFBD.vue';

// Import dialogs.
import CloseProject         from './ui/dialogs/CloseProject.vue';
import CouldntExportProject from './ui/dialogs/CouldntExportProject.vue';
import CouldntCreateProject from './ui/dialogs/CouldntCreateProject.vue';
import CouldntImportProject from './ui/dialogs/CouldntImportProject.vue';
import EditImportedProject  from './ui/dialogs/EditImportedProject.vue';
import DeleteProject        from './ui/dialogs/DeleteProject.vue';
import AddProgram           from './ui/dialogs/AddProgram.vue';
import AddFunction          from './ui/dialogs/AddFunction.vue';
import AddDataType          from './ui/dialogs/AddDataType.vue';
import ProjectCorrupted     from './ui/dialogs/ProjectCorrupted.vue';
import POURename            from './ui/dialogs/POURename.vue';
import POUClone             from './ui/dialogs/POUClone.vue';
import POURemove            from './ui/dialogs/POURemove.vue';
import NewProjectDialog     from './ui/dialogs/NewProject.vue';
import ConnectToPLC         from './ui/dialogs/ConnectToPLC.vue';

// Import main views.
import IL              from './ui/main/IL.vue';
import ST              from './ui/main/ST.vue';
import LD              from './ui/main/LD.vue';
import FBD             from './ui/main/FBD.vue';
import SFC             from './ui/main/SFC.vue';
import DataType        from './ui/main/DataType.vue';
import Tasks           from './ui/main/Tasks.vue';
import GlobalVariables from './ui/main/GlobalVariables.vue';
import Properties      from './ui/main/Properties.vue';
import Hardware        from './ui/main/Hardware.vue';
import Console         from './ui/main/Console.vue';
import SourceCode      from './ui/main/SourceCode.vue';

// Import pages.
import ProjectsPage from './ui/pages/Projects.vue';
import EditorPage   from './ui/pages/Editor.vue';
import AboutPage    from './ui/pages/About.vue';

// Import Vue-App.
import App from './ui/App.vue';

// Import Core.
import Core from './core/Core';

// Import from Utils.
import { UnderscoreMixins, EventBus } from './core/Utils';

// Import underscore.
import _ from 'underscore';

// Append mixins.
_.mixin(UnderscoreMixins);

// Store.
import Store from './core/Store';
Store.state.Projects = ProjectManager.getProjects();

// Use Vue plugins.
Vue.use(VueRouter);
Vue.use(VueI18N);
Vue.use(VueDragAndDrop);
Vue.use(VeeValidate);
Vue.use(VueProgressBar, {
  color: '#88939C',
  failedColor: 'red',
  height: '2px'
});
Vue.use(Core, Store);
Vue.use(EventBus);
Vue.use(VueCodemirror);

// Internationalzation
const i18n = new VueI18N({
  locale: remote.app.getLocale() .split('-')[0],
  fallbackLocale: 'en',
  messages
});


// Set UIs as components.
//
// Data types.
Vue.component('array-data-type',       ArrayDataType);
Vue.component('directly-data-type',    DirectlyDataType);
Vue.component('enumeration-data-type', EnumerationDataType);
Vue.component('structure-data-type',   StructureDataType);
Vue.component('subrange-data-type',    SubrangeDataType);

// Languages.
Vue.component('function-block-diagram',    FunctionBlockDiagram);
Vue.component('instruction-list',          InstructionList);
Vue.component('ladder-diagram',            LadderDiagram);
Vue.component('sequential-function-chart', SequentialFunctionChart);
Vue.component('structured-text',           StructuredText);

// Tables.
Vue.component('base-table',        BaseTable);
Vue.component('base-table-header', BaseTableHeader);
Vue.component('base-table-body',   BaseTableBody);
Vue.component('enumeration-table', EnumerationTable);
Vue.component('structure-table',   StructureTable);
Vue.component('task-table',        TaskTable);
Vue.component('variables-table',   VariablesTable);
Vue.component('network-plc-table', NetworkPLCTable);

// Layout controls.
Vue.component('file-menu',          FileMenu);
Vue.component('navigation',         Navigation);
Vue.component('new-project',        NewProject);
Vue.component('projects',           Projects);
Vue.component('sidebar',            Sidebar);
Vue.component('hardware-sidebar',   HardwareSidebar);
Vue.component('functions',          Functions);

// Single elements.
Vue.component('selection',        Selection);
Vue.component('name',             Name);
Vue.component('initial-value',    InitialValue);
Vue.component('data-type',        DataTypeSingleElement);
Vue.component('priority',         Priority);
Vue.component('member',           Member);
Vue.component('documentation',    Documentation);
Vue.component('variable-option',  Option);
Vue.component('variable-address', Address);
Vue.component('variable-class',   Class);
Vue.component('network-fbd',      NetworkFBD);
Vue.component('block-fbd',        BlockFBD);

// Dialogs.
Vue.component('close-project-dialog',          CloseProject);
Vue.component('couldnt-export-project-dialog', CouldntExportProject);
Vue.component('couldnt-create-project-dialog', CouldntCreateProject);
Vue.component('couldnt-import-project-dialog', CouldntImportProject);
Vue.component('delete-project-dialog',         DeleteProject);
Vue.component('edit-imported-project-dialog',  EditImportedProject);
Vue.component('add-program-dialog',            AddProgram);
Vue.component('add-function-dialog',           AddFunction);
Vue.component('add-datatype-dialog',           AddDataType);
Vue.component('project-corrupted-dialog',      ProjectCorrupted);
Vue.component('pou-rename-dialog',             POURename);
Vue.component('pou-clone-dialog',              POUClone);
Vue.component('pou-remove-dialog',             POURemove);
Vue.component('new-project-dialog',            NewProjectDialog);
Vue.component('connect-to-plc-dialog',         ConnectToPLC);

// Monaco.
Vue.component('monaco-editor', Monaco);

const Router = new VueRouter({
  routes: [
    { name: 'Projects', path: '/',     component: ProjectsPage },
    { name: 'About', path: '/About',   component: AboutPage },
    { name: 'Editor', path: '/Editor', component: EditorPage, children: [
      { path: "IL/:unit/:type",        component: IL },
      { path: "ST/:unit/:type",        component: ST },
      { path: "LD/:unit/:type",        component: LD },
      { path: "FBD/:unit/:type",       component: FBD },
      { path: "SFC/:unit/:type",       component: SFC },
      { path: "DataType/:unit",        component: DataType },
      { path: 'Tasks',                 component: Tasks },
      { path: 'GlobalVariables',       component: GlobalVariables },
      { path: 'Properties',            component: Properties },
      { path: 'Hardware',              component: Hardware },
      { path: 'Console',               component: Console },
      { path: 'SourceCode',            component: SourceCode },
    ]}
  ]
});

Store.state.Projects = ProjectManager.Projects;

new Vue({
  el: '#app',
  store: Store,
  router: Router,
  render: h => h(App),
  mounted: function() {
    remote.getCurrentWindow().show();
  },
  i18n
});
