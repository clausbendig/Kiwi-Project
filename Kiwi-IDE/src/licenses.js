/*
* Copyright (C) Claus Bendig
*/
'use strict'

import 'bootstrap-select/less/bootstrap-select.less';
import 'bootstrap3/less/bootstrap.less';
import '@fortawesome/fontawesome-free/css/all.css';
import 'roboto-fontface/css/roboto/roboto-fontface.css';
import 'vue2-animate/dist/vue2-animate.css';
import './design/app.less';

import 'bootstrap3';