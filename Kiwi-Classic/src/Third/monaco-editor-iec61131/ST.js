const Configuration = {
	comments: {
		lineComment: '//',
		blockComment: ['(*', '*)'],
	},
	brackets: [
		['{', '}'],
		['[', ']'],
		['(', ')'],
		['VAR', 'END_VAR'],
		['VAR_INPUT', 'END_VAR'],
		['VAR_OUTPUT', 'END_VAR'],
		['VAR_IN_OUT', 'END_VAR'],
		['VAR_TEMP', 'END_VAR'],
		['VAR_GLOBAL', 'END_VAR'],
		['VAR_ACCESS', 'END_VAR'],
		['VAR_EXTERNAL', 'END_VAR'],
		['TYPE', 'END_TYPE'],
		['STRUCT', 'END_STRUCT'],
		['PROGRAM', 'END_PROGRAM'],
		['FUNCTION', 'END_FUNCTION'],
		['FUNCTION_BLOCK', 'END_FUNCTION_BLOCK'],
		['ACTION', 'END_ACTION'],
		['STEP', 'END_STEP'],
		['INITIAL_STEP', 'END_STEP'],
		['TRANSACTION', 'END_TRANSACTION'],
		['CONFIGURATION', 'END_CONFIGURATION'],
		['TCP', 'END_TCP'],
		['RESOURCE', 'END_RESOURCE'],
		['CHANNEL', 'END_CHANNEL'],
		['LIBRARY', 'END_LIBRARY'],
		['FOLDER', 'END_FOLDER'],
		['BINARIES', 'END_BINARIES'],
		['INCLUDES', 'END_INCLUDES'],
		['SOURCES', 'END_SOURCES']
	],
	autoClosingPairs: [
		{ open: '[', close: ']' },
		{ open: '{', close: '}' },
		{ open: '(', close: ')' },
		{ open: '/*', close: '*/' },
		{ open: '\'', close: '\'', notIn: ['string_sq'] },
		{ open: '"', close: '"', notIn: ['string_dq'] },
		{ open: 'VAR', close: 'END_VAR'},
		{ open: 'VAR_INPUT', close: 'END_VAR'},
		{ open: 'VAR_OUTPUT', close: 'END_VAR'},
		{ open: 'VAR_IN_OUT', close: 'END_VAR'},
		{ open: 'VAR_TEMP', close: 'END_VAR'},
		{ open: 'VAR_GLOBAL', close: 'END_VAR'},
		{ open: 'VAR_ACCESS', close: 'END_VAR'},
		{ open: 'VAR_EXTERNAL', close: 'END_VAR'},
		{ open: 'TYPE', close: 'END_TYPE'},
		{ open: 'STRUCT', close: 'END_STRUCT'},
		{ open: 'PROGRAM', close: 'END_PROGRAM'},
		{ open: 'FUNCTION', close: 'END_FUNCTION'},
		{ open: 'FUNCTION_BLOCK', close: 'END_FUNCTION_BLOCK'},
		{ open: 'ACTION', close: 'END_ACTION'},
		{ open: 'STEP', close: 'END_STEP'},
		{ open: 'INITIAL_STEP', close: 'END_STEP'},
		{ open: 'TRANSACTION', close: 'END_TRANSACTION'},
		{ open: 'CONFIGURATION', close: 'END_CONFIGURATION'},
		{ open: 'TCP', close: 'END_TCP'},
		{ open: 'RESOURCE', close: 'END_RESOURCE'},
		{ open: 'CHANNEL', close: 'END_CHANNEL'},
		{ open: 'LIBRARY', close: 'END_LIBRARY'},
		{ open: 'FOLDER', close: 'END_FOLDER'},
		{ open: 'BINARIES', close: 'END_BINARIES'},
		{ open: 'INCLUDES', close: 'END_INCLUDES'},
		{ open: 'SOURCES', close: 'END_SOURCES'}
	],
	surroundingPairs: [
		{ open: '{', close: '}' },
		{ open: '[', close: ']' },
		{ open: '(', close: ')' },
		{ open: '"', close: '"' },
		{ open: '\'', close: '\'' },
		{ open: 'VAR', close: 'END_VAR'},
		{ open: 'VAR_INPUT', close: 'END_VAR'},
		{ open: 'VAR_OUTPUT', close: 'END_VAR'},
		{ open: 'VAR_IN_OUT', close: 'END_VAR'},
		{ open: 'VAR_TEMP', close: 'END_VAR'},
		{ open: 'VAR_GLOBAL', close: 'END_VAR'},
		{ open: 'VAR_ACCESS', close: 'END_VAR'},
		{ open: 'VAR_EXTERNAL', close: 'END_VAR'},
		{ open: 'TYPE', close: 'END_TYPE'},
		{ open: 'STRUCT', close: 'END_STRUCT'},
		{ open: 'PROGRAM', close: 'END_PROGRAM'},
		{ open: 'FUNCTION', close: 'END_FUNCTION'},
		{ open: 'FUNCTION_BLOCK', close: 'END_FUNCTION_BLOCK'},
		{ open: 'ACTION', close: 'END_ACTION'},
		{ open: 'STEP', close: 'END_STEP'},
		{ open: 'INITIAL_STEP', close: 'END_STEP'},
		{ open: 'TRANSACTION', close: 'END_TRANSACTION'},
		{ open: 'CONFIGURATION', close: 'END_CONFIGURATION'},
		{ open: 'TCP', close: 'END_TCP'},
		{ open: 'RESOURCE', close: 'END_RESOURCE'},
		{ open: 'CHANNEL', close: 'END_CHANNEL'},
		{ open: 'LIBRARY', close: 'END_LIBRARY'},
		{ open: 'FOLDER', close: 'END_FOLDER'},
		{ open: 'BINARIES', close: 'END_BINARIES'},
		{ open: 'INCLUDES', close: 'END_INCLUDES'},
		{ open: 'SOURCES', close: 'END_SOURCES'}
	],
	folding: {
		markers: {
			start: new RegExp("^\\s*#pragma\\s+region\\b"),
			end: new RegExp("^\\s*#pragma\\s+endregion\\b")
		}
	}
};

const Language = {
	defaultToken: '',
	tokenPostfix: '.st',
	ignoreCase: true,

	brackets: [
		{ token: 'delimiter.curly', open: '{', close: '}' },
		{ token: 'delimiter.parenthesis', open: '(', close: ')' },
		{ token: 'delimiter.square', open: '[', close: ']' }
	],

	keywords: ['IF', 'END_IF', 'ELSIF', 'ELSE', 'CASE', 'OF', 'TO',
		'DO', 'WITH', 'BY', 'WHILE', 'REPEAT', 'END_WHILE', 'END_REPEAT', 'END_CASE',
		'FOR', 'END_FOR', 'TASK', 'RETAIN', 'NON_RETAIN', 'CONSTANT', 'WITH', 'AT',
		'EXIT', 'RETURN', 'INTERVAL', 'PRIORITY', 'ADDRESS', 'PORT', 'ON_CHANNEL',
		'THEN', 'IEC', 'FILE', 'USES', 'VERSION', 'PACKAGETYPE', 'DISPLAYNAME',
		'COPYRIGHT', 'SUMMARY', 'VENDOR', 'COMMON_SOURCE', 'FROM', 'ON'],

	constant: ['false', 'true', 'null'],

	defineKeywords: [
		'VAR', 'VAR_INPUT', 'VAR_OUTPUT', 'VAR_IN_OUT', 'VAR_TEMP', 'VAR_GLOBAL',
		'VAR_ACCESS', 'VAR_EXTERNAL', 'END_VAR',

		'TYPE', 'END_TYPE', 'STRUCT', 'END_STRUCT', 'PROGRAM', 'END_PROGRAM',
		'FUNCTION', 'END_FUNCTION', 'FUNCTION_BLOCK', 'END_FUNCTION_BLOCK',

		'CONFIGURATION', 'END_CONFIGURATION', 'TCP', 'END_TCP', 'RESOURCE',
		'END_RESOURCE', 'CHANNEL', 'END_CHANNEL', 'LIBRARY', 'END_LIBRARY',
		'FOLDER', 'END_FOLDER', 'BINARIES', 'END_BINARIES', 'INCLUDES',
		'END_INCLUDES', 'SOURCES', 'END_SOURCES',

		'ACTION', 'END_ACTION', 'STEP', 'INITIAL_STEP', 'END_STEP', 'TRANSACTION', 'END_TRANSACTION'
	],

	typeKeywords: ['INT', 'SINT', 'DINT', 'LINT', 'USINT', 'UINT', 'UDINT', 'ULINT',
		'REAL', 'LREAL', 'TIME', 'DATE', 'TIME_OF_DAY', 'DATE_AND_TIME', 'STRING',
		'BOOL', 'BYTE', 'WORD', 'DWORD', 'ARRAY', 'POINTER', 'LWORD'],

	operators: ['=', '>', '<', ':', ':=', '<=', '>=', '<>', '&', '+', '-', '*', '**',
		'MOD', '^', 'OR', 'AND', 'NOT', 'XOR', 'ABS', 'ACOS', 'ASIN', 'ATAN', 'COS',
		'EXP', 'EXPT', 'LN', 'LOG', 'SIN', 'SQRT', 'TAN', 'SEL', 'MAX', 'MIN', 'LIMIT',
		'MUX', 'SHL', 'SHR', 'ROL', 'ROR', 'INDEXOF', 'SIZEOF', 'ADR', 'ADRINST',
		'BITADR', 'IS_VALID'],

	builtinVariables: [

	],

	builtinFunctions: ['SR', 'RS', 'TP', 'TON', 'TOF', 'EQ', 'GE', 'LE', 'LT',
		'NE', 'ROUND', 'TRUNC', 'CTD', 'CTU', 'CTUD', 'R_TRIG', 'F_TRIG',
		'MOVE', 'CONCAT', 'DELETE', 'FIND', 'INSERT', 'LEFT', 'LEN', 'REPLACE',
		'RIGHT', 'RTC'],

	// we include these common regular expressions
	symbols: /[=><!~?:&|+\-*\/\^%]+/,

	// C# style strings
	escapes: /\\(?:[abfnrtv\\"']|x[0-9A-Fa-f]{1,4}|u[0-9A-Fa-f]{4}|U[0-9A-Fa-f]{8})/,

	// The main tokenizer for our languages
	tokenizer: {
		root: [
			[/(T|DT|TOD)#[0-9:-_shmyd]*/, 'tag'],
			[/[A-Za-z]{1,6}#[0-9]*/, 'tag'],
			[/\%(I|Q|M)(X|B|W|D|L)[0-9\.]*/, 'tag'],
			[/\%(I|Q|M)[0-9\.]*/, 'tag'],
			[/(TO_|CTU_|CTD_|CTUD_|MUX_|SEL_)[A_Za-z]*/, 'predefined'],
			[/[A_Za-z]*(_TO_)[A_Za-z]*/, 'predefined'],

			// identifiers and keywords
			[/[a-zA-Z_]\w*/, {
				cases: {
					'@operators': 'operators',
					'@keywords': 'keyword',
					'@typeKeywords': 'type',
					'@defineKeywords': 'variable',
					'@constant': 'constant',
					'@builtinVariables': 'predefined',
					'@builtinFunctions': 'predefined',
					'@default': 'identifier'
				}
			}],

			{ include: '@whitespace' },

			[/[;.]/, 'delimiter'],
			[/[{}()\[\]]/, '@brackets'],
			[/\d*\.\d+([eE][\-+]?\d+)?/, 'number.float'],
			[/16#[0-9a-fA-F]+/, 'number.hex'],
			[/2#[0-9_]+/, 'number.binary'],
			[/\d+/, 'number'],

			[/"([^"\\]|\\.)*$/, 'string.invalid'],  // non-teminated string
			[/"/, { token: 'string.quote', bracket: '@open', next: '@string_dq' }],
			[/'/, { token: 'string.quote', bracket: '@open', next: '@string_sq' }],

			[/'[^\\']'/, 'string'],
			[/(')(@escapes)(')/, ['string', 'string.escape', 'string']],
			[/'/, 'string.invalid']
		],
		comment: [
			[/[^\/*]+/, 'comment'],
			[/\/\*/, 'comment', '@push'],    // nested comment
			["\\*/", 'comment', '@pop'],
			[/[\/*]/, 'comment']
		],
		comment2: [
			[/[^\(*]+/, 'comment'],
			[/\(\*/, 'comment', '@push'],    // nested comment
			["\\*\\)", 'comment', '@pop'],
			[/[\(*]/, 'comment']
		],
		whitespace: [
			[/[ \t\r\n]+/, 'white'],
			[/\/\/.*$/, 'comment'],
			[/\/\*/, 'comment', '@comment'],
			[/\(\*/, 'comment', '@comment2'],
		],
		string_dq: [
			[/[^\\"]+/, 'string'],
			[/@escapes/, 'string.escape'],
			[/\\./, 'string.escape.invalid'],
			[/"/, { token: 'string.quote', bracket: '@close', next: '@pop' }]
		],
		string_sq: [
			[/[^\\']+/, 'string'],
			[/@escapes/, 'string.escape'],
			[/\\./, 'string.escape.invalid'],
			[/'/, { token: 'string.quote', bracket: '@close', next: '@pop' }]
		]
	}
};

export { Configuration, Language };