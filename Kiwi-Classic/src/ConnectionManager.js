import SSDP from 'node-ssdp';
import BlueBird from 'bluebird';
import axios from 'axios';
import * as URI from "uri-js";
import io from 'socket.io-client';

class ConnectionManager {
  constructor() {
    this.SSDPClient = new SSDP.Client({ explicitSocketBind: true, ssdpIp: '239.255.255.250' });
    this.SSDPClient.on('response', this.onSSDP.bind(this));
    this.Gateways = [];
    this.Socket = null;
    this.PLC = null;
    this.Connected = false;
  }

  setEvents(events) {
    this.Events = events;
  }

  async search() {
    try {
      this.Gateways = [];
      this.SSDPClient.search('urn:schemas-upnp-org:service:kiwi-gateway:1');
      await new Promise(r => setTimeout(r, 3000))
      this.SSDPClient.stop();

      let urls = _.uniq(this.Gateways);
      let found = [];

      for(let i = 0; i < urls.length; i++) {
        try {
          let uri = URI.parse(urls[i]);
          let res = await axios({ method: 'get', url: urls[i], timeout: 1000 });
          let plc = res.data;
          found.push({
            PLC: plc,
            UUID: plc.UUID,
            IP: uri.host,
            Address: `${uri.scheme}://${uri.host}:${uri.port}/${plc.UUID}`
          });
        } catch(e) {
        }
      }
      console.log(found);
      return found;
    } catch(e) {
      console.log("oooopsss");
      return [];
    }
  }

  connect(plc) {
    var that = this;
    this.PLC = plc;
    this.Socket = io(plc.Address, {
      autoConnect: false,
      reconnection: false
    });
    this.Socket.on('blocked', (msg) => {
      that.Events.onBlocked(msg);
    });
    this.Socket.on('status', (msg) => {
      this.Events.onStatus(msg);
    });
    this.Socket.on('connect', () => {
      this.Connected = true;
      that.Events.onConnect(that.Socket.connected, plc);
    });
    this.Socket.on('error', (err) => {
      that.Events.onError(err);
    });
    this.Socket.on('connect_error', (err) => {
      that.Events.onConnect(false, plc);
    });
    this.Socket.on('connect_timeout', (err) => {
      that.Events.onConnect(false, plc);
    });
    this.Socket.on('disconnect', () => {
      if (that.StartCallback != null)    that.StartCallback({ MSGType: 0 });
      if (that.StopCallback != null)     that.StopCallback({ MSGType: 0 });
      if (that.CompileCallback != null)  that.CompileCallback({ MSGType: 0 });
      if (that.CleanCallback != null)    that.CleanCallback({ MSGType: 0 });
      if (that.UploadCallback != null)   that.UploadCallback({ MSGType: 0 });
      if (that.DownloadCallback != null) that.DownloadCallback({ MSGType: 0 });
      if (that.EraseCallback != null)    that.EraseCallback({ MSGType: 0 });
      if (that.ScanCallback != null)     that.ScanCallback({ MSGType: 0 });
      if (that.WifiCallback != null)     that.WifiCallback({ MSGType: 0 });

      that.Connected = false;

      that.Events.onDisconnect();
    });
    this.Socket.on('close-connection', () => {
      that.Socket.disconnect(true);
      that.Connected = false;
    });
    this.Socket.on('started', (msg) => {
      if (that.StartCallback) {
        that.StartCallback(msg);
        that.StartCallback = null;
      } else {
        that.Events.onStart(msg);
      }
    });
    this.Socket.on('stopped', (msg) => {
      if (that.StopCallback) {
        that.StopCallback(msg);
        that.StopCallback = null;
      } else {
        that.Events.onStop(msg);
      }
    });
    this.Socket.on('compiled', (msg) => {
      if (that.CompileCallback) {
        that.CompileCallback(msg);
        that.CompileCallback = null;
      } else {
        that.Events.onCompile(msg);
      }
    });
    this.Socket.on('cleaned', (msg) => {
      if (that.CleanCallback) {
        that.CleanCallback(msg);
        that.CleanCallback = null;
      } else {
        that.Events.onClean(msg);
      }
    });
    this.Socket.on('uploaded', (msg) => {
      if (that.UploadCallback) {
        that.UploadCallback(msg);
        that.UploadCallback = null;
      } else {
        that.Events.onUpload(msg);
      }
    });
    this.Socket.on('downloaded', (msg) => {
      if (that.DownloadCallback) {
        that.DownloadCallback(msg);
        that.DownloadCallback = null;
      } else {
        that.Events.onDownload(msg);
      }
    });
    this.Socket.on('erased', (msg) => {
      if (that.EraseCallback) {
        that.EraseCallback(msg);
        that.EraseCallback = null;
      } else {
        that.Events.onErase(msg);
      }
    });
    this.Socket.on('scanned', (msg) => {
      if (that.ScanCallback) {
        that.ScanCallback(msg);
        that.ScanCallback = null;
      } else {
        that.Events.onScan(msg);
      }
    });
    this.Socket.on('ethernet-set', (msg) => {
      if (that.EthernetCallback) {
        that.EthernetCallback(msg);
        that.EthernetCallback = null;
      } else {
        that.Events.onEthernet(msg);
      }
    });
    this.Socket.on('wifi-set', (msg) => {
      if (that.WifiCallback) {
        that.WifiCallback(msg);
        that.WifiCallback = null;
      } else {
        that.Events.onWifi(msg);
      }
    });
    this.Socket.on('usv-set', (msg) => {
      if (that.WifiCallback) {
        that.USVCallback(msg);
        that.USVCallback = null;
      } else {
        that.Events.onUSV(msg);
      }
    });
    this.Socket.connect();
  }

  disconnect() {
    if (this.Socket) {
      this.Socket.emit('close-connection', {});
      this.Socket.disconnect(true);
      if (this.UploadCallback != null) {
        this.UploadCallback({});
        this.UploadCallback = null;
      }
    }
  }

  start(callback) {
    this.StartCallback = callback;
    if (this.Socket) {
      this.Socket.emit('start', {});
    } else {
      this.StartCallback({MSGType: 0});
    }
  }

  stop(callback) {
    this.StopCallback = callback;
    if (this.Socket) {
      this.Socket.emit('stop', {});
    } else {
      this.StopCallback({MSGType: 0});
    }
  }

  compile(project, callback) {
    this.CompileCallback = callback;
    if (this.Socket) {
      this.Socket.emit('compile', { Project: project });
    } else {
      this.CompileCallback({MSGType: 0});
    }
  }

  clean(callback) {
    this.CleanCallback = callback;
    if (this.Socket) {
      this.Socket.emit('clean', {});
    } else {
      this.CleanCallback({MSGType: 0});
    }
  }

  upload(callback) {
    this.UploadCallback = callback;
    if (this.Socket) {
      this.Socket.emit('upload', {});
    } else {
      this.UploadCallback({MSGType: 0});
    }
  }

  download(callback) {
    this.DownloadCallback = callback;
    if (this.Socket) {
      this.Socket.emit('download', {});
    } else {
      this.DownloadCallback({MSGType: 0});
    }
  }

  erase(callback) {
    this.EraseCallback = callback;
    if (this.Socket) {
      this.Socket.emit('erase', {});
    } else {
      this.EraseCallback({MSGType: 0});
    }
  }

  scan(bus, callback) {
    this.ScanCallback = callback;
    if (this.Socket) {
      this.Socket.emit('scan', bus);
    } else {
      this.ScanCallback({MSGType: 0});
    }
  }

  setEthernet(preferences, callback) {
    this.EthernetCallback = callback;
    if (this.Socket) {
      this.Socket.emit('set-ethernet', preferences);
    } else {
      this.EthernetCallback({MSGType: 0});
    }
  }

  setWifi(preferences, callback) {
    this.WifiCallback = callback;
    if (this.Socket) {
      this.Socket.emit('set-wifi', preferences);
    } else {
      this.WifiCallback({MSGType: 0});
    }
  }

  setUSV(preferences, callback) {
    this.USVCallback = callback;
    if (this.Socket) {
      this.Socket.emit('set-usv', preferences);
    } else {
      this.USVCallback({MSGType: 0});
    }
  }

  get isConnected() {
    return this.Connected;
  }

  // Events.
  onSSDP(headers, statusCode, rinfo) {
    this.Gateways.push(headers.LOCATION);
  }
};

export default {
  install: (Vue, options) => {
    Vue.prototype.$CM = new ConnectionManager();
  }
};

