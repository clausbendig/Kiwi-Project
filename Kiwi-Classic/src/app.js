/*
* Copyright (C) Claus Bendig
*/

// Import CSS files.
import 'vue-awesome/icons';
import "vue-snotify/styles/dark.css";

// Import JS files.
import Vue from 'vue';
import VueDragAndDrop from 'vue-drag-drop';
import Icon from 'vue-awesome';
import Snotify, { SnotifyPosition } from 'vue-snotify';
import ConnectionManager from './ConnectionManager';
import PLCs from './Components/PLCs.vue';
import Hardware from './Components/Hardware.vue';
import Console from './Components/Console.vue';
import Va from 'vue-atlas';
import Bus from './Components/Bus/Bus.vue';
import Kiwi from './Components/Bus/Kiwi.vue';
import MQTT from './Components/Bus/MQTT.vue';
import ModbusTCP from './Components/Bus/Modbus-TCP.vue';

// Import CSS files.
import 'vue-atlas/dist/vue-atlas.css';

// Use the Vue development tool in development mode.
if (process.env.NODE_ENV == 'development') {
  Vue.config.devtools = true;
  // Vue.config.debug = false;
  Vue.config.silent = true;
}

// Import app entry.
import App from './App.vue';
import Monaco from './Third/monaco-editor-iec61131/Monaco.vue';

Vue.component('monaco-editor', Monaco);
Vue.component('v-icon', Icon);
Vue.component('plcs', PLCs);
Vue.component('hardware', Hardware);
Vue.component('console', Console);
Vue.component('bus', Bus);
Vue.component('kiwi', Kiwi);
Vue.component('mqtt', MQTT);
Vue.component('modbus-tcp', ModbusTCP);

// Use Vue plugins.
Vue.use(ConnectionManager);
Vue.use(VueDragAndDrop);
Vue.use(Va, 'en');
Vue.use(Snotify, {
  toast: {
    position: SnotifyPosition.centerTop
  }
});

new Vue({
  el: '#app',
  render: h => h(App),
});