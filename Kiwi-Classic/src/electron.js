/*
* Copyright (C) Claus Bendig
*/

const electron = require('electron');
const os = require('os');
const path = require('path');
const { app, BrowserWindow, Menu, dialog, ipcMain } = require('electron');

class MainWindow {

  constructor() {
    this.HomePath = app.getPath('home');
    this.ProjectPath = '';
    if (os.platform() == 'win32') this.HomePath = app.getPath('home') + path.sep + 'Documents';
    const isAlreadyRunning = app.makeSingleInstance(this.onSecondInstance.bind(this));
    if (isAlreadyRunning) app.exit();
    ipcMain.on('connected',    this.onConnected.bind(this));
    ipcMain.on('disconnected', this.onDisconnected.bind(this));
    ipcMain.on('started',      this.onStarted.bind(this));
    ipcMain.on('stopped',      this.onStopped.bind(this));
    ipcMain.on('compiled',     this.onCompiled.bind(this));
    ipcMain.on('cleaned',      this.onCleaned.bind(this));
    ipcMain.on('uploaded',     this.onUploaded.bind(this));
    ipcMain.on('downloaded',   this.onDownloaded.bind(this));
    ipcMain.on('erased',       this.onErased.bind(this));
    ipcMain.on('status',       this.onStatus.bind(this));
    
    ipcMain.on('starting',     this.onStarting.bind(this));
    ipcMain.on('stopping',     this.onStopping.bind(this));
    ipcMain.on('compiling',    this.onCompiling.bind(this));
    ipcMain.on('cleaning',     this.onCompiling.bind(this));
    ipcMain.on('uploading',    this.onUploading.bind(this));
    ipcMain.on('downloading',  this.onDownloading.bind(this));
    ipcMain.on('erasing',      this.onErasing.bind(this));

    ipcMain.on('processing',      this.onProcessing.bind(this));

    app.on('ready', this.onReady.bind(this));
    app.on('window-all-closed', this.onAllWindowsClosed.bind(this));
    app.on('activate', this.onActivate.bind(this));
    app.on('second-instance', this.onSecondInstance.bind(this));
  }

  createMenu() {    
    // If program runs in macOS.
     
      const name = app.getName();

      const template = [
        {
          label: 'File',
          submenu: [
            {
              label: 'New',
              accelerator: 'CmdOrCtrl+N',
              click: this.onNewProject.bind(this)
            },
            { type: 'separator' },
            {
              label: 'Open ...',
              accelerator: 'CmdOrCtrl+O',
              click: this.onOpenProject.bind(this)
            },
            { type: 'separator' },
            {
              label: 'Save',
              accelerator: 'CmdOrCtrl+S',
              click: this.onSaveProject.bind(this)
            },
            {
              label: 'Save as ...',
              accelerator: 'CmdOrCtrl+Shift+S',
              click: this.onSaveAsProject.bind(this)
            },
            { type: 'separator' },
            {
              label: 'Quit',
              accelerator:
              'CmdOrCtrl+Q',
              click: this.onQuit.bind(this)
            },
          ]
        },
        {
          label: 'Edit',
          submenu: [
            {role: 'undo'},
            {role: 'redo'},
            {type: 'separator'},
            {role: 'cut'},
            {role: 'copy'},
            {role: 'paste'}
          ]
        },
        {
          label: 'View',
          submenu: [
            { label: 'Program', accelerator: 'CmdOrCtrl+P', click: this.onShowProgram.bind(this) },
            { label: 'Hardware', accelerator: 'CmdOrCtrl+H', click: this.onShowHardware.bind(this) },
            { label: 'Console', accelerator: 'CmdOrCtrl+M', click: this.onShowConsole.bind(this) },
            {type: 'separator'},
            {role: 'togglefullscreen'}
          ]
        },
        {
          label: 'PLC',
          submenu: [
            {
              id: 'Connect',
              label: 'Connect',
              accelerator: 'CmdOrCtrl+Shift+C',
              click: this.onConnect.bind(this)
            },
            {
              id: 'Disconnect',
              label: 'Disconnect',
              accelerator: 'CmdOrCtrl+Shift+D',
              visible: false,
              click: this.onDisconnect.bind(this)
            },
            //{ id: 'Seperator1', type: 'separator', visible: false },
            {
              id: 'Start',
              label: 'Start',
              accelerator: 'CmdOrCtrl+1',
              visible: false,
              click: this.onStart.bind(this)
            },
            {
              id: 'Stop',
              label: 'Stop',
              accelerator: 'CmdOrCtrl+2',
              visible: false,
              click: this.onStop.bind(this)
            },
            //{ id: 'Seperator2', type: 'separator', visible: false },
            {
              id: 'Compile',
              label: 'Compile',
              accelerator: 'CmdOrCtrl+3',
              visible: false,
              click: this.onCompile.bind(this)
            },
            {
              id: 'Clean',
              label: 'Clean',
              accelerator: 'CmdOrCtrl+4',
              visible: false,
              click: this.onClean.bind(this)
            },
            {
              id: 'Upload',
              label: 'Upload',
              accelerator: 'CmdOrCtrl+5',
              visible: false,
              click: this.onUpload.bind(this)
            },
            {
              id: 'Download',
              label: 'Download',
              accelerator: 'CmdOrCtrl+6',
              visible: false,
              click: this.onDownload.bind(this)
            },
            {
              id: 'Erase',
              label: 'Erase',
              accelerator: 'CmdOrCtrl+7',
              visible: false,
              click: this.onErase.bind(this)
            },
          ]
        },
        {
          role: 'window',
          submenu: [
            {role: 'minimize'},
            {role: 'close'}
          ]
        },
        {
          role: 'help',
          submenu: [
            {
              label: 'About',
              click: this.onShowAbout.bind(this) 
            }
          ]
        }
      ];
      
      if (process.platform === 'darwin') {
        template.unshift({
          label: name,
          submenu: [
            {role: 'about'},
            {type: 'separator'},
            {role: 'services', submenu: []},
            {type: 'separator'},
            {role: 'hide'},
            {role: 'hideothers'},
            {role: 'unhide'},
            {type: 'separator'},
            {role: 'quit'}
          ]
        });

        template[1].submenu.push(
          {
            type: 'separator'
          },
          {
            label: 'Speech',
            submenu: [
              {
                role: 'startspeaking'
              },
              {
                role: 'stopspeaking'
              }
            ]
          }
        );
      
        template[3].submenu = [
          {role: 'close'},
          {role: 'minimize'},
        ];
      } else {
        template.splice(4, 1);
      }
      return template;
  }

  createWindow() {
    // Calculate position to center the window.
    let bounds = electron.screen.getPrimaryDisplay().bounds;
    let x = bounds.x + ((bounds.width - 1280) / 2);
    let y = bounds.y + ((bounds.height - 720) / 2);
  
    if (process.env.NODE_ENV == 'development') {
      this.MainWindow = new BrowserWindow({
        width: 1280,
        title: 'Kiwi-IDE',
        height: 720,
        backgroundColor: '#1e1e1e',
        show: false,
        x: x,
        y: y,
        center: true 
      });
    } else {
      this.MainWindow = new BrowserWindow({
        width: 1280,
        title: 'Kiwi-IDE',
        height: 720,
        minWidth: 1280,
        minHeight: 720,
        backgroundColor: '#1e1e1e',
        x: x,
        y: y,
        show: false,
        center: true 
      });
    }

    // Append menu.
    let template = this.createMenu();
    let menu = Menu.buildFromTemplate(template);
    if (process.platform === 'darwin') {
      Menu.setApplicationMenu(menu);
    } else {
      this.MainWindow.setMenu(menu);
    }

    this.ConnectMenuItem    = menu.getMenuItemById('Connect');
    this.DisconnectMenuItem = menu.getMenuItemById('Disconnect');
    this.StartMenuItem      = menu.getMenuItemById('Start');
    this.StopMenuItem       = menu.getMenuItemById('Stop');
    this.CompileMenuItem    = menu.getMenuItemById('Compile');
    this.CleanMenuItem      = menu.getMenuItemById('Clean');
    this.UploadMenuItem     = menu.getMenuItemById('Upload');
    this.DownloadMenuItem   = menu.getMenuItemById('Download');
    this.EraseMenuItem      = menu.getMenuItemById('Erase');

    // Load app page.
    this.MainWindow.loadFile('app/index.html');
        
    // If in development-mode, then open the development-tools.
    if (process.env.NODE_ENV == 'development') {
      this.MainWindow.webContents.openDevTools();
    }
   
    // Delegate events.
    this.MainWindow.on('close', this.onClose);
  
    // Show window.
    this.MainWindow.show();
  }

  onNewProject() {
    this.ProjectPath = '';
    this.MainWindow.webContents.send('new' , {});
    this.MainWindow.webContents.send('show-program' , {});
  }

  onOpenProject() {
    var _this = this;
    dialog.showOpenDialog(this.MainWindow, {
      defaultPath: this.HomePath,
      filters: [
        { 
          name: 'Kiwi-PLC project', extensions: ['kiwi']
        }
      ]
    }, function(from) {
      if (from) {
        _this.ProjectPath = from[0];
        _this.MainWindow.webContents.send('open' , { Path: _this.ProjectPath });
        _this.MainWindow.webContents.send('show-program' , {});
      }
    });
  }

  onSaveProject() {
    var _this = this;
    if (this.ProjectPath != '') {
      _this.MainWindow.webContents.send('save' , { Path: _this.ProjectPath });
    } else {
      console.log(this.HomePath);
      dialog.showSaveDialog(this.MainWindow, {
        defaultPath: this.HomePath,
        filters: [
          { 
            name: 'Kiwi-PLC project', extensions: ['kiwi']
          }
        ]
      }, function(to) {
        if (to) {
          _this.ProjectPath = to;
          _this.MainWindow.webContents.send('save' , { Path: _this.ProjectPath });
        }
      });
    }
  }

  onSaveAsProject() {
    let path = this.HomePath;
    if (this.ProjectPath != '') path = this.ProjectPath;
    var _this = this;

    dialog.showSaveDialog(this.MainWindow, {
      defaultPath: path,
      filters: [
        { 
          name: 'Kiwi-PLC project', extensions: ['kiwi']
        }
      ]
    }, function(to) {
      if (to) {
        _this.ProjectPath = to;
        _this.MainWindow.webContents.send('save' , { Path: _this.ProjectPath });
      }
    });
  }

  onConnect() {
    this.MainWindow.webContents.send('connect', {});
  }

  onDisconnect() {
    this.MainWindow.webContents.send('disconnect', {});
  }

  onCompile() {
    this.MainWindow.webContents.send('compile', {});
  }

  onClean() {
    this.MainWindow.webContents.send('clean', {});
  }

  onUpload() {
    this.MainWindow.webContents.send('upload', {});
  }

  onDownload() {
    this.MainWindow.webContents.send('download', {});
  }

  onStart() {
    this.MainWindow.webContents.send('start', {});
  }

  onStop() {
    this.MainWindow.webContents.send('stop', {});
  }

  onErase() {
    this.MainWindow.webContents.send('erase', {});
  }

  onQuit() {
    app.quit();
  }

  onShowProgram() {
    this.MainWindow.webContents.send('show-program' , {});
  }

  onShowHardware() {
    this.MainWindow.webContents.send('show-hardware' , {});
  }

  onShowConsole() {
    this.MainWindow.webContents.send('show-console' , {});
  }

  onShowAbout() {
    this.MainWindow.webContents.send('show-about' , {});
  }

  onConnected(event, arg) {    
    this.ConnectMenuItem.visible = false;
    this.DisconnectMenuItem.visible = true;
    this.CompileMenuItem.visible = true;
    this.UploadMenuItem.visible = true;
  }

  onDisconnected(event, arg) {
    this.ConnectMenuItem.visible    = true;
    this.DisconnectMenuItem.visible = false;
    this.StartMenuItem.visible      = false;
    this.StopMenuItem.visible       = false;
    this.CompileMenuItem.visible    = false;
    this.CleanMenuItem.visible      = false;
    this.UploadMenuItem.visible     = false;
    this.DownloadMenuItem.visible   = false;
    this.EraseMenuItem.visible      = false;
  }

  onStarted(event, arg) {
  }

  onStopped(event, arg) {
  }

  onCompiled(event, arg) {
  }

  onCleaned(event, arg) {
  }

  onUploaded(event, arg) {
  }

  onDownloaded(event, arg) {
  }

  onErased(event, arg) {
  }

  onStatus(event, arg) {
    if (arg.isRunning == true) {
      this.StartMenuItem.visible = false;
      this.StopMenuItem.visible = true;
    } else {
      this.StartMenuItem.visible = true;
      this.StopMenuItem.visible = false;
    }
    if (arg.canControl == false) {
      this.StartMenuItem.visible = false;
      this.StopMenuItem.visible = false;
    }
    if (arg.canErase == true) {
      this.EraseMenuItem.visible = true;
      this.DownloadMenuItem.visible = true;
    } else {
      this.EraseMenuItem.visible = false;
      this.DownloadMenuItem.visible = false;
    }
    if (arg.hasCompilation == true) {
      this.UploadMenuItem.visible = true;
    } else {
      this.UploadMenuItem.visible = false;
    }
    if (arg.canClean == true) {
      this.CleanMenuItem.visible = true;
    } else {
      this.CleanMenuItem.visible = false;
    }
  }

  onStarting(event, arg) {
  }

  onStopping(event, arg) {
  }

  onCompiling(event, arg) {
  }

  onCleaning(event, arg) {
  }

  onUploading(event, arg) {
  }

  onDownloading(event, arg) {
  }

  onErasing(event, arg) {
  }

  onProcessing(event, arg) {
    this.StartMenuItem.visible      = false;
    this.StopMenuItem.visible       = false;
    this.CompileMenuItem.visible    = false;
    this.CleanMenuItem.visible      = false;
    this.UploadMenuItem.visible     = false;
    this.DownloadMenuItem.visible   = false;
    this.EraseMenuItem.visible      = false;
  }

  onClose(event) {
    app.quit();
  }

  onReady() {
    // If in development-mode, then install Vue development-tools.
    if (process.env.NODE_ENV == 'development')
      require('vue-devtools').install();

    // Create window.
    this.createWindow();
  }

  onActivate() {
    // Create window on macOS.
    if (this.MainWindow === null) {
      this.createWindow();
    }
  }

  onAllWindowsClosed() {
    // Close window on non macOS operating systems.
    if (process.platform !== 'darwin') {
      app.quit();
    } 
  }

  onSecondInstance(event, commandLine, workingDirectory) {
    if (this.MainWindow) {
      if (this.MainWindow.isMinimized()) this.MainWindow.restore();
      this.MainWindow.focus();
    }
  }

};

let mainWindow = new MainWindow();