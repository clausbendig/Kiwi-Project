/*
* Copyright (C) Claus Bendig
*/

'use strict'

const { VueLoaderPlugin } = require('vue-loader');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const MonocoEditorPlugin = require('monaco-editor-webpack-plugin');
const LicenseWebpackPlugin = require('license-webpack-plugin').LicenseWebpackPlugin;
const ejs = require('ejs');
const fs = require('fs');
const path = require('path');

module.exports = {
  target: "electron-renderer",
  mode: 'development',
  entry: {
    app: './src/app.js',
  },
  module: {
    rules: [
      { test:/\.css$/, use:['style-loader','css-loader'] },
      { test:/\.less$/, use:['style-loader','css-loader', { 
          loader: "less-loader",
          options: {
            javascriptEnabled: true
          }
        }]
      },
      { test:/\.scss$/, use:['style-loader','css-loader', 'sass-loader'] },
      { test: /\.vue$/, use: 'vue-loader' },
      { test: /\.(png|woff|woff2|eot|ttf|svg)$/, loader: 'url-loader', options: { limit: 10, name: '[name].[ext]' }},
      { test: /\.ejs$/, loader: 'compile-ejs-loader' }
    ]
  },
  plugins: [
    
    new MonocoEditorPlugin(),
    new VueLoaderPlugin(),
    new webpack.ProvidePlugin({
      _: "underscore"
    }),
    new HtmlWebpackPlugin({
      filename: '../index.html',
      template: 'src/index.html',
      inject: true,
      chunks: ['app'],
    }),
    new CleanWebpackPlugin(['dist', 'app', 'build/about.html']),
    new LicenseWebpackPlugin({
      pattern: /.*/,
      outputFilename: '../../app/about.html',
      renderLicenses: (modules) => { return ejs.render(fs.readFileSync('src/about.ejs', 'utf-8'), { packages: modules }); },
      modulesDirectories: ['node_modules']
    }),
  ],
  output: {
    publicPath: 'static/',
    path: path.resolve(__dirname, 'app/static'),
    filename: '[name].js',
  },
};