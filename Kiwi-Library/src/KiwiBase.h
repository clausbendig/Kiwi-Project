#ifndef KiwiBase_h
#define KiwiBase_h

#include <functional>
#include <iostream>
#include <stdio.h>
#include "libs/SHA256/sha256.h"
#include "libs/matiec/iec_types_all.h"

//#define KIWI_TIME

#define KIWI_CAN_PAYLOAD 64

#ifndef KIWI_DESCRIPTION_BUFFER_SIZE
  #define KIWI_DESCRIPTION_BUFFER_SIZE 2048
#endif

#define toBuffer(value) ((uint8_t*) &value)
#define toDataType(dataType, value, index) (*((dataType*) &value[index]))

// Commands.
#define KIWI_CMD_ERROR                  0
#define KIWI_CMD_STOP                   1
#define KIWI_CMD_HEARTBEAT              2
#define KIWI_CMD_START                  3
#define KIWI_CMD_OUTPUT                 4
#define KIWI_CMD_INPUT                  5
#define KIWI_CMD_HEARTBEAT_TIME         6
#define KIWI_CMD_UUID                   7
#define KIWI_CMD_DESCRIPTION            8
#define KIWI_CMD_DESCRIPTION_SHA256     9
#define KIWI_CMD_SEARCH                10
#define KIWI_CMD_RESERVED_1            11
#define KIWI_CMD_RESERVED_2            12
#define KIWI_CMD_RESERVED_3            13
#define KIWI_CMD_RESERVED_4            14
#define KIWI_CMD_ECHO                  15

// 29-bit extraction.
#define extractMeta(val) ((val >> 0x12) & 0x7FF)
#define extractData(val) (val & 0x3FFFF)

// 29-bit setting.
#define setMeta(val, meta) (val |= (meta << 0x12))
#define setData(val, data) (val |= data)

// 11-bit extraction.
#define extractCommand(val)  ((val >> 0x07) & 0x0F)
#define extractAddress(val)  (val & 0x7F)

// 11-bit setting.
#define setCommand(val, cmd)   (val |= (cmd  << 0x07))
#define setAddress(val, addr)  (val |= addr)

typedef struct kiwi_hal_t {
  std::function<void()> begin;
  std::function<void(bool)> setStatusLED;
  std::function<void(uint32_t)> startHeartBeatWatchDog;
  std::function<void()> stopHeartBeatWatchDog;
  std::function<void()> resetHeartBeatWatchDog;
  //std::function<void(size_t)> delay;
  std::function<void(uint32_t id, bool extended, bool rtr, uint8_t *buffer, uint8_t length)> sendCANFrame;
  std::function<void()> loop;
  std::function<void(std::function<void(uint32_t id, bool extended, bool request, uint8_t *buffer, uint8_t length)>, std::function<void()>, std::function<void(uint8_t address)>)> setCallbacks;
} KiwiHAL;

#include "HAL/HAL.h"

class KiwiBase {
  public:
    void begin(const char *name, const char *uuid) {
      // If this method was already called, return.
      if (LibraryStarted) return;

      // Set status LED to low.
      HAL.setStatusLED(false);  

      #if defined(KIWI_DEBUG) || defined (KIWI_TIME)
      Serial.begin(115200);
      #endif

      int ret = sscanf(uuid, "%02hhx%02hhx%02hhx%02hhx-%02hhx%02hhx-%02hhx%02hhx-%02hhx%02hhx-%02hhx%02hhx%02hhx%02hhx%02hhx%02hhx",
                        &UUID[0],  &UUID[1],  &UUID[2],  &UUID[3],  &UUID[4],  &UUID[5],  &UUID[6],  &UUID[7],
                        &UUID[8],  &UUID[9],  &UUID[10], &UUID[11], &UUID[12], &UUID[13], &UUID[14], &UUID[15]);

      if (ret == EOF) {
        error('U');
      }

      // Write the description (JSON-Format).
      beginDescription();
      endDescription(name, uuid);


      // Hash description.
      SHA256.update((unsigned char*) DescriptionBuffer, DescriptionCursor);
      SHA256.final(DescriptionSHA256);

      // Set callbacks.
      HAL.setCallbacks(std::bind(&KiwiBase::gotCANFrame, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5),
                       std::bind(&KiwiBase::onHeartBeatTimer, this),
                       std::bind(&KiwiBase::setModuleAddress, this, std::placeholders::_1));

      // Begin HAL.
      HAL.begin();

      // Set status LED.
      HAL.setStatusLED(true);  

      #ifdef KIWI_DEBUG
      printMeta();
      #endif


      // Set it to true, so that begin() won't run twice or more.
      LibraryStarted = true;
    };

    void loop() {
      if (HAL.loop != NULL) {
        HAL.loop();
      }
    }

    void error(char msg) {
      if (ErrorCallback != NULL) {
        ErrorCallback(msg);
      }
      if (msg != 'B') sendErrorFrame();
      HAL.setStatusLED(false);
      Running = false;
    }

    void onStart(std::function<void()> cb) {
      StartCallback = cb;
    }

    void onStop(std::function<void()> cb) {
      StopCallback = cb;
    }

    void onInput(std::function<bool()> cb) {
      ProcessInputCallback = cb;
    }

    void onOutput(std::function<bool()> cb) {
      ProcessOutputCallback = cb;
    }

    void onError(std::function<void(char)> cb) {
      ErrorCallback = cb;
    }

    bool isRunning() {
      return Running;
    }

  protected:
    virtual void beginDescription() {
    }

    virtual void endDescription(const char *name, const char *uuid) {
    }

    virtual void processInput(uint32_t rx_id, bool rx_extended, bool rx_request, uint8_t *rx_buffer, uint8_t rx_length) {
    }

    virtual void processOutput(uint32_t rx_id, bool rx_extended, bool rx_request, uint8_t *rx_buffer, uint8_t rx_length) {
    }

    void gotCANFrame(uint32_t rx_id, bool rx_extended, bool rx_request, uint8_t *rx_buffer, uint8_t rx_length) {
      #ifdef KIWI_TIME
      uint32_t old = micros(); 
      #endif

      size_t cmd, addr, meta, data = 0;
      uint32_t tx_id = 0;
      uint32_t tx_data = 0;

      HAL.resetHeartBeatWatchDog();

      if (rx_extended == true) {
        // Extract the meta-informations.
        meta = extractMeta(rx_id);
        data = extractData(rx_id);
        cmd  = extractCommand(meta);
        addr = extractAddress(meta);

        // Switch to the command.
        switch(cmd) {
          case KIWI_CMD_DESCRIPTION:
            {
              if (addr == Address) {
                if (rx_request == true) {
                  setCommand(meta, KIWI_CMD_DESCRIPTION);
                  setAddress(meta, Address);
                  setMeta(tx_id, meta);
                  setData(tx_id, data);
                  if (data < (KIWI_DESCRIPTION_BUFFER_SIZE / KIWI_CAN_PAYLOAD)) {
                    sendCANFrame(tx_id, true, false, &DescriptionBuffer[data * 64], 64);
                  } else {
                    sendCANFrame(tx_id, true, false, NULL, 0);
                  }
                }
              }
            }
            break;

          case KIWI_CMD_ECHO:
            {
              if (addr == Address) {
                sendCANFrame(rx_id, rx_extended, rx_request, rx_buffer, rx_length);
              }
            }
            break;     

          default:
            break;
        }
      } else {
        // Extract the meta-informations.
        cmd   = extractCommand(rx_id);
        addr  = extractAddress(rx_id);

        // Switch to the command.
        switch(cmd) {
          case KIWI_CMD_ERROR:
            {
              error('B');
            }
            break;

          case KIWI_CMD_STOP:
            {
              stop();
            }
            break;

          case KIWI_CMD_START:
            {
              start();
            }
            break;

          case KIWI_CMD_HEARTBEAT:
            {
              if (Running == true) HAL.resetHeartBeatWatchDog();
            }
            break;

          case KIWI_CMD_OUTPUT:
            {
              if (addr == Address) {
                processOutput(rx_id, rx_extended, rx_request, rx_buffer, rx_length);
                if (ProcessOutputCallback != NULL) {
                   if (ProcessOutputCallback() == false) {
                     error('P');
                   }
                }
                
              }
            }
            break;

          case KIWI_CMD_INPUT:
            {
              if (addr == Address) {
                if (ProcessInputCallback != NULL) {
                  if (ProcessInputCallback() == true) {
                    processInput(rx_id, rx_extended, rx_request, rx_buffer, rx_length);
                  } else {
                    error('P');
                  }
                } else {
                  processInput(rx_id, rx_extended, rx_request, rx_buffer, rx_length);
                }
              }
            }
            break;
          
          case KIWI_CMD_HEARTBEAT_TIME:
            {
              if (addr == Address) {
                if (rx_request == true) {
                  setCommand(tx_id, KIWI_CMD_HEARTBEAT_TIME);
                  setAddress(tx_id, Address);
                  sendCANFrame(tx_id, false, false, toBuffer(HeartbeatTime), 4);
                  
                } else {
                  if (rx_length == 4) {
                    HeartbeatTime = toDataType(uint32_t, rx_buffer, 0);
                    #ifdef KIWI_DEBUG
                      Serial.print("Received HeartBeatTime: ");
                      Serial.print(HeartbeatTime, DEC);
                      Serial.println("ms.");
                    #endif
                  }
                }
              }
            }
            break;

          case KIWI_CMD_UUID:
            {
              if (addr == Address) {
                if (rx_request == true) {
                  setCommand(tx_id, KIWI_CMD_UUID);
                  setAddress(tx_id, Address);
                  sendCANFrame(tx_id, false, false, &UUID[0], 16);
                }
              }
            }
            break;

          case KIWI_CMD_DESCRIPTION_SHA256:
            {
              if (addr == Address) {
                if (rx_request == true) {
                  setCommand(tx_id, KIWI_CMD_DESCRIPTION_SHA256);
                  setAddress(tx_id, Address);
                  sendCANFrame(tx_id, false, false, &DescriptionSHA256[0], 32);
                }
              }
            }
            break;

          case KIWI_CMD_SEARCH:
            {
              if (rx_request == true) {
                setCommand(tx_id, KIWI_CMD_SEARCH);
                setAddress(tx_id, Address);
                sendCANFrame(tx_id, false, false, NULL, 0);
              }
            }
            break;
          
          case KIWI_CMD_ECHO:
            {
              if (addr == Address) {
                sendCANFrame(rx_id, rx_extended, rx_request, rx_buffer, rx_length);
              }
            }
            break;

          default:
            break;
        }
      }

      #ifdef KIWI_DEBUG
      printFrame(rx_id, rx_extended, rx_request, rx_buffer, rx_length, cmd, addr, data);
      #endif

      #ifdef KIWI_TIMES
      uint32_t current = micros();
      Serial.print("CMD: ");
      Serial.print(rx_id, HEX);
      Serial.print(" | TIME: ");
      Serial.print(current - old, DEC);
      Serial.println("us");
      #endif
      
    }

    void sendCANFrame(uint32_t tx_id, bool tx_extended, bool tx_request, uint8_t *buffer, uint8_t length) {
      HAL.sendCANFrame(tx_id, tx_extended, tx_request, buffer, length);

      #ifdef KIWI_DEBUG
      size_t cmd, addr, meta, data = 0;
      if (tx_extended == true) {
        // Extract the meta-informations.
        meta = extractMeta(tx_id);
        data = extractData(tx_id);
        cmd  = extractCommand(meta);
        addr = extractAddress(meta);
      } else {
        cmd   = extractCommand(tx_id);
        addr  = extractAddress(tx_id);
      }
      printFrame(tx_id, tx_extended, tx_request, buffer, length, cmd, addr, data);
      #endif
    }

    void sendErrorFrame() {
      sendCANFrame(Address, false, false, NULL, 0);
    }

    void setModuleAddress(uint8_t address) {
      Address = address;

      #ifdef KIWI_DEBUG
      Serial.print("Address:");
      Serial.println(Address, DEC);
      #endif
    }

    bool     LibraryStarted     = false;
    bool     DescriptionStarted = false;
    bool     DescriptionEnded   = false;
    bool     Running            = false;
    uint8_t  DescriptionBuffer[KIWI_DESCRIPTION_BUFFER_SIZE];
    uint16_t DescriptionCursor = 0;
    uint8_t  Address = 0;

  private:
    void onHeartBeatTimer() {
      if (Running == true) {
        error('C');
      }
    }

    void start() {
      if (StartCallback != NULL) {
        StartCallback();
      }
      HAL.setStatusLED(true); 
      HAL.startHeartBeatWatchDog(HeartbeatTime);
      Running = true;
    }

    void stop() {
      if (StopCallback != NULL) {
        StopCallback();
      }
      HAL.stopHeartBeatWatchDog();
      Running = false;
      Serial.println("Stop");
    }
  
    Sha256   SHA256;
    uint8_t  DescriptionSHA256[32];
    uint32_t HeartbeatTime = 15; // In micro-seconds, default: 10ms.
    uint8_t  UUID[16];
    std::function<void(char)> ErrorCallback;
    std::function<void()>      StartCallback;
    std::function<void()>      StopCallback;
    std::function<bool()>      ProcessInputCallback;
    std::function<bool()>      ProcessOutputCallback;


    #ifdef KIWI_DEBUG
    void printFrame(uint32_t id, bool extended, bool request, uint8_t *buffer, uint8_t length, uint8_t cmd, uint8_t addr, uint8_t data) {
      Serial.print("Frame: ");
      Serial.print("ID = ");
      Serial.print(id, HEX);
      Serial.print("; EXT = ");
      Serial.print(extended, DEC);
      Serial.print("; REQ = ");
      Serial.print(request, DEC);
      Serial.print("; LEN = ");
      Serial.print(length, DEC);
      Serial.print("; BUF = ");
      if (length == 0) {
        Serial.print("--");
      } else {
        for (int i = 0; i < length; i++) {
          Serial.print(buffer[i], HEX); if (i < (length - 1)) { Serial.print(":"); }
        }
      }
      
      Serial.print("; ADDR = ");
      Serial.print(addr, DEC);
      Serial.print("; CMD = ");
      Serial.print(isWhichCommand(cmd));
      Serial.print("; DATA = ");
      Serial.print(data, HEX);
      Serial.println(";");
    }

    const char* isWhichCommand(uint8_t cmd) {
      switch(cmd) {
        case KIWI_CMD_ERROR:
          return "ERROR";
          break;

        case KIWI_CMD_STOP:
          return "STOP";
          break;

        case KIWI_CMD_START:
          return "START";
          break;

        case KIWI_CMD_HEARTBEAT:
          return "HEARTBEAT";
          break;

        case KIWI_CMD_OUTPUT:
          return "OUTPUT";
          break;

        case KIWI_CMD_INPUT:
          return "INPUT";
          break;
        
        case KIWI_CMD_HEARTBEAT_TIME:
          return "HEARTBEAT_TIME";
          break;

        case KIWI_CMD_UUID:
          return "UUID";
          break;

        case KIWI_CMD_DESCRIPTION:
          return "DESCRIPTION";
          break;

        case KIWI_CMD_DESCRIPTION_SHA256:
          return "SHA256";
          break;

        case KIWI_CMD_SEARCH:
          return "SEARCH";
          break;

        case KIWI_CMD_ECHO:
          return "ECHO";
          break;

        default:
          return "UNKNOWN";
          break;
      }
    }

    void printMeta() {
      Serial.print("Description: ");
      for (int i = 0; i < KIWI_DESCRIPTION_BUFFER_SIZE; i++) {
        if (DescriptionBuffer[i] == '\0') {
          break;
        } else {
          Serial.print((char) DescriptionBuffer[i]);
        }
      }
      Serial.println();
      Serial.print("SHA256: ");
      for (int i = 0; i < 32; i++) { Serial.print(DescriptionSHA256[i], HEX); if (i < 31) { Serial.print(":"); } }
      Serial.println();
      Serial.print("UUID: ");
      for (int i = 0; i < 16; i++) { Serial.print(UUID[i], HEX); if (i < 15 ) { Serial.print(":"); } }
      Serial.println();
      Serial.println();
    }
    #endif
};

#endif