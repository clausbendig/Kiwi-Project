/*********************************************************************
* Filename:   sha256.h
* Author:     Brad Conte (brad AT bradconte.com)
* Copyright:
* Disclaimer: This code is presented "as is" without any guarantees.
* Details:    Defines the API for the corresponding SHA1 implementation.
*********************************************************************/

#ifndef SHA256_H
#define SHA256_H

/*************************** HEADER FILES ***************************/
#include <stddef.h>

/****************************** MACROS ******************************/
#define SHA256_BLOCK_SIZE 32            // SHA256 outputs a 32 byte digest

/**************************** DATA TYPES ****************************/
typedef unsigned char SHABYTE;             // 8-bit byte
typedef unsigned int  SHAWORD;             // 32-bit word, change to "long" for 16-bit machines

class Sha256 {
    public:
	Sha256();
	void update(const SHABYTE data[], size_t len);
	void final(SHABYTE hash[]);
    private:
	SHABYTE data[64];
	SHAWORD datalen;
	unsigned long long bitlen;
	SHAWORD state[8];
	void transform();
};

#endif   // SHA256_H
