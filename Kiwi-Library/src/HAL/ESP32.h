#ifndef KiwiHAL_ESP32_h
#define KiwiHAL_ESP32_h

#include <Arduino.h>
#include "../libs/acan2517FD/src/ACAN2517FD.h"
#include <Ticker.h>

#define LED_PIN         GPIO_NUM_4

#define ADDRESS_IN_PIN   GPIO_NUM_39
#define ADDRESS_OUT_PIN  GPIO_NUM_26

#define MCP2517_SCK     GPIO_NUM_18
#define MCP2517_MOSI    GPIO_NUM_23
#define MCP2517_MISO    GPIO_NUM_19
#define MCP2517_CS      GPIO_NUM_5
#define MCP2517_INT     GPIO_NUM_27

std::function<void(uint32_t id, bool extended, bool request, uint8_t *buffer, uint8_t length)> gotFrameCallback;
std::function<void()> triggerHeartBeatTimer;
std::function<void(uint8_t)> setModuleAddress;

ACAN2517FD can (MCP2517_CS, SPI, MCP2517_INT) ;

Ticker AddressTimer;
Ticker HeartBeatTicker;

uint8_t Address = 0;
bool isAddressing = false;
bool hadHeartBeat = false;

void addressIn();

void addressOut() {
  detachInterrupt(ADDRESS_IN_PIN);
 
  for (int i = 0; i < (Address + 1); i++) {
    digitalWrite(ADDRESS_OUT_PIN, HIGH);
    delayMicroseconds(100);
    digitalWrite(ADDRESS_OUT_PIN, LOW);
    if (i < Address) delayMicroseconds(100);
  }

  setModuleAddress(Address);
  isAddressing = false;

  //attachInterrupt(ADDRESS_IN_PIN, addressIn, RISING);
}

void addressIn() {
  if (isAddressing) {
    Address += 1;
  } else {
    Address = 1;
    AddressTimer.once_ms(10, &addressOut);
    isAddressing = true;
  }
}

uint32_t HeartBeatTime = 50;
bool     HeartBeatAttached = false;
bool     GotHeartBeat = false;

void heartBeatWatchDog() {
  if (GotHeartBeat == true) {
    GotHeartBeat = false;
  } else {
    triggerHeartBeatTimer();
  }
}

void resetHeartBeatWatchDog() {
  GotHeartBeat = true;
}

void startHeartBeatWatchDog(uint32_t ms) {
  HeartBeatTime = ms;
  GotHeartBeat = true;
  if (HeartBeatAttached == false) {
    GotHeartBeat = true;
    HeartBeatAttached = true;
    HeartBeatTicker.attach_ms(HeartBeatTime, heartBeatWatchDog);
  }
}

void stopHeartBeatWatchDog() {
}


void begin() {
  // Setup pins.
  pinMode(LED_PIN, OUTPUT);
  pinMode(ADDRESS_IN_PIN, INPUT_PULLDOWN);
  pinMode(ADDRESS_OUT_PIN, OUTPUT);

  // Delay 10ms, so that the MCP2517FD can power up.
  delay(1000);

  // Setup CAN bus.
  SPI.begin(MCP2517_SCK, MCP2517_MISO, MCP2517_MOSI);
  ACAN2517FDSettings settings (ACAN2517FDSettings::OSC_40MHz, 1000 * 1000, ACAN2517FDSettings::DATA_BITRATE_x8);
  settings.mRequestedMode = ACAN2517FDSettings::NormalFD; 
  settings.mControllerReceiveFIFOSize = 10;
  settings.mControllerTransmitFIFOSize = 9;
  settings.mControllerTXQSize = 9;

  const uint32_t errorCode = can.begin (settings, [] { can.isr(); });
  if (errorCode == 0) {
    #ifdef KIWI_DEBUG
      Serial.print ("Bit Rate prescaler: ") ;
      Serial.println (settings.mBitRatePrescaler) ;
      Serial.print ("Arbitration Phase segment 1: ") ;
      Serial.println (settings.mArbitrationPhaseSegment1) ;
      Serial.print ("Arbitration Phase segment 2: ") ;
      Serial.println (settings.mArbitrationPhaseSegment2) ;
      Serial.print ("Arbitration SJW:") ;
      Serial.println (settings.mArbitrationSJW) ;
      Serial.print ("Actual Arbitration Bit Rate: ") ;
      Serial.print (settings.actualArbitrationBitRate ()) ;
      Serial.println (" bit/s") ;
      Serial.print ("Actual Data Bit Rate: ") ;
      Serial.print (settings.actualDataBitRate ()) ;
      Serial.println (" bit/s") ;
      Serial.print ("Exact Arbitration Bit Rate ? ") ;
      Serial.println (settings.exactArbitrationBitRate () ? "yes" : "no") ;
      Serial.print ("Arbitration Sample point: ") ;
      Serial.print (settings.arbitrationSamplePointFromBitStart ()) ;
      Serial.println ("%") ;
    #endif
  } else {
    #ifdef KIWI_DEBUG
      Serial.print ("Configuration error 0x") ;
      Serial.println (errorCode, HEX) ;
    #endif
  }

  attachInterrupt(ADDRESS_IN_PIN, addressIn, RISING);
}

void setStatusLED(bool status) {
  digitalWrite(LED_PIN, status);
}

void sendCANFrame(uint32_t id, bool extended, bool request, uint8_t *buffer, uint8_t length) {
  CANFDMessage txFrame;
  txFrame.id = id;
  txFrame.ext = extended;
  txFrame.len = length;
  if (length > 0) memcpy(txFrame.data, buffer, length);
  bool ok = can.tryToSend(txFrame);

  if (ok == false) {
    txFrame.id = Address;
    txFrame.ext = false;
    txFrame.len = 0;
    can.tryToSend(txFrame);

    #ifdef KIWI_DEBUG
    Serial.println("Sent error frame!!!");
    #endif
  }
}


void setCallbacks(std::function<void(uint32_t id, bool extended, bool request, uint8_t *buffer, uint8_t length)> gf, std::function<void()> hbt, std::function<void(uint8_t address)> addr) {
  gotFrameCallback = gf;
  triggerHeartBeatTimer = hbt;
  setModuleAddress = addr;
}

void loopCAN() {
  CANFDMessage rxFrame;

  if (can.available() > 0) {
    if (can.receive(rxFrame)) {
      bool rtr = rxFrame.type ? rxFrame.CAN_REMOTE : rxFrame.CAN_DATA;
      gotFrameCallback(rxFrame.id, rxFrame.ext, rtr, rxFrame.data, rxFrame.len);
    }
  }
}

KiwiHAL HAL = {
  .begin                  = begin,
  .setStatusLED           = setStatusLED,
  .startHeartBeatWatchDog = startHeartBeatWatchDog,
  .stopHeartBeatWatchDog  = stopHeartBeatWatchDog,
  .resetHeartBeatWatchDog = resetHeartBeatWatchDog,
  .sendCANFrame           = sendCANFrame,
  .loop                   = loopCAN,
  .setCallbacks           = setCallbacks,
};

#endif