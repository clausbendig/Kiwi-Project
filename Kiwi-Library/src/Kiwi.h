#ifndef Kiwi_h
#define Kiwi_h

#include "KiwiBase.h"

typedef union {
  struct {
    BOOL D0 : 1;
    BOOL D1 : 1;
    BOOL D2 : 1;
    BOOL D3 : 1;
    BOOL D4 : 1;
    BOOL D5 : 1;
    BOOL D6 : 1;
    BOOL D7 : 1;
  } Bit;
  BYTE Byte;
} KiwiByte;

typedef union {
  struct {
    BOOL D0  : 1;
    BOOL D1  : 1;
    BOOL D2  : 1;
    BOOL D3  : 1;
    BOOL D4  : 1;
    BOOL D5  : 1;
    BOOL D6  : 1;
    BOOL D7  : 1;
    BOOL D8  : 1;
    BOOL D9  : 1;
    BOOL D10 : 1;
    BOOL D11 : 1;
    BOOL D12 : 1;
    BOOL D13 : 1;
    BOOL D14 : 1;
    BOOL D15 : 1;
  } Bit;
  WORD Word;
} KiwiWord;

typedef union {
  struct {
    BOOL D0  : 1;
    BOOL D1  : 1;
    BOOL D2  : 1;
    BOOL D3  : 1;
    BOOL D4  : 1;
    BOOL D5  : 1;
    BOOL D6  : 1;
    BOOL D7  : 1;
    BOOL D8  : 1;
    BOOL D9  : 1;
    BOOL D10 : 1;
    BOOL D11 : 1;
    BOOL D12 : 1;
    BOOL D13 : 1;
    BOOL D14 : 1;
    BOOL D15 : 1;
    BOOL D16 : 1;
    BOOL D17 : 1;
    BOOL D18 : 1;
    BOOL D19 : 1;
    BOOL D20 : 1;
    BOOL D21 : 1;
    BOOL D22 : 1;
    BOOL D23 : 1;
    BOOL D24 : 1;
    BOOL D25 : 1;
    BOOL D26 : 1;
    BOOL D27 : 1;
    BOOL D28 : 1;
    BOOL D29 : 1;
    BOOL D30 : 1;
    BOOL D31 : 1;
  } Bit;
  DWORD DWord;
} KiwiDWord;

typedef union {
  struct {
    BOOL D0  : 1;
    BOOL D1  : 1;
    BOOL D2  : 1;
    BOOL D3  : 1;
    BOOL D4  : 1;
    BOOL D5  : 1;
    BOOL D6  : 1;
    BOOL D7  : 1;
    BOOL D8  : 1;
    BOOL D9  : 1;
    BOOL D10 : 1;
    BOOL D11 : 1;
    BOOL D12 : 1;
    BOOL D13 : 1;
    BOOL D14 : 1;
    BOOL D15 : 1;
    BOOL D16 : 1;
    BOOL D17 : 1;
    BOOL D18 : 1;
    BOOL D19 : 1;
    BOOL D20 : 1;
    BOOL D21 : 1;
    BOOL D22 : 1;
    BOOL D23 : 1;
    BOOL D24 : 1;
    BOOL D25 : 1;
    BOOL D26 : 1;
    BOOL D27 : 1;
    BOOL D28 : 1;
    BOOL D29 : 1;
    BOOL D30 : 1;
    BOOL D31 : 1;
    BOOL D32 : 1;
    BOOL D33 : 1;
    BOOL D34 : 1;
    BOOL D35 : 1;
    BOOL D36 : 1;
    BOOL D37 : 1;
    BOOL D38 : 1;
    BOOL D39 : 1;
    BOOL D40 : 1;
    BOOL D41 : 1;
    BOOL D42 : 1;
    BOOL D43 : 1;
    BOOL D44 : 1;
    BOOL D45 : 1;
    BOOL D46 : 1;
    BOOL D47 : 1;
    BOOL D48 : 1;
    BOOL D49 : 1;
    BOOL D50 : 1;
    BOOL D51 : 1;
    BOOL D52 : 1;
    BOOL D53 : 1;
    BOOL D54 : 1;
    BOOL D55 : 1;
    BOOL D56 : 1;
    BOOL D57 : 1;
    BOOL D58 : 1;
    BOOL D59 : 1;
    BOOL D60 : 1;
    BOOL D61 : 1;
    BOOL D62 : 1;
    BOOL D63 : 1;
  } Bit;
  LWORD LWord;
} KiwiLWord;

class KiwiModule : public KiwiBase {
  public:
    template <uint8_t MEMORYTYPE, typename DATATYPE, uint16_t BYTE_ADDRESS, uint8_t BIT_ADDRESS>
    void define(const char *name) {
      // If begin() method was already called, return.
      if (KiwiBase::LibraryStarted) return;

      if (BYTE_ADDRESS + sizeof(DATATYPE) > 64) {
        KiwiBase::error('D');
        return;
      }

      if (MEMORYTYPE == INPUT) {
        if (BYTE_ADDRESS + sizeof(DATATYPE) > KIWI_INPUT_BUFFER_SIZE) {
          KIWI_INPUT_BUFFER_SIZE = BYTE_ADDRESS+ sizeof(DATATYPE);
        }
      }
      
      if (MEMORYTYPE == OUTPUT) {
        if (BYTE_ADDRESS + sizeof(DATATYPE) > KIWI_OUTPUT_BUFFER_SIZE) {
          KIWI_OUTPUT_BUFFER_SIZE = BYTE_ADDRESS+ sizeof(DATATYPE);
        }
      }

      // Write information into the description (JSON-Format).
      beginDescription();
      KiwiBase::DescriptionCursor += snprintf((char*) &KiwiBase::DescriptionBuffer[KiwiBase::DescriptionCursor], KIWI_DESCRIPTION_BUFFER_SIZE - KiwiBase::DescriptionCursor, "{\"Name\":\"%s\",\"DataType\":\"%s\",\"MemoryType\":\"%s\", \"Address\":\"%u.%u\"},", name, isWhichDataType<DATATYPE>(), isWhichMemoryType<MEMORYTYPE>(), BYTE_ADDRESS, BIT_ADDRESS);
    }

    template <uint8_t MEMORYTYPE, typename DATATYPE, uint16_t BYTE_ADDRESS>
    void define(const char *name) {
      // If begin() method was already called, return.
      if (KiwiBase::LibraryStarted) return;

      if (BYTE_ADDRESS + sizeof(DATATYPE) > 64) {
        KiwiBase::error('D');
        return;
      }

      if (MEMORYTYPE == INPUT) {
        if (BYTE_ADDRESS + sizeof(DATATYPE) > KIWI_INPUT_BUFFER_SIZE) {
          KIWI_INPUT_BUFFER_SIZE =  BYTE_ADDRESS+ sizeof(DATATYPE);
        }
      }
      
      if (MEMORYTYPE == OUTPUT) {
        if (BYTE_ADDRESS + sizeof(DATATYPE) > KIWI_OUTPUT_BUFFER_SIZE) {
          KIWI_OUTPUT_BUFFER_SIZE =  BYTE_ADDRESS + sizeof(DATATYPE);
        }
      }

      // Write information into the description (JSON-Format).
      beginDescription();
      KiwiBase::DescriptionCursor += snprintf((char*) &KiwiBase::DescriptionBuffer[KiwiBase::DescriptionCursor], KIWI_DESCRIPTION_BUFFER_SIZE - KiwiBase::DescriptionCursor, "{\"Name\":\"%s\",\"DataType\":\"%s\",\"MemoryType\":\"%s\", \"Address\":\"%u\"},", name, isWhichDataType<DATATYPE>(), isWhichMemoryType<MEMORYTYPE>(), BYTE_ADDRESS);
    }

    template <uint8_t MEMORYTYPE, typename DATATYPE, uint16_t BYTE_ADDRESS, uint8_t BIT_ADDRESS>
    DATATYPE get() {
      // Check if the BYTE_ADDRESS and BIT_ADRESS are in the bounds, else call error.
      if (MEMORYTYPE == INPUT) {
        if ((BYTE_ADDRESS + sizeof(DATATYPE) > KIWI_INPUT_BUFFER_SIZE)  || (BIT_ADDRESS >= (sizeof(DATATYPE) * 8)))
          KiwiBase::error('G');
      } else {
        if ((BYTE_ADDRESS + sizeof(DATATYPE) > KIWI_OUTPUT_BUFFER_SIZE) || (BIT_ADDRESS >= (sizeof(DATATYPE) * 8)))
          KiwiBase::error('G');
      }

      switch(sizeof(DATATYPE)) {
        case 1: {
            KiwiByte d;

            if (MEMORYTYPE == INPUT) {
              d.Byte = (BYTE) InputBuffer[BYTE_ADDRESS];
            } else {
              d.Byte = (BYTE) OutputBuffer[BYTE_ADDRESS];
            }

            switch(BIT_ADDRESS) {
              case 0 : return d.Bit.D0; break;
              case 1 : return d.Bit.D1; break; 
              case 2 : return d.Bit.D2; break;
              case 3 : return d.Bit.D3; break;
              case 4 : return d.Bit.D4; break;
              case 5 : return d.Bit.D5; break;
              case 6 : return d.Bit.D6; break;
              case 7 : return d.Bit.D7; break;
              default: return NULL; break;
            }
          }
          break;

        case 2: {
            KiwiWord d;

            if (MEMORYTYPE == INPUT) {
              d.Word = (*((WORD*) &InputBuffer[BYTE_ADDRESS]));
            } else {
              d.Word = (*((WORD*) &OutputBuffer[BYTE_ADDRESS]));
            }

            switch(BIT_ADDRESS) {
              case 0  : return d.Bit.D0;  break;
              case 1  : return d.Bit.D1;  break; 
              case 2  : return d.Bit.D2;  break;
              case 3  : return d.Bit.D3;  break;
              case 4  : return d.Bit.D4;  break;
              case 5  : return d.Bit.D5;  break;
              case 6  : return d.Bit.D6;  break;
              case 7  : return d.Bit.D7;  break;
              case 8  : return d.Bit.D8;  break;
              case 9  : return d.Bit.D9;  break; 
              case 10 : return d.Bit.D10; break;
              case 11 : return d.Bit.D11; break;
              case 12 : return d.Bit.D12; break;
              case 13 : return d.Bit.D13; break;
              case 14 : return d.Bit.D14; break;
              case 15 : return d.Bit.D15; break;
              default: return NULL; break;
            }
          }
          break;

        case 4: {
            KiwiDWord d;

            if (MEMORYTYPE == INPUT) {
              d.DWord = (*((DWORD*) &InputBuffer[BYTE_ADDRESS]));
            } else {
              d.DWord = (*((DWORD*) &OutputBuffer[BYTE_ADDRESS]));
            }

            switch(BIT_ADDRESS) {
              case 0  : return d.Bit.D0;  break;
              case 1  : return d.Bit.D1;  break; 
              case 2  : return d.Bit.D2;  break;
              case 3  : return d.Bit.D3;  break;
              case 4  : return d.Bit.D4;  break;
              case 5  : return d.Bit.D5;  break;
              case 6  : return d.Bit.D6;  break;
              case 7  : return d.Bit.D7;  break;
              case 8  : return d.Bit.D8;  break;
              case 9  : return d.Bit.D9;  break; 
              case 10 : return d.Bit.D10; break;
              case 11 : return d.Bit.D11; break;
              case 12 : return d.Bit.D12; break;
              case 13 : return d.Bit.D13; break;
              case 14 : return d.Bit.D14; break;
              case 15 : return d.Bit.D15; break;
              case 16 : return d.Bit.D16; break;
              case 17 : return d.Bit.D17; break; 
              case 18 : return d.Bit.D18; break;
              case 19 : return d.Bit.D19; break;
              case 20 : return d.Bit.D20; break;
              case 21 : return d.Bit.D21; break;
              case 22 : return d.Bit.D22; break;
              case 23 : return d.Bit.D23; break;
              case 24 : return d.Bit.D24; break;
              case 25 : return d.Bit.D25; break; 
              case 26 : return d.Bit.D26; break;
              case 27 : return d.Bit.D27; break;
              case 28 : return d.Bit.D28; break;
              case 29 : return d.Bit.D29; break;
              case 30 : return d.Bit.D30; break;
              case 31 : return d.Bit.D31; break;
              default: return NULL; break;
            }
          }
          break;

        case 8: {
            KiwiLWord d;

            if (MEMORYTYPE == INPUT) {
              d.LWord = (*((LWORD*) &InputBuffer[BYTE_ADDRESS]));
            } else {
              d.LWord = (*((LWORD*) &OutputBuffer[BYTE_ADDRESS]));
            }

            switch(BIT_ADDRESS) {
              case 0  : return d.Bit.D0;  break;
              case 1  : return d.Bit.D1;  break; 
              case 2  : return d.Bit.D2;  break;
              case 3  : return d.Bit.D3;  break;
              case 4  : return d.Bit.D4;  break;
              case 5  : return d.Bit.D5;  break;
              case 6  : return d.Bit.D6;  break;
              case 7  : return d.Bit.D7;  break;
              case 8  : return d.Bit.D8;  break;
              case 9  : return d.Bit.D9;  break; 
              case 10 : return d.Bit.D10; break;
              case 11 : return d.Bit.D11; break;
              case 12 : return d.Bit.D12; break;
              case 13 : return d.Bit.D13; break;
              case 14 : return d.Bit.D14; break;
              case 15 : return d.Bit.D15; break;
              case 16 : return d.Bit.D16; break;
              case 17 : return d.Bit.D17; break; 
              case 18 : return d.Bit.D18; break;
              case 19 : return d.Bit.D19; break;
              case 20 : return d.Bit.D20; break;
              case 21 : return d.Bit.D21; break;
              case 22 : return d.Bit.D22; break;
              case 23 : return d.Bit.D23; break;
              case 24 : return d.Bit.D24; break;
              case 25 : return d.Bit.D25; break; 
              case 26 : return d.Bit.D26; break;
              case 27 : return d.Bit.D27; break;
              case 28 : return d.Bit.D28; break;
              case 29 : return d.Bit.D29; break;
              case 30 : return d.Bit.D30; break;
              case 31 : return d.Bit.D31; break;
              case 32 : return d.Bit.D32; break;
              case 33 : return d.Bit.D33; break; 
              case 34 : return d.Bit.D34; break;
              case 35 : return d.Bit.D35; break;
              case 36 : return d.Bit.D36; break;
              case 37 : return d.Bit.D37; break;
              case 38 : return d.Bit.D38; break;
              case 39 : return d.Bit.D39; break;
              case 40 : return d.Bit.D40; break;
              case 41 : return d.Bit.D41; break; 
              case 42 : return d.Bit.D42; break;
              case 43 : return d.Bit.D43; break;
              case 44 : return d.Bit.D44; break;
              case 45 : return d.Bit.D45; break;
              case 46 : return d.Bit.D46; break;
              case 47 : return d.Bit.D47; break;
              case 48 : return d.Bit.D48; break;
              case 49 : return d.Bit.D49; break; 
              case 50 : return d.Bit.D50; break;
              case 51 : return d.Bit.D51; break;
              case 52 : return d.Bit.D52; break;
              case 53 : return d.Bit.D53; break;
              case 54 : return d.Bit.D54; break;
              case 55 : return d.Bit.D55; break;
              case 56 : return d.Bit.D56; break;
              case 57 : return d.Bit.D57; break; 
              case 58 : return d.Bit.D58; break;
              case 59 : return d.Bit.D59; break;
              case 60 : return d.Bit.D60; break;
              case 61 : return d.Bit.D61; break;
              case 62 : return d.Bit.D62; break;
              case 63 : return d.Bit.D63; break;
              default: return NULL; break;
            }
          }
          break;

        default:
          return NULL;
      }    
    }

    template <uint8_t MEMORYTYPE, typename DATATYPE, uint16_t BYTE_ADDRESS>
    DATATYPE get() {
      // Check if the BYTE_ADDRESS is in the bound, else call error.
      // Then look if the MEMORYTYPE is an INPUT or OUTPUT and return
      // the value from the corresponding buffer.
      if (MEMORYTYPE == INPUT) {
        if ((BYTE_ADDRESS + sizeof(DATATYPE) > KIWI_INPUT_BUFFER_SIZE)) {
          KiwiBase::error('G');
        } else {
          DATATYPE* ptr = (DATATYPE*) &InputBuffer[BYTE_ADDRESS];
          return (*ptr);
        }
      } else {
        if ((BYTE_ADDRESS + sizeof(DATATYPE) > KIWI_OUTPUT_BUFFER_SIZE)) {
          KiwiBase::error('G');
        } else {
          DATATYPE* ptr = (DATATYPE*) &OutputBuffer[BYTE_ADDRESS];
          return (*ptr);
        }       
      }
    }

    template <uint8_t MEMORYTYPE, typename DATATYPE, uint16_t BYTE_ADDRESS, uint8_t BIT_ADDRESS>
    DATATYPE get(const char* name) {
      return get<MEMORYTYPE, DATATYPE, BYTE_ADDRESS, BIT_ADDRESS>();
    }

    template <uint8_t MEMORYTYPE, typename DATATYPE, uint16_t BYTE_ADDRESS>
    DATATYPE get(const char* name) {
      return get<MEMORYTYPE, DATATYPE, BYTE_ADDRESS>();
    }

    template <uint8_t MEMORYTYPE, typename DATATYPE, uint16_t BYTE_ADDRESS, uint8_t BIT_ADDRESS>
    void set(DATATYPE value) {
      // Check if the BYTE_ADDRESS and BIT_ADRESS are in the bounds, else call error.
      if (MEMORYTYPE == INPUT) {
        if ((BYTE_ADDRESS + sizeof(DATATYPE) > KIWI_INPUT_BUFFER_SIZE)  || (BIT_ADDRESS >= (sizeof(DATATYPE) * 8)))
          KiwiBase::error('S');
      } else {
        if ((BYTE_ADDRESS + sizeof(DATATYPE) > KIWI_OUTPUT_BUFFER_SIZE) || (BIT_ADDRESS >= (sizeof(DATATYPE) * 8)))
          KiwiBase::error('S');
      }

      switch(sizeof(DATATYPE)) {
        case 1: {
          KiwiByte d;

          if (MEMORYTYPE == INPUT) {
            d.Byte = (BYTE) InputBuffer[BYTE_ADDRESS];
          } else {
            d.Byte = (BYTE) OutputBuffer[BYTE_ADDRESS];
          }

          //BYTE old = d.Byte;
          
          switch(BIT_ADDRESS) {
            case 0 : d.Bit.D0 = value; break;
            case 1 : d.Bit.D1 = value; break;
            case 2 : d.Bit.D2 = value; break;
            case 3 : d.Bit.D3 = value; break;
            case 4 : d.Bit.D4 = value; break;
            case 5 : d.Bit.D5 = value; break;
            case 6 : d.Bit.D6 = value; break;
            case 7 : d.Bit.D7 = value; break;
            default: break;
          }

          if (MEMORYTYPE == INPUT) {
            InputBuffer[BYTE_ADDRESS] = (uint8_t) d.Byte;
          } else {
            OutputBuffer[BYTE_ADDRESS] = (uint8_t) d.Byte;
          }

          //if (old != d.Byte) sendCANFrame();
        }
        break;

        case 2: {
          KiwiWord d;

          if (MEMORYTYPE == INPUT) {
            d.Word = (*((WORD*) &InputBuffer[BYTE_ADDRESS]));
          } else {
            d.Word = (*((WORD*) &OutputBuffer[BYTE_ADDRESS]));
          }

          //WORD old = d.Word;
          
          switch(BIT_ADDRESS) {
            case 0  : d.Bit.D0  = value; break;
            case 1  : d.Bit.D1  = value; break;
            case 2  : d.Bit.D2  = value; break;
            case 3  : d.Bit.D3  = value; break;
            case 4  : d.Bit.D4  = value; break;
            case 5  : d.Bit.D5  = value; break;
            case 6  : d.Bit.D6  = value; break;
            case 7  : d.Bit.D7  = value; break;
            case 8  : d.Bit.D8  = value; break;
            case 9  : d.Bit.D9  = value; break;
            case 10 : d.Bit.D10 = value; break;
            case 11 : d.Bit.D11 = value; break;
            case 12 : d.Bit.D12 = value; break;
            case 13 : d.Bit.D13 = value; break;
            case 14 : d.Bit.D14 = value; break;
            case 15 : d.Bit.D15 = value; break;
            default: break;
          }

          if (MEMORYTYPE == INPUT) {
            (*((WORD*) &InputBuffer[BYTE_ADDRESS])) = d.Word;
          } else {
            (*((WORD*) &OutputBuffer[BYTE_ADDRESS])) = d.Word;
          }

          //if (old != d.Word) sendCANFrame();
        }
        break;

        case 4: {
          KiwiDWord d;

          if (MEMORYTYPE == INPUT) {
            d.DWord = (*((DWORD*) &InputBuffer[BYTE_ADDRESS]));
          } else {
            d.DWord = (*((DWORD*) &OutputBuffer[BYTE_ADDRESS]));
          }

          //DWORD old = d.DWord;
          
          switch(BIT_ADDRESS) {
            case 0  : d.Bit.D0  = value; break;
            case 1  : d.Bit.D1  = value; break;
            case 2  : d.Bit.D2  = value; break;
            case 3  : d.Bit.D3  = value; break;
            case 4  : d.Bit.D4  = value; break;
            case 5  : d.Bit.D5  = value; break;
            case 6  : d.Bit.D6  = value; break;
            case 7  : d.Bit.D7  = value; break;
            case 8  : d.Bit.D8  = value; break;
            case 9  : d.Bit.D9  = value; break;
            case 10 : d.Bit.D10 = value; break;
            case 11 : d.Bit.D11 = value; break;
            case 12 : d.Bit.D12 = value; break;
            case 13 : d.Bit.D13 = value; break;
            case 14 : d.Bit.D14 = value; break;
            case 15 : d.Bit.D15 = value; break;
            case 16 : d.Bit.D16 = value; break;
            case 17 : d.Bit.D17 = value; break;
            case 18 : d.Bit.D18 = value; break;
            case 19 : d.Bit.D19 = value; break;
            case 20 : d.Bit.D20 = value; break;
            case 21 : d.Bit.D21 = value; break;
            case 22 : d.Bit.D22 = value; break;
            case 23 : d.Bit.D23 = value; break;
            case 24 : d.Bit.D24 = value; break;
            case 25 : d.Bit.D25 = value; break;
            case 26 : d.Bit.D26 = value; break;
            case 27 : d.Bit.D27 = value; break;
            case 28 : d.Bit.D28 = value; break;
            case 29 : d.Bit.D29 = value; break;
            case 30 : d.Bit.D30 = value; break;
            case 31 : d.Bit.D31 = value; break;
            default: break;
          }

          if (MEMORYTYPE == INPUT) {
            (*((DWORD*) &InputBuffer[BYTE_ADDRESS])) = d.DWord;
          } else {
            (*((DWORD*) &OutputBuffer[BYTE_ADDRESS])) = d.DWord;
          }        

          //if (old != d.DWord) sendCANFrame();
        }
        break;

        case 8: {
          KiwiLWord d;

          if (MEMORYTYPE == INPUT) {
            d.LWord = (*((LWORD*) &InputBuffer[BYTE_ADDRESS]));
          } else {
            d.LWord = (*((LWORD*) &OutputBuffer[BYTE_ADDRESS]));
          }
          
          //LWORD old = d.LWord;

          switch(BIT_ADDRESS) {
            case 0  : d.Bit.D0  = value; break;
            case 1  : d.Bit.D1  = value; break;
            case 2  : d.Bit.D2  = value; break;
            case 3  : d.Bit.D3  = value; break;
            case 4  : d.Bit.D4  = value; break;
            case 5  : d.Bit.D5  = value; break;
            case 6  : d.Bit.D6  = value; break;
            case 7  : d.Bit.D7  = value; break;
            case 8  : d.Bit.D8  = value; break;
            case 9  : d.Bit.D9  = value; break;
            case 10 : d.Bit.D10 = value; break;
            case 11 : d.Bit.D11 = value; break;
            case 12 : d.Bit.D12 = value; break;
            case 13 : d.Bit.D13 = value; break;
            case 14 : d.Bit.D14 = value; break;
            case 15 : d.Bit.D15 = value; break;
            case 16 : d.Bit.D16 = value; break;
            case 17 : d.Bit.D17 = value; break;
            case 18 : d.Bit.D18 = value; break;
            case 19 : d.Bit.D19 = value; break;
            case 20 : d.Bit.D20 = value; break;
            case 21 : d.Bit.D21 = value; break;
            case 22 : d.Bit.D22 = value; break;
            case 23 : d.Bit.D23 = value; break;
            case 24 : d.Bit.D24 = value; break;
            case 25 : d.Bit.D25 = value; break;
            case 26 : d.Bit.D26 = value; break;
            case 27 : d.Bit.D27 = value; break;
            case 28 : d.Bit.D28 = value; break;
            case 29 : d.Bit.D29 = value; break;
            case 30 : d.Bit.D30 = value; break;
            case 31 : d.Bit.D31 = value; break;
            case 32 : d.Bit.D32 = value; break;
            case 33 : d.Bit.D33 = value; break;
            case 34 : d.Bit.D34 = value; break;
            case 35 : d.Bit.D35 = value; break;
            case 36 : d.Bit.D36 = value; break;
            case 37 : d.Bit.D37 = value; break;
            case 38 : d.Bit.D38 = value; break;
            case 39 : d.Bit.D39 = value; break;
            case 40 : d.Bit.D40 = value; break;
            case 41 : d.Bit.D41 = value; break;
            case 42 : d.Bit.D42 = value; break;
            case 43 : d.Bit.D43 = value; break;
            case 44 : d.Bit.D44 = value; break;
            case 45 : d.Bit.D45 = value; break;
            case 46 : d.Bit.D46 = value; break;
            case 47 : d.Bit.D47 = value; break;
            case 48 : d.Bit.D48 = value; break;
            case 49 : d.Bit.D49 = value; break;
            case 50 : d.Bit.D50 = value; break;
            case 51 : d.Bit.D51 = value; break;
            case 52 : d.Bit.D52 = value; break;
            case 53 : d.Bit.D53 = value; break;
            case 54 : d.Bit.D54 = value; break;
            case 55 : d.Bit.D55 = value; break;
            case 56 : d.Bit.D56 = value; break;
            case 57 : d.Bit.D57 = value; break;
            case 58 : d.Bit.D58 = value; break;
            case 59 : d.Bit.D59 = value; break;
            case 60 : d.Bit.D60 = value; break;
            case 61 : d.Bit.D61 = value; break;
            case 62 : d.Bit.D62 = value; break;
            case 63 : d.Bit.D63 = value; break;
            default: break;
          }

          if (MEMORYTYPE == INPUT) {
            (*((LWORD*) &InputBuffer[BYTE_ADDRESS])) = d.LWord;
          } else {
            (*((LWORD*) &OutputBuffer[BYTE_ADDRESS])) = d.LWord;
          }        

          //if (old != d.LWord) sendCANFrame();
        }
        break;

        default:
          break;
      }
    }

    template <uint8_t MEMORYTYPE, typename DATATYPE, uint16_t BYTE_ADDRESS>
    void set(DATATYPE value) {
      // Check if the BYTE_ADDRESS is in the bound, else call error.
      // Then look if the MEMORYTYPE is an INPUT or OUTPUT and write
      // the value into the corresponding buffer.
      if (MEMORYTYPE == INPUT) {
        if ((BYTE_ADDRESS + sizeof(DATATYPE) > KIWI_INPUT_BUFFER_SIZE)) {
          KiwiBase::error('S');
        } else {
          DATATYPE* ptr = (DATATYPE*) &InputBuffer[BYTE_ADDRESS];
          (*ptr) = value;
        }
      } else {
        if ((BYTE_ADDRESS + sizeof(DATATYPE) > KIWI_OUTPUT_BUFFER_SIZE)) {
          KiwiBase::error('S');
        } else {
          DATATYPE* ptr = (DATATYPE*) &OutputBuffer[BYTE_ADDRESS];
          (*ptr) = value;
        }
      }
    }

    template <uint8_t MEMORYTYPE, typename DATATYPE, uint16_t BYTE_ADDRESS, uint8_t BIT_ADDRESS>
    void set(const char *name, DATATYPE value) {
      set<MEMORYTYPE, DATATYPE, BYTE_ADDRESS, BIT_ADDRESS>(value);
    }

    template <uint8_t MEMORYTYPE, typename DATATYPE, uint16_t BYTE_ADDRESS>
    void set(const char *name, DATATYPE value) {
      set<MEMORYTYPE, DATATYPE, BYTE_ADDRESS>(value);
    }

  private:
    template <typename T>
    const char* isWhichDataType() {
      if (std::is_same<T, BOOL>::value)  return "BOOL";
      if (std::is_same<T, SINT>::value)  return "SINT";
      if (std::is_same<T, INT>::value)   return "INT";
      if (std::is_same<T, DINT>::value)  return "DINT";
      if (std::is_same<T, LINT>::value)  return "LINT";
      if (std::is_same<T, USINT>::value) return "USINT";
      if (std::is_same<T, UINT>::value)  return "UINT";
      if (std::is_same<T, UDINT>::value) return "UDINT";
      if (std::is_same<T, ULINT>::value) return "ULINT";
      if (std::is_same<T, BYTE>::value)  return "BYTE";
      if (std::is_same<T, WORD>::value)  return "WORD";
      if (std::is_same<T, DWORD>::value) return "DWORD";
      if (std::is_same<T, LWORD>::value) return "LWORD";
      if (std::is_same<T, REAL>::value)  return "REAL";
      if (std::is_same<T, LREAL>::value) return "LREAL";
      return "E";
    }

    template <unsigned char T>
    const char* isWhichMemoryType() {
      switch(T) {
        case INPUT:
          return "I";
          break;

        case OUTPUT:
          return "Q";
          break;

        default:
          return "E";
          break;
      }
    }

    void beginDescription() {
      // If this method was already called, return.
      if (KiwiBase::DescriptionStarted) return;

      // Set every byte of the JSON buffer to zero.
      memset(&KiwiBase::DescriptionBuffer[0], 0, KIWI_DESCRIPTION_BUFFER_SIZE);

      // Write the beginning JSON text to the description buffer.
      KiwiBase::DescriptionCursor = snprintf((char*) &KiwiBase::DescriptionBuffer[0], KIWI_DESCRIPTION_BUFFER_SIZE, "%s", "{\"Variables\":[");

      // Set KiwiBase::DescriptionStarted to true, so this method will be in the next call ignored.
      KiwiBase::DescriptionStarted = true;
    }

    void endDescription(const char *name, const char *uuid) {
      // If this method was already called, return.
      if (KiwiBase::DescriptionEnded) return;

      // Reset the description-cursor by one character.
      KiwiBase::DescriptionCursor -= 1;

      KIWI_INPUT_BUFFER_SIZE = getBufferCANFDSize(KIWI_INPUT_BUFFER_SIZE);
      KIWI_OUTPUT_BUFFER_SIZE = getBufferCANFDSize(KIWI_OUTPUT_BUFFER_SIZE);

      // Write the last description-information.
      KiwiBase::DescriptionCursor += snprintf((char*) &KiwiBase::DescriptionBuffer[KiwiBase::DescriptionCursor], KIWI_DESCRIPTION_BUFFER_SIZE - KiwiBase::DescriptionCursor, "],\"Name\":\"%s\",\"UUID\":\"%s\",\"Type\":\"%s\",\"InputMemory\":%u,\"OutputMemory\":%u}", name, uuid, "Module", KIWI_INPUT_BUFFER_SIZE, KIWI_OUTPUT_BUFFER_SIZE);

      // Set KiwiBase::DescriptionEnded to true, so this method will be in the next call ignored.
      KiwiBase::DescriptionEnded = true;
    }

    int getBufferCANFDSize(uint8_t size) {
      if ((size > 0) && (size <= 4))  return 4;
      if ((size > 0) && (size <= 8))  return 8;
      if ((size > 8) && (size <= 12)) return 12;
      if ((size > 12) && (size <= 16)) return 16;
      if ((size > 16) && (size <= 20)) return 20;
      if ((size > 20) && (size <= 24)) return 24;
      if ((size > 24) && (size <= 32)) return 32;
      if ((size > 32) && (size <= 48)) return 48;
      if ((size > 48) && (size <= 64)) return 64;
      return 0;
    }

    void processInput(uint32_t rx_id, bool rx_extended, bool rx_request, uint8_t *rx_buffer, uint8_t rx_length) {
      uint16_t tx_id = 0;
      if (rx_request == true) {
        setCommand(tx_id, KIWI_CMD_INPUT);
        setAddress(tx_id, Address);
        sendCANFrame(tx_id, false, false, InputBuffer, KIWI_INPUT_BUFFER_SIZE);
      }
    }

    void processOutput(uint32_t rx_id, bool rx_extended, bool rx_request, uint8_t *rx_buffer, uint8_t rx_length) {
      uint16_t tx_id = 0;
      if (rx_request == true) {
        setCommand(tx_id, KIWI_CMD_OUTPUT);
        setAddress(tx_id, Address);
        sendCANFrame(tx_id, false, false, OutputBuffer, KIWI_OUTPUT_BUFFER_SIZE);
      } else {
        if (rx_length == KIWI_OUTPUT_BUFFER_SIZE) {
          setCommand(tx_id, KIWI_CMD_OUTPUT);
          setAddress(tx_id, Address);
          //sendCANFrame(tx_id, false, false, NULL, 0);
          memcpy(OutputBuffer, rx_buffer, KIWI_OUTPUT_BUFFER_SIZE);
        }
      }
    }
    uint8_t  KIWI_INPUT_BUFFER_SIZE = 0;
    uint8_t  KIWI_OUTPUT_BUFFER_SIZE = 0;
    uint8_t  InputBuffer[64];
    uint8_t  OutputBuffer[64];
};

#endif