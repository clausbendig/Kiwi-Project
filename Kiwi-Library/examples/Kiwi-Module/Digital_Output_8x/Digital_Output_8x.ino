// Uncomment for debug informations.
//#define KIWI_DEBUG

#include <Kiwi.h>

#define DO0 GPIO_NUM_15
#define DO1 GPIO_NUM_16
#define DO2 GPIO_NUM_17
#define DO3 GPIO_NUM_21
#define DO4 GPIO_NUM_22
#define DO5 GPIO_NUM_25
#define DO6 GPIO_NUM_32
#define DO7 GPIO_NUM_33

#define DIS GPIO_NUM_12
#define CS  GPIO_NUM_13
#define WR  GPIO_NUM_14

#define POWER_SUPPLY GPIO_NUM_2
#define OVER_CURRENT GPIO_NUM_34

bool hasOverCurrent = false;
bool hasPower       = false;

KiwiModule Kiwi;

void setup() {
  // Set pin-modes.
  pinMode(DO0, OUTPUT);
  pinMode(DO1, OUTPUT);
  pinMode(DO2, OUTPUT);
  pinMode(DO3, OUTPUT);
  pinMode(DO4, OUTPUT);
  pinMode(DO5, OUTPUT);
  pinMode(DO6, OUTPUT);
  pinMode(DO7, OUTPUT);
  pinMode(DIS, OUTPUT);
  pinMode(WR,  OUTPUT);
  pinMode(CS,  OUTPUT);
  pinMode(POWER_SUPPLY, INPUT);
  pinMode(OVER_CURRENT, INPUT);
  
  // Define outputs.
  Kiwi.define<OUTPUT, BOOL, 0, 0>("DO0");
  Kiwi.define<OUTPUT, BOOL, 0, 1>("DO1");
  Kiwi.define<OUTPUT, BOOL, 0, 2>("DO2");
  Kiwi.define<OUTPUT, BOOL, 0, 3>("DO3");
  Kiwi.define<OUTPUT, BOOL, 0, 4>("DO4");
  Kiwi.define<OUTPUT, BOOL, 0, 5>("DO5");
  Kiwi.define<OUTPUT, BOOL, 0, 6>("DO6");
  Kiwi.define<OUTPUT, BOOL, 0, 7>("DO7");
  
  // Set events.
  Kiwi.onStart(onStart);
  Kiwi.onStop(onStop);
  Kiwi.onError(onError);
  Kiwi.onOutput(onOutput);

  // Set interrupts.
  attachInterrupt(POWER_SUPPLY, onLostPower, RISING);
  attachInterrupt(OVER_CURRENT, onOverCurrent, FALLING);

  // Set pin-states.
  digitalWrite(CS, HIGH);
  digitalWrite(WR, HIGH);

  // Begin Kiwi module.
  Kiwi.begin("Digital Output 8x", "9eb4da2a-7ddd-4be0-9105-67e7fec7a281");
}

void loop() {
  Kiwi.loop();
}

void onLostPower() {
  Kiwi.error('P');
}

void onOverCurrent() {
 Kiwi.error('O');
}

void onStart() {
  digitalWrite(DIS, HIGH);
}

void onStop() {
  Kiwi.set<OUTPUT, BYTE, 0>(0);
  digitalWrite(DIS, LOW);
}

void onError(char msg) {
  Kiwi.set<OUTPUT, BYTE, 0>(0);
  digitalWrite(DIS, LOW);
}

bool onOutput() {
  // Read diagnose pins.
  hasPower       = !digitalRead(POWER_SUPPLY);
  hasOverCurrent = !digitalRead(OVER_CURRENT);

  if (hasPower && !hasOverCurrent) {
    // Begin writing.
    digitalWrite(CS, LOW);
    digitalWrite(WR, LOW);

    // Write digital outputs.
    digitalWrite(DO0, Kiwi.get<OUTPUT, BOOL, 0, 0>());
    digitalWrite(DO1, Kiwi.get<OUTPUT, BOOL, 0, 1>());
    digitalWrite(DO2, Kiwi.get<OUTPUT, BOOL, 0, 2>());
    digitalWrite(DO3, Kiwi.get<OUTPUT, BOOL, 0, 3>());
    digitalWrite(DO4, Kiwi.get<OUTPUT, BOOL, 0, 4>());
    digitalWrite(DO5, Kiwi.get<OUTPUT, BOOL, 0, 5>());
    digitalWrite(DO6, Kiwi.get<OUTPUT, BOOL, 0, 6>());
    digitalWrite(DO7, Kiwi.get<OUTPUT, BOOL, 0, 7>());

    // End writing.
    digitalWrite(CS, HIGH);
    digitalWrite(WR, HIGH);
    return true;
  } else {
    return false;
  }
}
