// Uncomment for debug informations.
//#define KIWI_DEBUG

#include <Kiwi.h>

#define POWER_SUPPLY GPIO_NUM_34
#define DB0          GPIO_NUM_32
#define DB1          GPIO_NUM_33
#define CLK          GPIO_NUM_14
#define SOP          GPIO_NUM_12
#define LD           GPIO_NUM_13
#define CE           GPIO_NUM_15
#define EN           GPIO_NUM_16

KiwiModule Kiwi;

void setup() {
  // Set pin-modes.
  pinMode(POWER_SUPPLY, INPUT);
  pinMode(DB0,          OUTPUT);
  pinMode(DB1,          OUTPUT);
  pinMode(CLK,          OUTPUT);
  pinMode(SOP,          INPUT);
  pinMode(LD,           OUTPUT);
  pinMode(CE,           OUTPUT);
  pinMode(EN,           OUTPUT);

  // Define inputs.
  Kiwi.define<INPUT, BOOL, 0, 0>("DI0");
  Kiwi.define<INPUT, BOOL, 0, 1>("DI1");
  Kiwi.define<INPUT, BOOL, 0, 2>("DI2");
  Kiwi.define<INPUT, BOOL, 0, 3>("DI3");
  Kiwi.define<INPUT, BOOL, 0, 4>("DI4");
  Kiwi.define<INPUT, BOOL, 0, 5>("DI5");
  Kiwi.define<INPUT, BOOL, 0, 6>("DI6");
  Kiwi.define<INPUT, BOOL, 0, 7>("DI7");

  // Set events.
  Kiwi.onStart(onStart);
  Kiwi.onStop(onStop);
  Kiwi.onError(onError);
  Kiwi.onInput(onInput);

  // Set interrupts.
  attachInterrupt(POWER_SUPPLY, onLostPower, RISING);

  // Set pin-states.
  digitalWrite(CLK, LOW);  
  digitalWrite(LD, HIGH);
  digitalWrite(CE, HIGH);
  digitalWrite(EN, HIGH);

  // Set debounce to 3ms.
  // DB0 = LOW;  DB1 = LOW;  => 3ms.
  // DB0 = HIGH; DB1 = LOW;  => 1ms.
  // DB0 = LOW;  DB1 = HIGH; => 0ms.
  digitalWrite(DB0, LOW);
  digitalWrite(DB1, LOW);

  // Begin Kiwi module.
  Kiwi.begin("Digital Input 8x", "1cf0d108-975b-42ba-9a0f-45da0c5ab162");
}

void loop() { 
  Kiwi.loop();
}

byte readInputs() {
  byte b = 0; byte i;

  // Parallel load.
  digitalWrite(LD, LOW);
  digitalWrite(LD, HIGH);

  // Shift in serial input data.
  digitalWrite(CE, LOW);
  for(i = 0; i < 8; ++i) {
    digitalWrite(CLK, HIGH);
    b |= digitalRead(SOP) << (7 - i);
    digitalWrite(CLK, LOW);
  }
  digitalWrite(CE, HIGH);
  
  return b;
}

void onLostPower() {
  Kiwi.error('P');
}

void onStart() {
}

void onStop() {
}

void onError(char msg) {
}

bool onInput() { 
  // Set digital inputs.
  if (digitalRead(POWER_SUPPLY) == HIGH) {
    return false;
  } else {
    Kiwi.set<INPUT, BYTE, 0>(readInputs());
    return true;
  }
}
