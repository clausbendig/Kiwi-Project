// Uncomment for debug informations.
//#define KIWI_DEBUG

#include <Kiwi.h>
#include <RCSwitch.h>

RCSwitch rcSwitch = RCSwitch();

KiwiModule<8, 35> Kiwi;

struct {
  BOOL  Received;
  BYTE  Protocol;
  BYTE  BitLength;
  WORD  PulseLength;
  DWORD Value;
} Code;

void setup() {
  // Enable RC-Switch.
  rcSwitch.enableTransmit(GPIO_NUM_16);
  rcSwitch.enableReceive(GPIO_NUM_12);

  // Define outputs.
  Kiwi.define<OUTPUT, BOOL, 0, 0>("OnA");
  Kiwi.define<OUTPUT, BOOL, 0, 1>("OnB");
  Kiwi.define<OUTPUT, BOOL, 0, 2>("OnC");
  Kiwi.define<OUTPUT, BOOL, 0, 3>("OnD");
  Kiwi.define<OUTPUT, BOOL, 0, 4>("OffA");
  Kiwi.define<OUTPUT, BOOL, 0, 5>("OffB");
  Kiwi.define<OUTPUT, BOOL, 0, 6>("OffC");
  Kiwi.define<OUTPUT, BOOL, 0, 7>("OffD");

  Kiwi.define<OUTPUT, DWORD,  1>("OnA_Code");
  Kiwi.define<OUTPUT, DWORD,  5>("OnB_Code");
  Kiwi.define<OUTPUT, DWORD,  9>("OnC_Code");
  Kiwi.define<OUTPUT, DWORD, 13>("OnD_Code");
  Kiwi.define<OUTPUT, DWORD, 17>("OffA_Code");
  Kiwi.define<OUTPUT, DWORD, 21>("OffB_Code");
  Kiwi.define<OUTPUT, DWORD, 25>("OffC_Code");
  Kiwi.define<OUTPUT, DWORD, 29>("OffD_Code");
  
  Kiwi.define<OUTPUT, BYTE, 30>("Protocol");
  Kiwi.define<OUTPUT, WORD, 31>("PulseLength");
  Kiwi.define<OUTPUT, BYTE, 33>("RepeatTransmit");
  Kiwi.define<OUTPUT, BYTE, 34>("BitLength");

  // Define inputs.
  Kiwi.define<INPUT, BOOL,  0, 0>("Received");
  Kiwi.define<INPUT, BYTE,  1   >("Protocol");
  Kiwi.define<INPUT, WORD,  2   >("BitLength");
  Kiwi.define<INPUT, WORD,  3   >("PulseLength");
  Kiwi.define<INPUT, DWORD, 4   >("Value");

  // Set events.
  Kiwi.onStart(onStart);
  Kiwi.onStop(onStop);
  Kiwi.onError(onError);
  Kiwi.onInput(onInput);
  Kiwi.onOutput(onOutput);


  // Begin Kiwi module.
  Kiwi.begin("RC-Switch", "15c163ac-879e-4eab-bab1-c08d301266a6");
}

void loop() {
  if (rcSwitch.available()) {
    Code.Received    = true;
    Code.Protocol    = rcSwitch.getReceivedProtocol();
    Code.BitLength   = rcSwitch.getReceivedBitlength();
    Code.PulseLength = rcSwitch.getReceivedDelay();
    Code.Value       = rcSwitch.getReceivedValue();
  }
  Kiwi.loop();
}

void onStart() {
}

void onStop() {
}

void onError(char msg) {
}

void onInput() {
  Kiwi.set<INPUT, BOOL,  0, 0>(Code.Received);
  Kiwi.set<INPUT, BYTE,  1   >(Code.Protocol);
  Kiwi.set<INPUT, WORD,  2   >(Code.BitLength);
  Kiwi.set<INPUT, WORD,  3   >(Code.PulseLength);
  Kiwi.set<INPUT, DWORD, 4   >(Code.Value);
  Code.Received = false;
}

void onOutput() {
  rcSwitch.setProtocol(Kiwi.get<OUTPUT, BYTE, 30>());
  rcSwitch.setPulseLength(Kiwi.get<OUTPUT, WORD, 31>());
  rcSwitch.setRepeatTransmit(Kiwi.get<OUTPUT, BYTE, 33>());

  if (Kiwi.get<OUTPUT, BOOL, 0, 0>() == true) rcSwitch.send(Kiwi.get<OUTPUT, DWORD,  1>(), Kiwi.get<OUTPUT, BYTE, 34>());
  if (Kiwi.get<OUTPUT, BOOL, 0, 1>() == true) rcSwitch.send(Kiwi.get<OUTPUT, DWORD,  5>(), Kiwi.get<OUTPUT, BYTE, 34>());
  if (Kiwi.get<OUTPUT, BOOL, 0, 2>() == true) rcSwitch.send(Kiwi.get<OUTPUT, DWORD,  9>(), Kiwi.get<OUTPUT, BYTE, 34>());
  if (Kiwi.get<OUTPUT, BOOL, 0, 3>() == true) rcSwitch.send(Kiwi.get<OUTPUT, DWORD, 13>(), Kiwi.get<OUTPUT, BYTE, 34>());
  if (Kiwi.get<OUTPUT, BOOL, 0, 4>() == true) rcSwitch.send(Kiwi.get<OUTPUT, DWORD, 17>(), Kiwi.get<OUTPUT, BYTE, 34>());
  if (Kiwi.get<OUTPUT, BOOL, 0, 5>() == true) rcSwitch.send(Kiwi.get<OUTPUT, DWORD, 21>(), Kiwi.get<OUTPUT, BYTE, 34>());
  if (Kiwi.get<OUTPUT, BOOL, 0, 6>() == true) rcSwitch.send(Kiwi.get<OUTPUT, DWORD, 25>(), Kiwi.get<OUTPUT, BYTE, 34>());
  if (Kiwi.get<OUTPUT, BOOL, 0, 7>() == true) rcSwitch.send(Kiwi.get<OUTPUT, DWORD, 29>(), Kiwi.get<OUTPUT, BYTE, 34>());
}