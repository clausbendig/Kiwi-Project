#ifndef Thread_h
#define Thread_h

#include <pthread.h>
#include <sched.h>

typedef struct {
  pthread_mutex_t Mutex;
  pthread_mutexattr_t Attributes;
} Mutex;

typedef struct {
  pthread_t Thread;
  pthread_attr_t Attributes;
  struct sched_param Parameters;
} Thread;

#define createMutex(m)                                                 \
  pthread_mutexattr_init(&m.Attributes);                               \
  pthread_mutexattr_setprotocol(&m.Attributes, PTHREAD_PRIO_INHERIT);  \
  pthread_mutex_init(&m.Mutex, &m.Attributes);

#define lockMutex(m)                                                   \
  pthread_mutex_lock( &m.Mutex );

#define unlockMutex(m)                                                 \
  pthread_mutex_unlock( &m.Mutex );

#define destroyMutex(m)                                                \
  pthread_mutexattr_destroy(&m.Attributes);                            \
  pthread_mutex_destroy(&m.Mutex);

#define createThread(t, priority)                                      \
  pthread_attr_init(&t.Attributes);                                    \
  pthread_attr_setstacksize(&t.Attributes, PTHREAD_STACK_MIN);         \
  pthread_attr_setschedpolicy(&t.Attributes, SCHED_FIFO);              \
  t.Parameters.sched_priority = priority;                              \
  pthread_attr_setschedparam(&t.Attributes, &t.Parameters);            \
  pthread_attr_setinheritsched(&t.Attributes, PTHREAD_EXPLICIT_SCHED);

#define startThread(t, function, input)                                \
  pthread_create(&t.Thread, &t.Attributes, function, (void*) input);

#define stopThread(t)                                                  \
  pthread_join(t.Thread, NULL);

#define exitThread(output)                                             \
  pthread_exit(NULL);

#define destroyThread(t)                                               \
  pthread_attr_destroy(&t.Attributes);

#endif
