#include <PLC.h>

// Prototypes of functions provided by Matiec.
void config_init__(void);
void config_run__(unsigned long tick);

// Variables used by MatIEC.
IEC_TIME __CURRENT_TIME;
IEC_BOOL __DEBUG = 0;
unsigned long __tick = 0;
char *PLC_ID = NULL;
extern unsigned long common_ticktime__;
extern unsigned long greatest_tick_count__;

// Help to quit cleanly when initialization fails at a certain level.
int init_level = 0;

static void getTime(IEC_TIME *current) {
    struct timespec tmp;
    clock_gettime(CLOCK_REALTIME, &tmp);
    current->tv_sec = tmp.tv_sec;
    current->tv_nsec = tmp.tv_nsec;
}

static void timerNotify(sigval_t val) {
    getTime(&__CURRENT_TIME);
    sem_post(&PLC.Timer.Tick);
}

static void setTimer(unsigned long long next, unsigned long long period) {
  struct itimerspec timerValues;
  memset (&timerValues, 0, sizeof (struct itimerspec));
  timerValues.it_value.tv_sec = next / 1000000000;
  timerValues.it_value.tv_nsec = next % 1000000000;
  timerValues.it_interval.tv_sec = period / 1000000000;
  timerValues.it_interval.tv_nsec = period % 1000000000;
  timer_settime(PLC.Timer.Instance, 0, &timerValues, NULL);
}

void startTimer() {
  if (PLC.Timer.isRunning == false) {
    pthread_attr_init(&PLC.Timer.Attribute);
    PLC.Timer.ScheduleParameters.sched_priority = 255;
    pthread_attr_setschedparam(&PLC.Timer.Attribute, &PLC.Timer.ScheduleParameters);

    struct sigevent sigev;
    memset(&sigev, 0, sizeof (struct sigevent));
    sigev.sigev_value.sival_int = 50;
    sigev.sigev_notify = SIGEV_THREAD;
    sigev.sigev_notify_attributes = &PLC.Timer.Attribute;
    sigev.sigev_notify_function = timerNotify;
    
    timer_create(CLOCK_MONOTONIC, &sigev, &PLC.Timer.Instance);
    setTimer(common_ticktime__, common_ticktime__);

    PLC.Timer.isRunning = true;
  }
}

void stopTimer() {
  if (PLC.Timer.isRunning == true) {
    setTimer(0, 0);
    timer_delete(PLC.Timer.Instance);
    PLC.Timer.isRunning = false;
  }
}

int initializePLC(void) {

  int res = 0;
  init_level = 0;

  if(!common_ticktime__) common_ticktime__ = 1000000;

  sem_init(&PLC.Timer.Tick, 0, 0);

  startTimer();

  config_init__();

  return res;
}

void runPLC(void) {
  sem_wait(&PLC.Timer.Tick);
  __tick++;
  if (greatest_tick_count__)
    __tick %= greatest_tick_count__;

  config_run__(__tick);
}

void deinitializePLC(void) {
  sem_post(&PLC.Timer.Tick);
  stopTimer();
	stopThread(PLC.Program);
  sem_destroy(&PLC.Timer.Tick);
}

void printOut(const char * format, ...) {
  char buffer[1024];
  va_list args;
  va_start (args, format);
  vsnprintf (buffer, 1024, format, args);
  if (printOutCallback != NULL) printOutCallback(buffer);
  va_end (args);
}

void printErr(const char * format, ...) {
  char buffer[1024];
  va_list args;
  va_start (args, format);
  vsnprintf (buffer, 1024, format, args);
  if (printErrCallback != NULL) printErrCallback(buffer);
  va_end (args);
}

void callError() {
  if (callErrorCallback != NULL) callErrorCallback();
}

