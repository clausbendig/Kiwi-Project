#include <Interfaces/Kiwi/Kiwi.h>
#include <PLC.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sched.h>
#include <limits.h>
#include <stdint.h>
#include <pthread.h>
#include <assert.h>
#include <sys/time.h>
#include <inttypes.h>

#include <linux/can.h>
#include <linux/can/raw.h>

#define KIWI_TIME_OUT 500000
#define KIWI_HEARTBEAT_TIME 200

typedef struct {
  bool     Extended;
  bool     Request;
  uint8_t  Command;
  uint8_t  Address;
  uint16_t Data;
  uint8_t  Payload[64];
  uint8_t  Size;
} KiwiPackage;

typedef struct canfd_frame CanPackage;

static size_t toCAN(KiwiPackage *kiwi, CanPackage *can) {
  size_t meta = 0;

  // Weird, the ID is 4 when it's created.
  can->can_id = 0;
  
  if (kiwi->Extended == true) {
    setCommand(meta, kiwi->Command);
    setAddress(meta, kiwi->Address);
    setMeta(can->can_id, meta);
    setData(can->can_id, kiwi->Data);
    can->can_id |= CAN_EFF_FLAG;
  } else {
    setCommand(can->can_id,  kiwi->Command);
    setAddress(can->can_id,  kiwi->Address);
  }

  memcpy(&can->data[0], &kiwi->Payload[0], kiwi->Size);
  can->len = kiwi->Size;

  if (kiwi->Request) {
    can->can_id |= CAN_RTR_FLAG;
    return CAN_MTU;
  } else {
    return CANFD_MTU;
  }
}

static size_t toKiwi(KiwiPackage *kiwi, CanPackage *can) {
  size_t meta;
  bool isExtended = can->can_id & CAN_EFF_FLAG;
  bool isRequest  = can->can_id & CAN_RTR_FLAG;
  bool isError    = can->can_id & CAN_ERR_FLAG;

  if (isError   == true) return -1;
  if (isRequest == true) return -1;

  if (isExtended == true) {
    meta           = extractMeta(can->can_id);
    kiwi->Data     = extractData(can->can_id);
    kiwi->Command  = extractCommand(meta);
    kiwi->Address  = extractAddress(meta);
  } else {
    kiwi->Command  = extractCommand(can->can_id);
    kiwi->Address  = extractAddress(can->can_id);
  }  

  kiwi->Size = can->len;
  memcpy(&kiwi->Payload[0], &can->data[0], kiwi->Size);

  return kiwi->Size;
}

static int openKiwiInterface(Kiwi *interface) {
  // Generate socket for CAN.
  if((interface->Socket = socket(PF_CAN, SOCK_RAW, CAN_RAW)) < 0) return -1;

  // Interface configurator request.
  struct ifreq ifr;
  strcpy(ifr.ifr_name, interface->CAN);
  if (ioctl(interface->Socket, SIOCGIFINDEX, &ifr) < 0) return -1;

  // Set option to time-out on reading.
  struct timeval tv;
  tv.tv_sec = 0;
  tv.tv_usec = KIWI_TIME_OUT;
  if (setsockopt(interface->Socket, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv)) < 0) return -1;
  if (setsockopt(interface->Socket, SOL_SOCKET, SO_SNDTIMEO, &tv, sizeof(tv)) < 0) return -1;

  // Set option to receive errors.
  can_err_mask_t err_mask;
  err_mask = CAN_ERR_MASK;
  if (setsockopt(interface->Socket, SOL_CAN_RAW, CAN_RAW_ERR_FILTER, &err_mask, sizeof(err_mask)) < 0) return -1;

  // Set option to support CAN-FD.
  int enable_canfd = 1;
  if (setsockopt(interface->Socket, SOL_CAN_RAW, CAN_RAW_FD_FRAMES, &enable_canfd, sizeof(enable_canfd)) < 0) return -1;


  // Select that CAN interface, and bind the socket to it.
  struct sockaddr_can addr;
  addr.can_family = AF_CAN;
  addr.can_ifindex = ifr.ifr_ifindex;
  if (bind(interface->Socket, (struct sockaddr*) &addr, sizeof(addr)) < 0) return -1;

  interface->isSocketOpened = true;

  // Everything went good.
  return 0;
}

static int closeKiwiInterface(Kiwi *interface) {
  interface->isSocketOpened = false;
  return close(interface->Socket);
}

static int sendPackage(Kiwi *interface, KiwiPackage *kiwi) {
  CanPackage frame;
  int mtu = toCAN(kiwi, &frame);
  int sent = send(interface->Socket, &frame, mtu, 0);
  return sent;
}

static int readPackage(Kiwi *interface, KiwiPackage *kiwi) {
  CanPackage frame;
  int len = read(interface->Socket, &frame, sizeof(frame));
  if ((len == CANFD_MTU)) {
    int klen = toKiwi(kiwi, &frame);
    return klen;
  } else {
    if (len == CAN_MTU) {
      return readPackage(interface, kiwi);
    }
    return -1;
  }
}

static int sendError(Kiwi* interface) {
  KiwiPackage kiwiOut;
  
  kiwiOut.Command  = KIWI_CMD_ERROR;
  kiwiOut.Address  = 0;
  kiwiOut.Request  = false;
  kiwiOut.Extended = false;
  kiwiOut.Data     = 0;
  kiwiOut.Size     = 0;

  
  if (sendPackage(interface, &kiwiOut) < 0) return -1;

  return 0;
}

static int readInput(Kiwi* interface, KiwiModuleDescription *module) {
  if (module->Buffers->Input.BufferSize == 0) return 0;

  KiwiPackage kiwiOut;
  KiwiPackage kiwiIn;
 
  kiwiOut.Command  = KIWI_CMD_INPUT;
  kiwiOut.Address  = module->Address;
  kiwiOut.Request  = true;
  kiwiOut.Extended = false;
  kiwiOut.Data     = 0;
  kiwiOut.Size     = 0;

  if (sendPackage(interface, &kiwiOut) < 0) return -1;
  if (readPackage(interface, &kiwiIn) < 0)  return -2;

  if (kiwiIn.Command != KIWI_CMD_INPUT)  return -1;
  if (kiwiIn.Address != module->Address) return -1;
  if (kiwiIn.Size != module->Buffers->Input.BufferSize) return -1;

  memcpy(&module->Buffers->Input.BackBuffer[0], &kiwiIn.Payload[0], module->Buffers->Input.BufferSize);

  return 0;
}

static int sendOutput(Kiwi* interface, KiwiModuleDescription *module) {
  if (module->Buffers->Output.BufferSize == 0) return 0;
  KiwiPackage kiwiIn;
  KiwiPackage kiwiOut;
  
  kiwiOut.Command  = KIWI_CMD_OUTPUT;
  kiwiOut.Address  = module->Address;
  kiwiOut.Request  = false;
  kiwiOut.Extended = false;
  kiwiOut.Data     = 0;
  kiwiOut.Size     = module->Buffers->Output.BufferSize;

  memcpy(&kiwiOut.Payload[0], &module->Buffers->Output.BackBuffer[0], module->Buffers->Output.BufferSize);
  
  if (sendPackage(interface, &kiwiOut) < 0) return -1;

  kiwiOut.Request  = true;
  kiwiOut.Size     = 0;

  if (sendPackage(interface, &kiwiOut) < 0) return -2;
  if (readPackage(interface, &kiwiIn) < 0)  return -3;

  if (kiwiIn.Command != KIWI_CMD_OUTPUT)  return -4;
  if (kiwiIn.Address != module->Address) return -5;
  if (kiwiIn.Size    != module->Buffers->Output.BufferSize) return -6;

  if (memcmp(&kiwiIn.Payload[0], &kiwiOut.Payload[0], module->Buffers->Output.BufferSize) != 0) return -7;
  
  return 0;
}

static int startKiwi(Kiwi* interface) {
  KiwiPackage kiwiOut;
  kiwiOut.Command  = KIWI_CMD_START;
  kiwiOut.Address  = 0;
  kiwiOut.Request  = false;
  kiwiOut.Extended = false;
  kiwiOut.Data     = 0;
  kiwiOut.Size     = 0;

  if (sendPackage(interface, &kiwiOut) < 0) return -1;
  return 0;
}

static int stopKiwi(Kiwi* interface) {
  KiwiPackage kiwiOut;
  kiwiOut.Command  = KIWI_CMD_STOP;
  kiwiOut.Address  = 0;
  kiwiOut.Request  = false;
  kiwiOut.Extended = false;
  kiwiOut.Data     = 0;
  kiwiOut.Size     = 0;

  if (sendPackage(interface, &kiwiOut) < 0) return -1;
  return 0;
}

static int sendHeartbeat(Kiwi *interface) {
  KiwiPackage kiwiOut;
  kiwiOut.Command  = KIWI_CMD_HEARTBEAT;
  kiwiOut.Address  = 0;
  kiwiOut.Request  = false;
  kiwiOut.Extended = false;
  kiwiOut.Data     = 0;
  kiwiOut.Size     = 0;

  if (sendPackage(interface, &kiwiOut) < 0) return -1;
  return 0;
}

static void heartBeatTimerNotify(sigval_t val) {
  Kiwi *interface = (Kiwi*) val.sival_ptr;
  lockMutex(interface->HeartBeatTimerMutex);
  sendHeartbeat(interface);
  unlockMutex(interface->HeartBeatTimerMutex);
}

static void setHeartbeatTimer(Kiwi* interface, unsigned long long next, unsigned long long period) {
  struct itimerspec timerValues;
  memset (&timerValues, 0, sizeof (struct itimerspec));
  timerValues.it_value.tv_sec = next / 1000000000;
  timerValues.it_value.tv_nsec = next % 1000000000;
  timerValues.it_interval.tv_sec = period / 1000000000;
  timerValues.it_interval.tv_nsec = period % 1000000000;
  timer_settime(interface->HeartBeatTimer.Instance, 0, &timerValues, NULL);
}

static void startHeartBeatTimer(Kiwi* interface) {
  if (interface->HeartBeatTimer.isRunning == false) {
    pthread_attr_init(&interface->HeartBeatTimer.Attribute);
    interface->HeartBeatTimer.ScheduleParameters.sched_priority = 56;
    pthread_attr_setschedparam(&interface->HeartBeatTimer.Attribute, &interface->HeartBeatTimer.ScheduleParameters);

    struct sigevent sigev;
    memset(&sigev, 0, sizeof (struct sigevent));
    sigev.sigev_value.sival_int = 55;
    sigev.sigev_value.sival_ptr = interface;
    sigev.sigev_notify = SIGEV_THREAD;
    sigev.sigev_notify_attributes = &interface->HeartBeatTimer.Attribute;
    sigev.sigev_notify_function = heartBeatTimerNotify;
    
    timer_create(CLOCK_MONOTONIC, &sigev, &interface->HeartBeatTimer.Instance);
    setHeartbeatTimer(interface, KIWI_HEARTBEAT_TIME * 1000000 , KIWI_HEARTBEAT_TIME * 1000000);

    interface->HeartBeatTimer.isRunning = true;
  }
}

static void stopHeartBeatTimer(Kiwi* interface) {
  if (interface->HeartBeatTimer.isRunning == true) {
    setHeartbeatTimer(interface, 0, 0);
    timer_delete(interface->HeartBeatTimer.Instance);
    interface->HeartBeatTimer.isRunning = false;
  }
}

int beginKiwi(Kiwi* interface) {
  // Open Kiwi interface.
  openKiwiInterface(interface);

  interface->Cycle = 0;

  int len = -1;
  uint32_t heartBeatTime = KIWI_HEARTBEAT_TIME + 20;
  KiwiPackage kiwiOut;
  KiwiPackage kiwiIn;

  // Verify Kiwi modules.
  for (int i = 0; i < interface->Count; i++) {
    
    kiwiOut.Command  = KIWI_CMD_UUID;
    kiwiOut.Address  = (*interface->Modules[i]).Address;
    kiwiOut.Request  = true;
    kiwiOut.Extended = false;
    kiwiOut.Data     = 0;
    kiwiOut.Size     = 0;
   
    if (sendPackage(interface, &kiwiOut) < 0) return -1;
    if ((len = readPackage(interface, &kiwiIn)) == 16) {
      if (memcmp(&kiwiIn.Payload[0], &(*interface->Modules[i]).UUID[0], 16) != 0) {
        printErr("FAILED. \n \u2570\u257C Module not in correct place.\n");
        closeKiwiInterface(interface);
        return -1;
      }
    } else {
      printErr("FAILED. \n \u2570\u257C Communication time-out.\n");
      closeKiwiInterface(interface);
      return -1;
    }
  }

  kiwiOut.Command  = KIWI_CMD_HEARTBEAT_TIME;
  kiwiOut.Address  = 0;
  kiwiOut.Request  = false;
  kiwiOut.Extended = false;
  kiwiOut.Data     = 0;
  kiwiOut.Size     = sizeof(heartBeatTime);
  
  memcpy(&kiwiOut.Payload[0], &heartBeatTime, sizeof(heartBeatTime));
  if (sendPackage(interface, &kiwiOut) < 0) return -1;

  //startHeartBeatTimer(interface);

  if (startKiwi(interface) < 0) return -1;


  return 0;
}

int flipKiwiInputs(Kiwi* interface) {
  int h = 0;
  for (h = 0; h < interface->Count; h++) {
    if (readInput(interface, interface->Modules[h]) < 0) {
      printErr("PLC: Couldn't read input-image from module %u.\n", interface->Modules[h]->Address);
      return -1;
    }
    (*interface->Modules[h]).flipInputs();
  }
  
  return 0;
}

int flipKiwiOutputs(Kiwi* interface) {
  int h = 0; int err = 0;
  for (h = 0; h < interface->Count; h++) {
    if ((err = sendOutput(interface, interface->Modules[h])) < 0)
    {
      printErr("PLC: Couldn't send output-image to module %u.Error: %d.\n", interface->Modules[h]->Address, err);
      return -1;
    } 
    (*interface->Modules[h]).flipOutputs();
  }
  
  return 0;
}

int cleanKiwi(Kiwi* interface) {
  stopKiwi(interface);
  //stopHeartBeatTimer(interface);
  closeKiwiInterface(interface);
  return 0;
}

void errorKiwi(Kiwi* interface) {
  if (interface->isSocketOpened == true) {
    sendError(interface);
    //stopHeartBeatTimer(interface);
    closeKiwiInterface(interface);
  }
}