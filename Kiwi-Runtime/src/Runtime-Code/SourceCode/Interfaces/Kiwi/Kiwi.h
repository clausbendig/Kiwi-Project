#ifndef Kiwi_h
#define Kiwi_h

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include <PLC.h>

// Commands.
#define KIWI_CMD_ERROR                  0
#define KIWI_CMD_STOP                   1
#define KIWI_CMD_HEARTBEAT              2
#define KIWI_CMD_START                  3
#define KIWI_CMD_OUTPUT                 4
#define KIWI_CMD_INPUT                  5
#define KIWI_CMD_HEARTBEAT_TIME         6
#define KIWI_CMD_UUID                   7
#define KIWI_CMD_DESCRIPTION            8
#define KIWI_CMD_DESCRIPTION_SHA256     9
#define KIWI_CMD_SEARCH                10
#define KIWI_CMD_RESERVED_1            11
#define KIWI_CMD_RESERVED_2            12
#define KIWI_CMD_RESERVED_3            13
#define KIWI_CMD_RESERVED_4            14
#define KIWI_CMD_ECHO                  15

// 29-bit extraction.
#define extractMeta(val) ((val >> 0x12) & 0x7FF)
#define extractData(val) (val & 0x3FFFF)

// 29-bit setting.
#define setMeta(val, meta) (val |= (meta << 0x12))
#define setData(val, data) (val |= data)

// 11-bit extraction.
#define extractCommand(val)  ((val >> 0x07) & 0x0F)
#define extractAddress(val)  (val & 0x7F)

// 11-bit setting.
#define setCommand(val, cmd)   (val |= (cmd  << 0x07))
#define setAddress(val, addr)  (val |= addr)

#define bitRead(value, bit) (((value) >> (bit)) & 0x01)
#define bitSet(value, bit) ((value) |= (1UL << (bit)))
#define bitClear(value, bit) ((value) &= ~(1UL << (bit)))
#define bitWrite(value, bit, bitvalue) (bitvalue ? bitSet(value, bit) : bitClear(value, bit))

typedef void (*KiwiBegin)();
typedef void (*KiwiFlip)();
typedef void (*KiwiClean)();

typedef struct {
  uint8_t   Address;
  uint8_t   Priority;
  uint8_t  *UUID;
  BufferIO *Buffers;
  KiwiBegin begin;
  KiwiFlip  flipInputs;
  KiwiFlip  flipOutputs;
  KiwiClean clean;
} KiwiModuleDescription;

typedef struct {
  const char *CAN;
  int Socket;
  int Count;
  uint64_t Cycle;
  bool isSocketOpened;
  Timer_t HeartBeatTimer;
  Mutex HeartBeatTimerMutex;
  KiwiModuleDescription** Modules;
} Kiwi;

int beginKiwi(Kiwi* interface);
int flipKiwiInputs(Kiwi* interface);
int flipKiwiOutputs(Kiwi* interface);
int cleanKiwi(Kiwi* interface);
void errorKiwi(Kiwi* interface);

#endif