#ifndef PLC_h
#define PLC_h

#include <pthread.h>
#include <stdio.h>
#include <unistd.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/mman.h>
#include <limits.h>
#include <time.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <signal.h>
#include <fcntl.h>
#include <string.h>
#include <net/if.h>
#include <semaphore.h>

#include <iec_types.h>
#include <iec_std_lib.h>

#include <Thread/Thread.h>

typedef void (*printCallback)(const char*);
typedef void (*errorCallback)();

printCallback printOutCallback;
printCallback printErrCallback;
errorCallback callErrorCallback;

void printOut(const char * format, ...);
void printErr(const char * format, ...);
void callError();

typedef struct {
  const char *Name;
  const char *DataType;
  const char *MemoryType;
  const char *Address;
  void *Variable;
} DefinitionIO;

typedef struct {
  uint8_t *FrontBuffer;
  uint8_t *BackBuffer;
  uint8_t BufferSize;
} DoubleBuffer;

typedef struct {
  DoubleBuffer Input;
  DoubleBuffer Output;
} BufferIO;

typedef struct {
  timer_t Instance;
  sem_t   Tick;
  bool    isRunning;
  pthread_attr_t Attribute;
  struct sched_param ScheduleParameters;
} Timer_t;

typedef struct {
  Thread  Program;
  Mutex   RunMutex;
  Mutex   ResourcesMutex;
  Timer_t Timer;
  bool    Run;
} PLC_t;

PLC_t PLC;

int initializePLC(void);
void runPLC(void);
void deinitializePLC(void);

int beginInterfaces();
int flipInputs();
int flipOutputs();
int cleanInterfaces();
void errorInterfaces();

void startTimer();
void stopTimer();

#endif