#include <PLC.h>

bool isRunning = false;

int TickCycle = 0;

void *program(void *arg) {
  bool errorOccurred = false;

  // Initialise PLC.
  printOut("PLC: Intializing PLC resources ... ");
	initializePLC();
  printOut("OK.\n");

  printOut("PLC: Intializing PLC interfaces ... ");
	if (beginInterfaces() >= 0) {
    printOut("OK.\n");  
  } else {
    errorOccurred = true;
    goto ON_ERROR;
  };
  
  printOut("PLC: Program started.\n");
  lockMutex(PLC.RunMutex);
	while(PLC.Run == true) {
    unlockMutex(PLC.RunMutex);

    // Run program.
    lockMutex(PLC.ResourcesMutex);
    if (flipInputs() < 0) errorOccurred = true;
    runPLC();
    if (flipOutputs() < 0) errorOccurred = true;
    unlockMutex(PLC.ResourcesMutex);

    if (errorOccurred == true) {
      goto ON_ERROR;
    };

    lockMutex(PLC.RunMutex);
	}
  unlockMutex(PLC.RunMutex);

  ON_ERROR:

    stopTimer();

    if (errorOccurred == true) {
      errorInterfaces();
      printErr("PLC: An interface error occurred.\n");
    } else {
      // Stop interfaces.
      printOut("PLC: Stopping PLC interfaces ... ");
      if (cleanInterfaces() >= 0) {
        printOut("OK.\n");  
      } else {
        printOut("FAILED.\n");  
      };
    }

    // Clean up PLC resources.
    printOut("PLC: Programm stopped.\n");

    // Destroy mutexs.
    destroyMutex(PLC.RunMutex);
    destroyMutex(PLC.ResourcesMutex);

    isRunning = false;

    if (errorOccurred == true) {
      callError();
    }

    exitThread(NULL);
}

int startPLC(printCallback out, printCallback err, errorCallback onErr) {
  if (!isRunning) {
    
    // Set print callbacks.
    printOutCallback = out;
    printErrCallback = err;
    callErrorCallback = onErr;
 
    if(mlockall(MCL_CURRENT | MCL_FUTURE) == -1) {
      printErr("PLC: Locking memory failed.\n");
      return -1;
    }

    printOut("PLC: Starting program ... ");

    createMutex(PLC.RunMutex);
    createMutex(PLC.ResourcesMutex);
    createThread(PLC.Program, 78);

    PLC.Run = true;

    startThread(PLC.Program, program, NULL);
    printOut("OK.\n");

    isRunning = true;
    
    return 0;
  } else {
    return -1;
  }
}

int stopPLC() {
  if (isRunning == true) {
    lockMutex(PLC.RunMutex);
    PLC.Run = false;
    unlockMutex(PLC.RunMutex);

    // Deinitialize the PLC.
    deinitializePLC();

    isRunning = false;

    return 0;
  } else {
    return -1;
  }
}