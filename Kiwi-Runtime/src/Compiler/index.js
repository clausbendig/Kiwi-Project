const jetpack = require('fs-jetpack');
const execFile = require('child_process').execFile;
const _ = require('underscore');
const ejs = require('ejs');
const sha256File = require('sha256-file');
const uuidBuffer = require('uuid-buffer');
const paths = require('../Paths');

class Compiler {
  constructor() {
  }

  get GCC() {
    // Find all *.c files in the compilation folder.
    let files = jetpack.find(paths.Compilation.Target, {
      matching: '*.c',
      recursive: false,
      files: true,
      directories: false
    });

    // Filter out the POUS.c file.
    let filteredFiles = _.filter(files, (file) => {
      return !file.endsWith('POUS.c')
    });

    // Convert app files from relative path to absolute path.
    for (let i = 0; i < filteredFiles.length; i++) {
      filteredFiles[i] = jetpack.path(filteredFiles[i]);
    }

    filteredFiles.push(paths.Compilation.GCC.Application);
    filteredFiles.push(paths.Compilation.GCC.PLC);
    filteredFiles.push(paths.Compilation.GCC.Interfaces.Kiwi);

    // Return the path of gcc and the arguments to compile the application.
    return {
      Command: 'gcc',
      Arguments: [
        '-Wall',
        '-Wno-unused-function',
        '--shared',
        '-fPIC',
        `-I${paths.Compilation.Target}`,
        `-I${paths.Compilation.GCC.Includes}`,
        `-I${paths.Compilation.GCC.MatIEC}`,
        `-lrt`,
        '-pthread',
        '-D_GNU_SOURCE',
        ...filteredFiles,
        '-o',
        paths.Compilation.Temporary.Application,
      ],
      Files: filteredFiles
    }
  }

  get IEC2C() {
    // Return the path of iec2c and the arguments to convert the ST-code to C-code.
    return {
      Command: 'iec2c',
      Arguments: [
        '-n',
        '-I',
        paths.Compilation.IEC2C.Includes,
        '-T',
        paths.Compilation.Target,
        paths.Compilation.IEC2C.Application,
      ]
    };
  }

  get hasCompilation() {
    // Check if the Application.so, Application.kiwi and Application.sha256 exist in the application folder.
    if (jetpack.exists(paths.Compilation.Temporary.Application) && jetpack.exists(paths.Compilation.Temporary.Project) && jetpack.exists(paths.Compilation.Temporary.SHA256)) {
      try {
        // Read the Application.sha256 file.
        let sha256 = JSON.parse(jetpack.read(paths.Compilation.Temporary.SHA256));

        // Create the sha256 checksum of the Application.so.
        let sha256Application = sha256File(paths.Compilation.Temporary.Application);
        let sha256Project = sha256File(paths.Compilation.Temporary.Project);

        // Validate both checksums.
        if ((sha256.Application === sha256Application) && (sha256.Project === sha256Project)){
          return true;
        } else {
          return false;
        }
      } catch(err) {
        return false;
      }
    } else {
      return false;
    }
  }

  clean() {
    let p = this.Paths;
    return new Promise((resolve, reject) => {
      try {
        // Remove compiliation folder.
        jetpack.remove(paths.Compilation.Target);

        // Create compilation folder.
        jetpack.dir(paths.Compilation.Target);

        // Resolve, if no error occured.
        resolve();
      } catch(err) {
        // Reject, if error occured.
        reject();
      }
    });
  }

  prepare(project) {
    let vars = [];
    project.Interfaces.forEach((iface) => {
      iface.LocatedVariables = [];
      if (iface.Bus === 'Kiwi') {
        
        iface.Modules.forEach((m) => {
          m.Variables.forEach((v) => {
            let prefix = this.getDataTypePrefix(v.DataType);
            let size = this.getDataTypeSize(v.DataType);
            let address = iface.Address.toString() + '.' + m.Address.toString() + '.' + v.Address.toString();
            let addr = address.split('.');
            let addrUnderscore = addr.join('_');
            let addrComma = addr.join(',');
            let isBit = (addr.length === 4) ? true : false;
            let lastBit = 0;
            let lastByte = 0;

            if (isBit === true) {
              lastBit = addr[addr.length - 1];
              lastByte = addr[addr.length - 2]; 
            } else {
              lastByte = addr[addr.length - 1];
            }
            
            v.Prepared = {
              Address: {
                Array:      addr,
                Comma:      addrComma,
                Underscore: addrUnderscore,
                isBit:      isBit,
                LastBit: lastBit,
                LastByte: lastByte,
              },
              Prefix: prefix,
              Size: size,
            };
            vars.push(`__LOCATED_VAR(${v.DataType},__${v.MemoryType}${prefix}${addrUnderscore},${v.MemoryType},${prefix},${addrComma})`);
          });
          let arr = new Array();
          let buffer = uuidBuffer.toBuffer(m.UUID);
          m.UUID_Buffer = Array.prototype.slice.call(buffer, 0);
        });
      }
    });
    project.LocatedVariables = vars.join('\n');
  }

  compile(project) {
    let that = this;

    // Create project clone.
    let projectClone = JSON.parse(JSON.stringify(project));

    // Prepare project for compiling.
    this.prepare(project);

    let output = {
      Files: {
        Transpiled: [],
        Compiled: [],
      },
      Commands: {
        Transpiled: [],
        Compiled: [],
      },
      STDOUT: {
        Transpiled: [],
        Compiled: [],
      },
      STDERR: {
        Transpiled: [],
        Compiled: [],
      },
      Checksums: {
        Application: '',
        Project: '',
      },
      Transfered: [],
    };

    return new Promise((resolve, reject) => {
      // Clean the compilation folder.
      jetpack.remove(paths.Compilation.Target);
      jetpack.dir(paths.Compilation.Target);

      // Write the project file to the compilation folder.
      jetpack.write(paths.Compilation.Temporary.Project, JSON.stringify(projectClone));

      // Write the ST file to the compilation folder
      jetpack.write(paths.Compilation.IEC2C.Application, project.SourceCode);

      // Render IO.ejs and write to the target folder.
      let ioTemplate = jetpack.read(paths.Compilation.EJS.IO);
      let ioRendered = ejs.render(ioTemplate, { Project: project }, {});

      jetpack.write(paths.Compilation.GCC.IO, ioRendered);

      // Get iec2c command path and arguments.
      let iec2c = that.IEC2C;

      // Execute iec2c.
      execFile(iec2c.Command, iec2c.Arguments, function(err, iec2cStdout, iec2cSdterr) {
        // Set console output informations.
        output.Files.Transpiled = [paths.Compilation.IEC2C.Application];
        output.Commands.Transpiled = [`iec2c ${iec2c.Arguments.join(' ')}`];
        output.STDERR.Transpiled = iec2cStdout.split('\n');
        output.STDOUT.Transpiled = iec2cSdterr.split('\n');
        if (err) {
          reject(output);
        } else {
          // Get gcc command path and arguments.
          let gcc = that.GCC;

          // Prepare LOCATED_VARS.h.
          let locatedVars = jetpack.read(paths.Compilation.GCC.LocatedVariables);
          locatedVars += '\n' + project.LocatedVariables;
          jetpack.write(paths.Compilation.GCC.LocatedVariables, _.uniq(locatedVars.split('\n')).join('\n'));

          // Execute gcc.
          execFile(gcc.Command, gcc.Arguments, function(err, gccStdout, gccSdterr) {
            // Set console output informations.
            output.Files.Compiled = gcc.Files;
            output.Commands.Compiled = [`gcc ${gcc.Arguments.join(' ')}`];
            output.STDERR.Compiled = gccStdout.split('\n');
            output.STDOUT.Compiled = gccSdterr.split('\n');
            if (err) {
              reject(output);
            } else {
              let out = `Transpiled:\n\nApplication.st\n\nCompiled:\n\n${gcc.Files.join('\n')}\n\nIEC2C-Output:\n\n${iec2cStdout}\n\nGCC-Output:\n\n + ${gccStdout}`;
              let shasumApplication = sha256File(paths.Compilation.Temporary.Application);
              let shasumProject = sha256File(paths.Compilation.Temporary.Project);
              output.Checksums = {
                Application: shasumApplication,
                Project: shasumProject,
              };
              jetpack.write(paths.Compilation.Temporary.SHA256, JSON.stringify(output.Checksums));
              resolve(output);
            }
          });        
        }
      });      
    });
  }

  upload() {
    let that = this;

    return new Promise((resolve, reject) => {
      try {
        // Remove the Application.so, Application.kiwi and Application.sha256 in the application folder.
        that.erase();
  
        // Copy the the Application.so in the application folder and write the Application.kiwi and Application.sha256.
        jetpack.copy(paths.Compilation.Temporary.Application, paths.App.Application);
        jetpack.copy(paths.Compilation.Temporary.Project, paths.App.Project);
        jetpack.copy(paths.Compilation.Temporary.SHA256, paths.App.SHA256);
  
        let output = {
          Transfered: true,
          TransferedFiles: [paths.App.Application, paths.App.Project, paths.App.SHA256],
        };

        // Resolve, if no error occured.
        resolve(output);
      } catch(err) {
        let output = {
          Transfered: false,
          TransferedFiles: [],
        };

        // On error return reject.
        reject(output);
      }
    });
  }

  download() {
    return new Promise((resolve, reject) => {
      let project = this.Project;
      if (project == null) {
        reject();
      } else {
        resolve(project);
      }
    });
  }

  erase() {
    return new Promise((resolve, reject) => {
      // If Application.so, Application.kiwi and Application exists in the application folder,
      // then it will be removed.
      try {
        if (jetpack.exists(paths.App.Application)) {
          jetpack.remove(paths.App.Application);
        }     
        if (jetpack.exists(paths.App.Project)) {
          jetpack.remove(paths.App.Project);
        }
        if (jetpack.exists(paths.App.SHA256)) {
          jetpack.remove(paths.App.SHA256);
        }
        resolve();
      } catch(err) {
        reject();
      }
    });
  }

  getDataTypePrefix(dt) {
    switch(dt) {
      case 'BOOL':
        return 'X';
        break;
      case 'SINT':
        return 'B';
        break;
      case 'INT':
        return 'W';
        break;
      case 'DINT':
        return 'D';
        break;
      case 'LINT':
        return 'L';
        break;
      case 'USINT':
        return 'B';
        break;
      case 'UINT':
        return 'W';
        break;
      case 'UDINT':
        return 'D';
        break;
      case 'ULINT':
        return 'L';
        break;
      case 'BYTE':
        return 'B';
        break;
      case 'WORD':
        return 'W';
        break;
      case 'DWORD':
        return 'D';
        break;
      case 'LWORD':
        return 'L';
        break;
      case 'REAL':
        return 'D';
        break;
      case 'LREAL':
        return 'L';
        break;
      case 'DATE':
        return 'L';
        break;
      case 'TOD':
        return 'L';
        break;
      case 'DT':
        return 'L';
        break;
      case 'TIME':
        return 'L';
        break;
    }
  }

  getDataTypeSize(dt) {
    switch(dt) {
      case 'BOOL':
        return 0;
        break;
      case 'SINT':
        return 1;
        break;
      case 'INT':
        return 2;
        break;
      case 'DINT':
        return 4;
        break;
      case 'LINT':
        return 8;
        break;
      case 'USINT':
        return 1;
        break;
      case 'UINT':
        return 2;
        break;
      case 'UDINT':
        return 4;
        break;
      case 'ULINT':
        return 8;
        break;
      case 'BYTE':
        return 1;
        break;
      case 'WORD':
        return 2;
        break;
      case 'DWORD':
        return 4;
        break;
      case 'LWORD':
        return 8;
        break;
      case 'REAL':
        return 8;
        break;
      case 'LREAL':
        return 8;
        break;
      case 'DATE':
        return 8;
        break;
      case 'TOD':
        return 8;
        break;
      case 'DT':
        return 8;
        break;
      case 'TIME':
        return 8;
        break;
    }
  }
};

module.exports = Compiler;