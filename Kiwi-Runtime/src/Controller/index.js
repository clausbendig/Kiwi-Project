const Compiler = require('../Compiler');
const FFI      = require('../FFI');
const HD       = require('../Hardware');
const Hardware = HD.Hardware;
const Color    = HD.Color;

class Controller {
  constructor(modules) {
    this.FFI = new FFI(this.error.bind(this));
    this.Compiler = new Compiler();
    this.Hardware = new Hardware(modules, {
      start: this.start.bind(this),
      stop:  this.stop.bind(this),
      erase: this.erase.bind(this),
    });

    this.Modules = modules;

    this.Socket = null;
    this.Connected = false;

    process.on('exit', () => this.disconnect());
    process.on('exit', () => this.stop());
  }

  get Status() {
    let isRunning = this.FFI.isRunning;
    let hasApplication = this.FFI.hasApplication;
    let hasCompilation = this.Compiler.hasCompilation;

    let canControl = this.Hardware.canControl && hasApplication;
    let canClean = hasCompilation;
    let canErase = false;

    if ((hasApplication == true) && (isRunning == false)) {
      canErase = true;
    }

    return {
      isRunning: isRunning,
      hasApplication: hasApplication,
      hasCompilation: hasCompilation,
      canControl: canControl,
      canClean: canClean,
      canErase: canErase,
    };
  }

  setSocket(socket, uuid) {
    if (uuid == this.Hardware.Information.UUID) {
      if (this.Connected) {
        console.log(`SRV: Client "${socket.request.connection._peername.address}" was blocked.`);
        socket.emit('blocked', { Blocked: false });
        socket.disconnect(true);
      } else {
        this.Socket = socket;
        this.Socket.on('close-connection', this.disconnect.bind(this)); 
        this.Socket.on('disconnect',       this.onDisconnect.bind(this));
        this.Socket.on('error',            this.disconnect.bind(this));
        this.Socket.on('start',            this.start.bind(this));
        this.Socket.on('stop',             this.stop.bind(this));
        this.Socket.on('compile',          this.compile.bind(this));
        this.Socket.on('clean',            this.clean.bind(this));
        this.Socket.on('upload',           this.upload.bind(this));
        this.Socket.on('download',         this.download.bind(this));
        this.Socket.on('erase',            this.erase.bind(this));
        this.Socket.on('scan',             this.scan.bind(this));
        this.Socket.on('set-ethernet',     this.setEthernet.bind(this));
        this.Socket.on('set-wifi',         this.setWiFi.bind(this));
        this.Socket.on('set-usv',          this.setUSV.bind(this));
        this.Socket.emit('status',         this.Status);
        this.Connected = true;
        console.log(`SRV: Client "${this.Socket.request.connection._peername.address}" connected.`);
      }
    } else {
      socket.disconnect(true);
    }
  }

  emitDisconnect() {
    if (this.Socket != null) {
      this.Socket.emit('close-connection',  {});
    }
  }

  disconnect() {
    if (this.Socket != null) {
      this.Socket.disconnect(true);
    }
  }

  onDisconnect(reason) {
    if (this.Socket != null) {
      console.log(`SRV: Client "${this.Socket.request.connection._peername.address}" disconnect.`);
      this.Socket = null;
    }
    this.Connected = false;
  }

  information() {
    return this.Hardware.Information;
  }

  lookFor(msg) {
    this.Hardware.lookFor(msg).then((ret) => {
      that.Socket.emit('status', that.Status);
      that.Socket.emit('lookFor', ret);
    }).catch((ret) => {
      that.Socket.emit('status', that.Status);
      that.Socket.emit('lookFor', ret);
    });
  }

  start() {
    process.stdout.write("SRV: Start PLC.\n");
    let that = this;
    this.FFI.start().then(() => {
      that.Hardware.setColor(Color.Green);
      if (that.Socket != null) {
        that.Socket.emit('status', that.Status);
        that.Socket.emit('started', { Started: true, MSGType: 2 });
      }
    }).catch(() => {
      process.stdout.write("SRV: Couldn't start PLC.\n");
      that.Hardware.setColor(Color.Red);
      if (that.Socket != null) {
        that.Socket.emit('status', that.Status);
        that.Socket.emit('started', { Started: false, MSGType: 1 });
      }
    });
  }

  stop() {
    process.stdout.write("SRV: Stop PLC.\n");
    let that = this;
    this.FFI.stop().then(() => {
      that.Hardware.setColor(Color.White);
      process.stdout.write("SRV: PLC stopped.\n");
      if (that.Socket != null) {
        that.Socket.emit('status', that.Status);
        that.Socket.emit('stopped', { Stopped: true, MSGType: 2 });
      }
    }).catch(() => {
      that.Hardware.setColor(Color.White);
      process.stdout.write("SRV: Couldn't stop PLC.\n");
      if (that.Socket != null) {
        that.Socket.emit('status', that.Status);
        that.Socket.emit('stopped', { Stopped: false, MSGType: 1 });
      }
    });
  }

  error() {
    this.Hardware.setColor(Color.Red);
    if (this.Socket != null) {
      this.Socket.emit('status', this.Status);
      this.Socket.emit('started', { Started: false, MSGType: 1 });
    }
  }

  compile(msg) {
    let that = this;
    process.stdout.write("SRV: Compiling PLC application ... ");
    this.Compiler.compile(msg.Project).then((cons) => {
      process.stdout.write("OK.\n");
      if (that.Socket != null) {
        that.Socket.emit('status', that.Status);
        that.Socket.emit('compiled', { Compiled: true, MSGType: 2, Console: cons });
      }
    }).catch((cons) => {
      process.stdout.write("FAILED.\n");
      if (that.Socket != null) {
        that.Socket.emit('status', that.Status);
        that.Socket.emit('compiled', { Compiled: false, MSGType: 1, Console: cons });
      }
    });
  }

  clean() {
    let that = this;
    process.stdout.write("SRV: Cleaning PLC application ... ");
    this.Compiler.clean().then(() => {
      process.stdout.write("OK.\n");
      if (that.Socket != null) {
        that.Socket.emit('status', that.Status);
        that.Socket.emit('cleaned', { Cleaned: true, MSGType: 2 });
      }
    }).catch(() => {
      process.stdout.write("FAILED.\n");
      if (that.Socket != null) {
        that.Socket.emit('status', that.Status);
        that.Socket.emit('cleaned', { Cleaned: false, MSGType: 1 });
      }
    });
  }

  upload() {
    let that = this;

    if (this.FFI.isRunning == true) {
      if (this.Socket != null) {
        this.Socket.emit('uploaded', {
          Uploaded: false,
          MSGType: 3,
          Console: ''
        });
      }
      return;
    }

    process.stdout.write("SRV: Uploading application to PLC ... ");

    this.Compiler.upload().then((cons) => {
      process.stdout.write("OK.\n");
      if (that.Socket != null) {
        that.Socket.emit('status', that.Status);
        that.Socket.emit('uploaded', { Uploaded: true, MSGType: 2, Console: cons });
      }
    }).catch((cons) => {
      process.stdout.write("FAILED.\n");
      if (that.Socket != null) {
        that.Socket.emit('status', that.Status);
        that.Socket.emit('uploaded', { Uploaded: false, MSGType: 1, Console: cons });
      }
    });
  }

  download() {
    let that = this;
    process.stdout.write("SRV: Downloading application from PLC ... ");
    this.Compiler.download().then((project) => {
      process.stdout.write("OK.\n");
      if (that.Socket != null) {
        that.Hardware.setColor(Color.White);
        that.Socket.emit('status', that.Status);
        that.Socket.emit('downloaded', { Downloaded: true, MSGType: 2, Project: project });
      }
    }).catch(() => {
      process.stdout.write("FAILED.\n");
      if (that.Socket != null) {
        that.Hardware.setColor(Color.White);
        that.Socket.emit('status', that.Status);
        that.Socket.emit('downloaded', { Downloaded: false, MSGType: 1, Project: null });
      }
    });
  }

  erase() {
    let that = this;

    if (this.FFI.isRunning == true) {
      if (this.Socket != null) {
        this.Socket.emit('erased', {
          Erased: false,
          MSGType: 3,
          Console: ''
        });
      }
      return;
    }

    process.stdout.write("SRV: Erasing application from PLC ... ");

    this.Compiler.erase().then(() => {
      process.stdout.write("OK.\n");
      if (that.Socket != null) {
        that.Socket.emit('status', that.Status);
        that.Socket.emit('erased', { Erased: true, MSGType: 2 });
      }
    }).catch(() => {
      process.stdout.write("FAILED.\n");
      if (that.Socket != null) {
        that.Socket.emit('status', that.Status);
        that.Socket.emit('erased', { Erased: false, MSGType: 1 });
      }
    });
  }

  scan(msg) {
    if (msg.Interface === 'CAN' && msg.Physical === 'can0' && msg.Name === 'CAN' && msg.Bus === 'Kiwi') {
      if (this.Socket != null) {
        this.Socket.emit('status', this.Status);
        this.Socket.emit('scanned', { Scanned: true, Modules: this.Hardware.Modules, MSGType: 2 });
      }
    } else {
      if (this.Socket != null) {
        this.Socket.emit('status', this.Status);
        this.Socket.emit('scanned', { Scanned: false, Modules: null, MSGType: 1 });
      }
    }
  }

  setEthernet(msg) {
    let that = this;
    this.Hardware.setEthernet(msg.DHCP, msg.IP, msg.Gateway).then(() => {
      if (that.Socket != null) {
        that.Socket.emit('status', that.Status);
        that.Socket.emit('ethernet-set', { Ethernet: true, MSGType: 2 });
      }
    }).catch(() => {
      process.stdout.write("SRV: Couldn't set Ethernet preferences.\n");
      if (that.Socket != null) {
        that.Socket.emit('status', that.Status);
        that.Socket.emit('ethernet-set', { Ethernet: false, MSGType: 1 });
      }
    });
  }

  setWiFi(msg) {
    let that = this;
    this.Hardware.setWiFi(msg.ActingAs, msg.SSID, msg.Password).then((actingAs) => {
      if (that.Socket != null) {
        that.Socket.emit('status', that.Status);
        that.Socket.emit('wifi-set', { Wifi: true, ActingAs: actingAs, MSGType: 2 });
      }
    }).catch(() => {
      process.stdout.write("SRV: Couldn't set WiFi preferences.\n");
      if (that.Socket != null) {
        that.Socket.emit('status', that.Status);
        that.Socket.emit('wifi-set', { Wifi: false, MSGType: 1 });
      }
    });
  }

  setUSV(msg) {
    let that = this;
    this.Hardware.setUSV(msg.Time).then(() => {
      if (that.Socket != null) {
        that.Socket.emit('status', that.Status);
        that.Socket.emit('usv-set', { USV: true, MSGType: 2 });
      }
    }).catch(() => {
      process.stdout.write("SRV: Couldn't set USV time.\n");
      if (that.Socket != null) {
        that.Socket.emit('status', that.Status);
        that.Socket.emit('usv-set', { USV: false, MSGType: 1 });
      }
    });
  }
};

module.exports = Controller;
