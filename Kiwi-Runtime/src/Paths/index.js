const path = require('path');

const appFolder = '/kiwi';
const tmpFolder = '/tmp';

const Paths = {
  Compilation: {
    Target: `/tmp/kiwi-compilation`,
    EJS: {
      IO: `${path.dirname(__dirname)}/Runtime-Code/Templates/IO.ejs`,
    },
    GCC: {
      Includes:         `${path.dirname(__dirname)}/Runtime-Code/SourceCode`,
      MatIEC:           `${path.dirname(__dirname)}/Runtime-Code/SourceCode/MatIEC/C`,
      Application:      `${path.dirname(__dirname)}/Runtime-Code/SourceCode/Application.c`,
      PLC:              `${path.dirname(__dirname)}/Runtime-Code/SourceCode/PLC.c`,
      IO:               `/tmp/kiwi-compilation/IO.c`,
      LocatedVariables: `/tmp/kiwi-compilation/LOCATED_VARIABLES.h`,
      Interfaces: {
        Kiwi: `${path.dirname(__dirname)}/Runtime-Code/SourceCode/Interfaces/Kiwi/Kiwi.c`,
      }
    },
    IEC2C: {
      Includes:    `${path.dirname(__dirname)}/Runtime-Code/SourceCode/MatIEC`,
      Application: `/tmp/kiwi-compilation/Application.st`,
    },
    Temporary: {
      Application: `/tmp/kiwi-compilation/Application.so`,
      Project:     `/tmp/kiwi-compilation/Application.kiwi`,
      SHA256:      `/tmp/kiwi-compilation/Application.sha256`,
    }
  },
  App: {
    Folder: appFolder,
    Application: `${appFolder}/Application.so`,
    Project:     `${appFolder}/Application.kiwi`,
    SHA256:      `${appFolder}/Application.sha256`,
  },
  Temp: {
    Folder:       tmpFolder,
    Descriptions: `${tmpFolder}/Descriptions.json`,
    SHA256:       `${tmpFolder}/Descriptions.sha256`,
  },
  Library: {
    Folder: appFolder,
    Descriptions: `${appFolder}/Descriptions.json`,
    SHA256:       `${appFolder}/Descriptions.sha256`,
  },
  Modules: {
    Temporary: {
      Folder:       tmpFolder,
      Descriptions: `${tmpFolder}/Descriptions.json`,
      SHA256:       `${tmpFolder}/Descriptions.sha256`,
    },
    Library: {
      Folder: appFolder,
      Descriptions: `${appFolder}/Descriptions.json`,
      SHA256:       `${appFolder}/Descriptions.sha256`,
    }
  },
  Webserver: {
    Configuration: `${appFolder}/Webserver.json`,
  },
  USV: {
    Configuration: `${appFolder}/USV.json`,
  },
  WiFi: {
    Configuration: `${appFolder}/WiFi.json`,
  }
};

module.exports = Paths;