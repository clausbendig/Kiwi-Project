const socketcan = require('kiwi-socketcan');
const jetpack = require('fs-jetpack');
const { StringDecoder } = require('string_decoder');
const uuidv4 = require('uuid/v4');
const sha256 = require('sha256');
const sha256File = require('sha256-file');
const _ = require('underscore');
const wpi = require('node-wiring-pi');

let Commands = {
  Error            :  0,
  Stop             :  1,
  Heartbeat        :  2,
  Start            :  3,
  Output           :  4,
  Input            :  5,
  HeartbeatTime    :  6,
  UUID             :  7,
  Description      :  8,
  DescriptionSHA256:  9,
  Search           : 10,
  Reserved_1       : 11,
  Reserved_2       : 12,
  Reserved_3       : 13,
  Reserved_4       : 13,
  Echo             : 15,
};

// 29-bit extraction.
let extractMeta = (val) => ((val >> 0x12) & 0x7FF);
let extractData = (val) => (val & 0x3FFFF);

// 29-bit setting.
let setMeta = (val, meta) => (val |= (meta << 0x12));
let setData = (val, data) => (val |= data);

// 11-bit extraction.
let extractCommand  = (val) => ((val >> 0x07) & 0x0F);
let extractAddress  = (val) => (val & 0x1F);

// 11-bit setting.
let setCommand  = (val, cmd)  => (val |= (cmd  << 0x07));
let setAddress  = (val, addr) => (val |= addr);

class UglyMutex {
  constructor(interval = 100, timeOut = 1000) {
    this.Interval = interval;
    this.TimeOut = timeOut;
    this.Lock = false;
    this.IntervalID = null;
    this.TimeOutID = null;
  }

  lock() {
    let that = this;
    return new Promise((resolve, reject) => {
      // Lock.
      that.Lock = true;

      // Run interval to check the lock-status.
      that.IntervalID = setInterval(() => {
        if (that.Lock == false) {
          clearInterval(that.IntervalID);
          clearTimeout(that.TimeOutID);
          resolve();
        }
      }, that.Interval);

      // Run timout, if the unlocking process is to slow.
      that.TimeOutID = setTimeout(() => {
        this.Lock = false;
        clearInterval(that.IntervalID);
        reject();
      }, that.TimeOut);
    });
    
  }

  unlock() {
    this.Lock = false;
  }
};

module.exports = (paths) => {
  class Modules {
    constructor() {
      this.Mutex = new UglyMutex();
      this.Found = new Object();
      this.toRead = new Object();
      this.Modules = [];
      this.Descriptions = [];

      // Use GPIO pin schema.
      wpi.wiringPiSetupGpio();

      // Setup ID pin.
      wpi.pinMode(37,  wpi.OUTPUT);
    }

    onMessage(msg) {
      if (msg.ext == true) {
        var meta = extractMeta(msg.id);
        var data = extractData(msg.id);
        var cmd  = extractCommand(meta);
        var addr = extractAddress(meta);
      } else {
        var cmd  = extractCommand(msg.id);
        var addr = extractAddress(msg.id);
        var data = 0;
      }

      if (msg.data) {
        var buffer = Array.prototype.slice.call(msg.data, 0);
      } else {
        var buffer = [];
      }
      
      switch(cmd) {
        case Commands.Search:
          this.Found[addr] = {
            Address: addr,
            UUID: Array(1),
            DescriptionSHA256: Array(1),
            Description: Array(32),
          };
          break;
        
        case Commands.UUID:
          this.Found[addr].UUID[data] = msg.data;
          break;

        case Commands.DescriptionSHA256:
          this.toRead[addr].DescriptionSHA256[data] = msg.data;
          break;

        case Commands.Description:
          this.toRead[addr].Description[data] = msg.data;
          break;

        default:
          break;
      }
      
      this.Mutex.unlock();
    }

    transmitMessage(cmd, addr, ext, data) {
      let id = 0;

      if (ext == true) {
        let meta;
        meta = setCommand(meta, cmd);
        meta = setAddress(meta, addr);
        id = setMeta(id, meta);
        id = setData(id, data);
      } else {
        id = setCommand(id, cmd);
        id = setAddress(id, addr);
      }
    
      let ret = this.CAN.send({
        id: id,
        rtr: true,
        ext: ext,
        data: Buffer.from([]),
      });
    }

    async lookFor() {
      // Set ID pin to LOW for 1ms.
      wpi.digitalWrite(37, 0);
      await new Promise(r => setTimeout(r, 1));

     // Set ID to HIGH and wait 1s for the completion of the addressing.
     wpi.digitalWrite(37, 1);
      await new Promise(r => setTimeout(r, 2000));

      // Get the current time.
      let old = Date.now();

      try {
        try {
          // Load descriptions.
          if (jetpack.exists(paths.Modules.Library.Descriptions) && jetpack.exists(paths.Modules.Library.SHA256)) {
            // Read the sha256 checksum from Description.sha256.
            let sha256Descriptions = JSON.parse(jetpack.read(paths.Modules.Library.SHA256));

            // Verify checksums.
            if (sha256Descriptions.Descriptions == sha256File(paths.Modules.Library.Descriptions)) {
              this.Descriptions = JSON.parse(jetpack.read(paths.Modules.Library.Descriptions));
            }
          }
        } catch(e) {
          jetpack.remove(paths.Modules.Library.Descriptions);
          jetpack.remove(paths.Modules.Library.SHA256);
          this.Descriptions = [];
        }

        // Create CAN-socket.
        this.CAN = socketcan.createRawChannel("can0", true);
        this.CAN.start();
        this.CAN.addListener("onMessage", this.onMessage.bind(this));

        // Run initializing steps.
        await this.search();
        await this.uuid();

        // Iterate through all modules, and fill the to read descriptions and not to read descriptions.
        for (let key in this.Found) {
          let m = this.Found[key];

          // Convert UUID buffer to UUID string.
          let uuid = uuidv4({
            random: Array.prototype.slice.call(Buffer.concat([m.UUID[0]]), 0),
          });

          let i = _.findIndex(this.Descriptions, (description) => {
            if (description.UUID == uuid) {
              return true;
            } else {
              return false;
            }
          });

          if (i == -1) {
            this.toRead[m.Address] = m;
          } else {
            this.Modules.push({
              Address: m.Address,
              Description: this.Descriptions[i],
            });
          }
        }

        await this.sha256();
        await this.description();
        await this.merge();

        // Sort modules.
        this.Modules = _.sortBy(this.Modules, 'Address');

        // Save description library to /tmp.
        jetpack.write(paths.Modules.Temporary.Descriptions, JSON.stringify(this.Descriptions));
        jetpack.write(paths.Modules.Temporary.SHA256, JSON.stringify({
          Descriptions: sha256File(paths.Modules.Temporary.Descriptions),
        }));

        // Remove existing description library.
        if (jetpack.exists(paths.Modules.Library.Descriptions)) jetpack.remove(paths.Modules.Library.Descriptions);
        if (jetpack.exists(paths.Modules.Library.SHA256)) jetpack.remove(paths.Modules.Library.SHA256);

        // Copy temporary description library to /kiwi.
        jetpack.copy(paths.Modules.Temporary.Descriptions, paths.Modules.Library.Descriptions);
        jetpack.copy(paths.Modules.Temporary.SHA256, paths.Modules.Library.SHA256);

        this.CAN.stop();

        console.log(`SRV: Module search process took ${Date.now() - old} ms.`);      

        return {
          Modules: this.Modules,
          Descriptions: this.Descriptions,
        };
      } catch(e) {
        //console.log(e);
        throw Error(e);
      }
    }

    async search() {
      this.transmitMessage(Commands.Search, 0, false, 0);
      await new Promise(r => setTimeout(r, 1000));
    }

    async uuid() {
      try {
        for (let key in this.Found) {
          let m = this.Found[key];
          // for (let i = 0; i < 2; i++) {
            this.transmitMessage(Commands.UUID, m.Address, false, 0);
            await this.Mutex.lock();
          // }
        }
      } catch(e) {
        console.log(e);
        throw Error('Couldn\'t search modules.');
      }
    }

    async sha256() {
      try {
        for (let key in this.toRead) {
          let m = this.toRead[key];
          // for (let i = 0; i < 4; i++) {
            this.transmitMessage(Commands.DescriptionSHA256, m.Address, false, 0);
            await this.Mutex.lock();
          // }
        }
      } catch(e) {
        console.log(e);
        throw Error('Couldn\'t receive SHA256.');
      }
    }

    async description() {
      try {
        for (let key in this.toRead) {
          let m = this.toRead[key];
          for (let i = 0; i < 32; i++) {
            this.transmitMessage(Commands.Description, m.Address, true, i);
            await this.Mutex.lock();
          }
        }
      } catch(e) {
        console.log(e);
        throw Error('Couldn\'t receive description.');
      }
    }

    async merge() {
      try {
        for (let key in this.toRead) {

          let m = this.toRead[key];

          // Concat SHA256 buffers.
          let sha256Buffer = Buffer.concat([m.DescriptionSHA256[0]]);
          let sha256String = sha256Buffer.toString('hex');
          

          // Concat description buffer.
          let descriptionString = '';
          let descriptionDecoder = new StringDecoder('ascii');
          for (let i = 0; i < 32; i++) descriptionString += descriptionDecoder.write(m.Description[i]);

          let n = descriptionString.indexOf('\u0000');
          if (n == -1) {
            var description = descriptionString.slice(0, 2048);
          } else {
            var description = descriptionString.slice(0, n);
          }
      
          if (sha256(description) === sha256String) {
            let parsedDescription = JSON.parse(description);
            this.Modules.push({
              Address: m.Address,
              Description: parsedDescription,
            });
            this.Descriptions.push(parsedDescription);
          }
        }
      } catch(e) {
        console.log(e);
        console.log("Couldn't merge descriptions.")
      }
    }
  };

  return new Modules();
};
