const wpi = require('node-wiring-pi');
const machineId = require('machine-id');
const execFile = require('child_process').execFile;
const jetpack = require('fs-jetpack');
const paths = require('../Paths');
var wifi = require("node-wifi");

let Pins = {
  Start: 17,
  Erase: 4,
  Red: 13,
  Blue: 16,
  Green: 5,
  RGBLED: 7,
  PowerInterrupt: 39,
  SwitchInterrupt: 38,
  ID: 37,
  OTG_ID: 41,
  OTG_EN: 42,
  OTG_OC: 43,
};

let Color = {
  White:   0,
  Red:     1,
  Green:   2,
  Blue:    3,
  Yellow:  4,
  Aqua:    5,
  Fuchsia: 6,
  Off:     7,
};

class Hardware {
  constructor(modules, events) {

    this.USV = { Time: 60000 };
    this.PoweringOff = false;

    if (jetpack.exists(paths.USV.Configuration)) {
      try {
        this.USV = JSON.parse(jetpack.read(paths.USV.Configuration));
      } catch(e) {
        this.USV = { Time: 60000 };
      }
    } else {
      jetpack.write(paths.USV.Configuration, JSON.stringify(this.USV));
    }

    wifi.init({
      iface: null
    });

    if (jetpack.exists(paths.WiFi.Configuration)) {
      try {
        let that = this;
        this.WiFi = JSON.parse(jetpack.read(paths.WiFi.Configuration));
        if (this.WiFi.ActAs == 'Wifi') {
          wifi.connect({ ssid: this.WiFi.SSID, password: this.WiFi.Password }, function(err) {
            if (err) {
              console.log(`SRV: Couldn't connect WiFi to "${that.WiFi.SSID}".`)
            } else {
              console.log(`SRV: WiFi connected to "${that.WiFi.SSID}".`)
            }
          });
        }
      } catch(e) {
        this.WiFi = { ActAs: 'None', SSID: '', Password: '' };
        jetpack.write(paths.WiFi.Configuration, JSON.stringify(this.WiFi));
      }
    } else {
      this.WiFi = { ActAs: 'None', SSID: '', Password: '' };
      jetpack.write(paths.WiFi.Configuration, JSON.stringify(this.WiFi));
    }

    // Setup pin modes.
    wpi.pinMode(Pins.Start,  wpi.INPUT);
    wpi.pinMode(Pins.Erase,  wpi.INPUT);
    wpi.pinMode(Pins.Red,    wpi.OUTPUT);
    wpi.pinMode(Pins.Green,  wpi.OUTPUT);
    wpi.pinMode(Pins.Blue,   wpi.OUTPUT);
    wpi.pinMode(Pins.RGBLED, wpi.OUTPUT);
    wpi.pinMode(Pins.PowerInterrupt,  wpi.INPUT);
    wpi.pinMode(Pins.SwitchInterrupt, wpi.INPUT);
    wpi.pinMode(Pins.ID,  wpi.OUTPUT);
    wpi.pinMode(Pins.OTG_ID, wpi.INPUT);
    wpi.pinMode(Pins.OTG_EN, wpi.OUTPUT);
    wpi.pinMode(Pins.OTG_ID, wpi.INPUT);

    wpi.pullUpDnControl(Pins.PowerInterrupt,  wpi.PUD_UP);
    wpi.pullUpDnControl(Pins.SwitchInterrupt, wpi.PUD_UP);
    wpi.pullUpDnControl(Pins.OTG_ID, wpi.PUD_UP);
    wpi.pullUpDnControl(Pins.OTG_OC, wpi.PUD_UP);

    // Button states.
    this.ButtonStates = {
      Old: {
        Start: 1,
        Erase: 1,
      },
      Current: {
        Start: 1,
        Erase: 1,
      }
    };

    // Events.
    this.Events = events;

    // Erase state.
    this.EraseState = false;

    // Using this variable for blinking.
    this.EraseBlink = false;

    // Set detected modules.
    this.Modules = modules;

    // Power off LED, when the process exits.
    process.on('exit', () => this.setColor(Color.Off));

    // Set RGB-LED to white.
    this.setColor(Color.White);

    // Setup loop interval to 1ms.
    setInterval(this.loop.bind(this), 1);

    // Run the loop once.
    this.loop();
  }

  get canControl() {
    if (this.ButtonStates.Old.Start == 0) {
      return true;
    } else {
      return false;
    }
  }

  get Information() {
    return {
      Name: 'Kiwi-PLC',
      Model: '4cf36f25-64d5-40ef-8014-5c9db4acea78',
      UUID: machineId(true),
      Modules: this.Modules,
      Interfaces: [
        {
          Name: 'CAN',
          Physical: 'can0',
          Interface: 'CAN',
          MultiUsage: false,
          Compatible: [
            {
              Name: 'Kiwi',
              canScan: true
            }
          ]
        },
        {
          Name: 'Ethernet',
          Physical: 'eth0',
          Interface: 'IP',
          MultiUsage: true,
          Compatible: [
            {
              Name: 'MQTT',
              canScan: false
            }
          ]
        }
      ]
    };
  }

  loop() {
    let that = this;

    // Read OTG-Pins
    if (this.PoweringOff == false) {
      let otgOC = wpi.digitalRead(Pins.OTG_ID);
      let otgID = wpi.digitalRead(Pins.OTG_OC);

      if ((otgID == 1) && (otgOC == 1)) {
        wpi.digitalWrite(Pins.OTG_EN, 1);
      } else {
        wpi.digitalWrite(Pins.OTG_EN, 0);
      }
    } else {
      wpi.digitalWrite(Pins.OTG_EN, 0);
    }
    

    // Read the pins of the USV.
    let powerInterrupt = wpi.digitalRead(Pins.PowerInterrupt);

    if ((powerInterrupt == 1) && (this.PoweringOff == false)) {
      this.PoweringOff = true;
      setTimeout(() => {
        let switchInterrupt = wpi.digitalRead(Pins.SwitchInterrupt);
        if (switchInterrupt == 0) {
          console.log(`SRV: Powering PLC down in ${this.USV.Time}ms.`);
          that.PoweringOffTimeout = setTimeout(() => {
            setTimeout(() => {
              execFile('/sbin/poweroff');  
            }, 10);
          }, this.USV.Time);
        } else {
          console.log(`SRV: Powering PLC down now.`);
          setTimeout(() => {
            execFile('/sbin/poweroff');  
          }, 10);

        }
      }, 5000);
    }

    if((powerInterrupt == 0) && (this.PoweringOff == true)) {
      this.PoweringOff = false;
      clearTimeout(this.PoweringOffTimeout);

      setTimeout(() => {
        // Set ID pin to HIGH for 1ms.
        wpi.digitalWrite(Pins.ID, 0);
        setTimeout(() => {
          // Set ID to low and wait 1s for the completion of the addressing.
          wpi.digitalWrite(Pins.ID, 1);
        }, 1);
      }, 5000);

      console.log("SRV: Back on net.");
    }


    // Read the current button states.
    this.ButtonStates.Current.Start = wpi.digitalRead(Pins.Start);
    this.ButtonStates.Current.Erase = wpi.digitalRead(Pins.Erase);

    // If start button state differs from the old one, then trigger start or stop event.
    if (this.ButtonStates.Current.Start != this.ButtonStates.Old.Start) {
      if (this.ButtonStates.Current.Start == 1) {
        if (this.Events != null) this.Events.stop();
      } else {
        this.EraseState = false;
        this.EraseTimeOutHandle = clearTimeout(this.EraseTimeOutHandle);
        this.stopEraseBlink();
        if (this.Events != null) this.Events.start();
      }
    }

    // If erase button states differs from the old one,
    if (this.ButtonStates.Current.Erase != this.ButtonStates.Old.Erase) {
      // then check if an erase was requested,
      if ((this.ButtonStates.Current.Erase == 0) && (this.EraseState == true)) {
        // if yes then trigger the erase event and stop blinking.
        if (this.Events != null) this.Events.erase();
        this.stopEraseBlink();
        this.setColor(Color.Red);
        this.EraseState = false;
        this.EraseTimeOutHandle = clearTimeout(this.EraseTimeOutHandle);
      } else {
        // If erasing wasn't requested, then start blinking and start blinking.
        if ((this.ButtonStates.Current.Erase == 0) && (this.EraseState == false)) {
          this.EraseState = true;
          this.startEraseBlink();
          this.EraseTimeOutHandle = setTimeout(() => {
            that.stopEraseBlink();
            that.setColor(Color.White);
            that.EraseState = false;
          }, 5000);
        }
      }
    }

    if ((this.ButtonStates.Current.Start == 1) &&
        (this.ButtonStates.Current.Erase == 1) &&
        (this.EraseState == false)) {
          this.setColor(Color.White);
    }

    // Switch button states.
    this.ButtonStates.Old.Start = this.ButtonStates.Current.Start;
    this.ButtonStates.Old.Erase = this.ButtonStates.Current.Erase;
  }

  setColor(c) {
    switch(c) {
      case Color.White:
        wpi.digitalWrite(Pins.Red,    0);
        wpi.digitalWrite(Pins.Green,  0);
        wpi.digitalWrite(Pins.Blue,   0);
        wpi.digitalWrite(Pins.RGBLED, 1);
        break;

      case Color.Red:
        wpi.digitalWrite(Pins.Red,    0);
        wpi.digitalWrite(Pins.Green,  1);
        wpi.digitalWrite(Pins.Blue,   1);
        wpi.digitalWrite(Pins.RGBLED, 1);
        break;

      case Color.Green:
        wpi.digitalWrite(Pins.Red,    1);
        wpi.digitalWrite(Pins.Green,  0);
        wpi.digitalWrite(Pins.Blue,   1);
        wpi.digitalWrite(Pins.RGBLED, 1);
        break;

      case Color.Blue:
        wpi.digitalWrite(Pins.Red,    1);
        wpi.digitalWrite(Pins.Green,  1);
        wpi.digitalWrite(Pins.Blue,   0);
        wpi.digitalWrite(Pins.RGBLED, 1);
        break;

      case Color.Yellow:
        wpi.digitalWrite(Pins.Red,    0);
        wpi.digitalWrite(Pins.Green,  0);
        wpi.digitalWrite(Pins.Blue,   1);
        wpi.digitalWrite(Pins.RGBLED, 1);
        break;

      case Color.Aqua:
        wpi.digitalWrite(Pins.Red,    1);
        wpi.digitalWrite(Pins.Green,  0);
        wpi.digitalWrite(Pins.Blue,   0);
        wpi.digitalWrite(Pins.RGBLED, 1);
        break;

      case Color.Fuchsia:
        wpi.digitalWrite(Pins.Red,    0);
        wpi.digitalWrite(Pins.Green,  1);
        wpi.digitalWrite(Pins.Blue,   0);
        wpi.digitalWrite(Pins.RGBLED, 1);
        break;

      case Color.Off:
        wpi.digitalWrite(Pins.Red,    0);
        wpi.digitalWrite(Pins.Green,  0);
        wpi.digitalWrite(Pins.Blue,   0);
        wpi.digitalWrite(Pins.RGBLED, 0);
        break;
    }
  }

  startEraseBlink() {
    this.EraseBlinkHandle = setInterval(this.eraseBlink.bind(this), 500);
  }

  stopEraseBlink() {
    this.EraseBlinkHandle = clearInterval(this.EraseBlinkHandle);
    this.EraseBlink = false;
    this.setColor(Color.Off);
  }

  eraseBlink() {
    if (this.EraseBlink) {
      this.setColor(Color.Red);
      this.EraseBlink = false;
    } else  {
      this.setColor(Color.Off);
      this.EraseBlink = true;
    }
  }

  setWiFi(actingAs, ssid, password) {
    let that = this;
    return new Promise((resolve, reject) => {
      if (ssid == that.SSID) {
        resolve(actingAs);
      }
      if (actingAs == 'None') {
        that.WiFi.ActAs = 'None';
        that.WiFi.SSID = '';
        that.WiFi.Password = '';
        if (jetpack.exists(paths.WiFi.Configuration)) {
          jetpack.remove(paths.WiFi.Configuration);
          jetpack.write(paths.WiFi.Configuration, JSON.stringify(that.WiFi));
        } else {
          jetpack.write(paths.USV.Configuration, JSON.stringify(that.WiFi));
        }
        wifi.disconnect(function(err) {
          if (err) {
            console.log(err);
          }
          
          console.log("SRV: WiFi disconnected.");
        });
        resolve(actingAs);       
      } else {
        console.log(`SRV: Connecting WiFi to "${ssid}".`);
        wifi.connect({ ssid: ssid, password: password }, function(err) {
          if (err) {
            reject();
          }

          that.WiFi.ActAs = 'Wifi';
          that.WiFi.SSID = ssid;
          that.WiFi.Password = password;

          if (jetpack.exists(paths.WiFi.Configuration)) {
            jetpack.remove(paths.WiFi.Configuration);
            jetpack.write(paths.WiFi.Configuration, JSON.stringify(that.WiFi));
          } else {
            jetpack.write(paths.USV.Configuration, JSON.stringify(that.WiFi));
          }

          console.log(`SRV: WiFi connected to "${ssid}".`);
  
          resolve(actingAs);
        });
      }    
    });
  }

  async setEthernet(dhcp, ip, gateway) {
    console.log(`DHCP: ${dhcp}, IP: ${ip}, Gateway: ${gateway}`);
    if (dhcp == true) {
      return true;
    } else {
      var ip_subnet_regex = /^\d{1,3}(\.\d{1,3}){3}\/\d{1,2}$/;
      var ip_regex = /^\d{1,3}(\.\d{1,3}){3}$/;

      if (ip_subnet_regex.test(ip) && ip_regex.test(gateway)) {

        
        return true;
      } else {
        throw("Ooops.")
      };
    }
  }

  async setUSV(t) {
    let time = parseInt(t);
    if (Number.isInteger(time)) {
      if ((time > 0) && (time <= 300)) {
        this.USV.Time = time * 60000;
        if (jetpack.exists(paths.USV.Configuration)) {
          jetpack.remove(paths.USV.Configuration);
          jetpack.write(paths.USV.Configuration, JSON.stringify(this.USV));
        } else {
          jetpack.write(paths.USV.Configuration, JSON.stringify(this.USV));
        }
      } else {
        throw("Ooops.")
      }
    } else {
      throw("Ooops.")
    }
  }
};

module.exports = {
  Hardware: Hardware,
  Color: Color,
};
