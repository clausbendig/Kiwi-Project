const jetpack = require('fs-jetpack');
const crypto = require('crypto');

module.exports = (port, modules, descriptions) => {
  // Includes.
  const sockpress = require('sockpress');
  const Controller = require('../Controller');
  const controller = new Controller(modules);

  class Webserver {
    constructor(port) {
      let appFolder = '/kiwi';
      let webserverConfigurationFile = '/kiwi/Webserver.json';
      let secretKey = '';

      if (jetpack.exists(appFolder) == false) {
        jetpack.dir(appFolder);
      }

      if (jetpack.exists(webserverConfigurationFile) == true) {
        let webserverConfiguration = JSON.parse(jetpack.read(webserverConfigurationFile));
        secretKey = webserverConfiguration.SecretKey;
      } else {
        secretKey = crypto.randomBytes(64).toString('hex');
        jetpack.write(webserverConfigurationFile, JSON.stringify({ SecretKey: secretKey }));
      }

      // Create Webserver instance.
      this.app = sockpress({
        secret: secretKey,
        saveUninitialized: false,
      });

      // Bind routes.
      this.Webserver();
      this.SocketIO();

      // Set port.
      this.Port = port;
    }

    Webserver() {
      this.app.get("/information", function(req, res) {
        res.setHeader('Content-Type', 'application/json');
        res.send(JSON.stringify(controller.information()));
      });

      this.app.get("/descriptions", function(req, res) {
        res.setHeader('Content-Type', 'application/json');
        res.send(JSON.stringify(descriptions));
      });
    }

    SocketIO() {
      this.app.io.of(/^\/[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}$/i).on('connect', (socket) => {
        const uuid = socket.nsp.name.slice(1);
        controller.setSocket(socket, uuid);
      });
    }

    listen() {
      this.app.listen(this.Port);
      console.log('SRV: Web-Server started.');


      // Add 'exit' to close the server.
      process.on('exit', this.onExit);
    }

    onExit() {
      console.log('SRV: Web-Server stopped.');
    }
  };
  
  return new Webserver(port);
};