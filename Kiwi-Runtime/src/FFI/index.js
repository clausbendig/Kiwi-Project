const paths = require('../Paths');
const ffi = require('kiwi-ffi');
const sha256File = require('sha256-file');
const jetpack = require('fs-jetpack');

class FFI {
  constructor(error) {
    this.Running = false;

    this.printOutCallback = ffi.Callback('void', ['string'], this.printOut.bind(this));
    this.printErrCallback = ffi.Callback('void', ['string'], this.printErr.bind(this));
    this.errorCallback    = ffi.Callback('void', [], this.onError.bind(this));

    this.error = error;
    this.ErrorOccurred = false;
  }

  get hasApplication() {
    // Check if the Application.so, Application.kiwi and Application.sha256 exist in the application folder.
    if (jetpack.exists(paths.App.Application) && jetpack.exists(paths.App.Project) && jetpack.exists(paths.App.SHA256)) {
      try {
        // Read the Application.sha256 file.
        let sha256 = JSON.parse(jetpack.read(paths.App.SHA256));

        // Create the sha256 checksum of the Application.so.
        let sha256Application = sha256File(paths.App.Application);
        let sha256Project = sha256File(paths.App.Project);

        // Validate both checksums.
        if ((sha256.Application === sha256Application) && (sha256.Project === sha256Project)){
          return true;
        } else {
          return false;
        }
      } catch(err) {
        return false;
      }
    } else {
      return false;
    }
  }

  get isRunning() {
    return this.Running;
  }

  printOut(text) {
    process.stdout.write(text);
  }

  printErr(text) {
    process.stderr.write(text);
  }

  onError(text) {
    this.ErrorOccurred = true;
    this.Running = false;
    this.error();
    console.log('SRV: Error occurred.');
  }

  start() {
    let that = this;

    return new Promise((resolve, reject) => {
      if (that.ErrorOccurred == true) {
        that.Application.DynamicLibrary.close();
        that.Application = null;
        that.ErrorOccurred = false;
      }

      if (that.hasApplication == true) {
        that.Application = ffi.Library(paths.App.Application, {
          'startPLC': [ 'int', ['pointer', 'pointer', 'pointer'] ],
          'stopPLC': [ 'int', [] ]
        });
        that.Application.startPLC.async(that.printOutCallback, that.printErrCallback, that.errorCallback, (err, res) => {
          if (err) {
            reject();
          } else {
            if (res < 0) {
              reject();
            } else {
              that.Running = true;
              resolve();
            }
          }
        });
      } else {
        reject();
      }
    });
  }

  stop() {
    let that = this;
    return new Promise((resolve, reject) => {
      if (that.Running == true) {
        that.Application.stopPLC.async((err, res) => {
          try {
            that.Application.DynamicLibrary.close();
            that.Application = null;
          } catch(e) {
            console.log("SRV: Oops, couldn't close the PLC application.")
          }
          if (err) {
            reject();
          } else {
            if (res < 0) {
              reject();
            } else {
              resolve();
            }
            that.Running = false;
          };
        });   
      } else {
        reject();
      }
    });  
  }
};

module.exports = FFI;
