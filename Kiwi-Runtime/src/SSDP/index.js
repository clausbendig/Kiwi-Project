module.exports = (port) => {

  // Includes.
  const ip = require('ip');
  const SSDPServer = require('node-ssdp').Server;  
  
  // SSDP class.
  class SSDP {
    constructor(port) {
      // Create a new instance of the SSDP-Server.
      this.SSDP = new SSDPServer({
        location: {
          protocol: 'http://',
          port: port,
          path: '/information',
        },
      });
  
      // Apply USN.
      this.SSDP.addUSN('urn:schemas-upnp-org:service:kiwi-gateway:1');
  
      // Add 'exit' to close the server.
      process.on('exit', this.onExit);
    };
  
    listen() {
      this.SSDP.start().then(() => {
        console.log('SRV: SSDP-Server started.');
      }).catch((err) => {
        console.log('SRV: SSDP-Server couldn\'t be started.');
        console.log(err);
        process.exit(1);
      });
    }
    onExit() {
      try {
        console.log("SRV: SSDP-Server stopped.");
        this.SSDP.stop();
      } catch(e) {}
    };
  };

  return new SSDP(port);
};

