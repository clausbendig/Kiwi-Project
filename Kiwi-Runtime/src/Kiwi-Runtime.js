#!/usr/bin/env node

// Arguments
const program = require('commander');
const paths   = require('./Paths');
 
program.version('1.0.0')
       .option('-p, --port <n>', 'Port of the Kiwi-Runtime', parseInt)
       .parse(process.argv);

// Standard port.
let port = 3000; 

if (program.port) {
  port = program.Port;
}

class KiwiRuntime {
  constructor() {

    this.Modules = require('./Kiwi-Bus')(paths);

    process.on('SIGTERM', this.onSIGTERM.bind(this));
    process.on('SIGUSR1', this.onSIGUSR1.bind(this));
    process.on('SIGUSR2', this.onSIGUSR2.bind(this));
    process.on('SIGINT',  this.onSIGINT.bind(this));
    process.on('SIGHUP',  this.onSIGHUP.bind(this));
  }

  onSIGKILL() {
    process.exit();
  }
  
  onSIGUSR1() {
    process.exit();
  }
   
  onSIGUSR2() { 
    process.exit();
  }
   
  onSIGTERM() {
    process.exit();
  }
   
  onSIGINT() {
    process.exit();
  }
   
  onSIGHUP() {
    process.exit();
  }

  listen() {
    let that = this;
    this.Modules.lookFor().then((obj) => {
      obj.Modules.forEach((module) => {
        console.log(`SRV: Found at 0x${module.Address.toString(16).padStart(2, '0')}: ['${module.Description.UUID}'] - '${module.Description.Name}'. `);
      });

      that.SSDP = require('./SSDP')(port);
      that.Webserver = require('./Webserver')(port, obj.Modules, obj.Descriptions);
      that.MQTT = require('./MQTT')(port);

      that.MQTT.listen();
      that.SSDP.listen();
      that.Webserver.listen();
    }).catch((e) => {
      console.log("SRV: Looking for modules failed.");
      that.SSDP = require('./SSDP')(port);
      that.Webserver = require('./Webserver')(port, [], []);
      that.MQTT = require('./MQTT')(port);

      that.MQTT.listen();
      that.SSDP.listen();
      that.Webserver.listen();
    });
    
  }
};

let Kiwi = new KiwiRuntime();
Kiwi.listen();