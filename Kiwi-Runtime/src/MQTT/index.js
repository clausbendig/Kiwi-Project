module.exports = (port) => {

  // Includes.
  const mosca = require('mosca');
  const redis = require('redis');
  
  // MQTT class.
  class MQTT {
    constructor(port) {
      this.MQTT = new mosca.Server({
        port: 1883,
        backend: {
          type: 'redis',
          redis: redis,
          db: 12,
          port: 6379,
          return_buffers: true,
          host: "localhost"
        },
        persistence: {
          factory: mosca.persistence.Redis
        }
      });

      this.MQTT.on('ready', () => {
        console.log("SRV: MQTT-Server started.");
      });

      // Add 'exit' to close the server.
      process.on('exit', this.onExit);
    };
  
    listen() {
    }

    onExit() {
      try {
        console.log("SRV: MQTT-Server stopped.");
      } catch(e) {}
    };
  };

  return new MQTT(port);
};

