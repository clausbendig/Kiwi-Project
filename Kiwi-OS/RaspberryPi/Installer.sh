#!/bin/sh
# Add NodeJS 8 repository to the apt-sources.
curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -

# Update repositories.
sudo apt-get update

# Upgrade all packages.
sudo apt-get -y upgrade

# Install developer tools and needed applications and libraries.
sudo apt-get -y install build-essential checkinstall autoconf automake gcc make libncursesw5-dev libreadline-dev git bison flex libffi-dev fakeroot jq nodejs redis-server device-tree-compiler mercurial debhelper libnanomsg-dev libzmq-dev libkrb5-dev can-utils i2c-tools dkms raspberrypi-kernel-headers libncurses5-dev bc haveged links wiringpi libssl-dev dnsmasq network-manager

# Remove packages
sudo apt-get -y purge openresolv dhcpcd5

# Autoremove
sudo apt-get -y autoremove

# Clean
sudo apt-get -y autoclean

# Use when NPM fails to compile node-wiring-pi, remove wiringpi from the above line.
wget https://unicorn.drogon.net/wiringpi-2.46-1.deb
sudo dpkg --install wiringpi-2.46-1.deb

# Install prebuilt and prebuilt-install
sudo npm install -g prebuild prebuild-install

# Download Kiwi-Project.
git clone https://gitlab.com/clausbendig/Kiwi-Project.git

# Download, compile and install MatIEC compiler.
hg clone https://bitbucket.org/mjsousa/matiec
cp Kiwi-Project/Kiwi-OS/RaspberryPi/MatIEC/* matiec/
cd matiec
autoreconf -i
./configure
make
sudo checkinstall -y --pkgname=matiec --pkgversion=1.0 --pkgrelease=1 --pkglicense=GPL --pkggroup=kiwi --maintainer=clausbendig@outlook.de
cd ..

# Compile and install Kiwi-Distributor.
cd Kiwi-Project/Kiwi-Distributor/
make
sudo checkinstall -y --pkgname=kiwi-distributor --pkgversion=1.0 --pkgrelease=1 --pkglicense=GPL --pkggroup=kiwi --maintainer=clausbendig@outlook.de
cd ../../

# Compile and install Kiwi-PLC.
cd Kiwi-Project/Kiwi-PLC/
npm install
npm run dist
sudo dpkg --install kiwi-plc_1.0.0_all.deb
cd ../../


# Download Linux kernel sources (4.14.y-rt).
git clone https://github.com/raspberrypi/linux.git -b rpi-4.14.y-rt

# Patch (MCP2517FD) and compile Linux kernel.
cd linux
patch -p1 < ../Kiwi-Project/Kiwi-OS/RaspberryPi/MCP2517FD/Patch.diff
make bcm2709_defconfig
cp ../Kiwi-Project/Kiwi-OS/RaspberryPi/Linux/config .config
make -j 4 zImage
make -j 4 modules
make -j 4 dtbs

# Install Linux kernel.
sudo ./scripts/mkknlimg ./arch/arm/boot/zImage /boot/kernel7.img
sudo make -j4 modules_install
sudo make -j4 dtbs_install

# Compile and install device-tree overlays.
sudo dtc -I dts -O dtb -o /boot/dtbs/4.14.91-rt49-v7+/overlays/mcp2515-can2.dtbo Kiwi-Project/Kiwi-OS/RaspberryPi/DT-Overlays/mcp2515-can2-overlay.dts
sudo dtc -I dts -O dtb -o /boot/dtbs/4.14.91-rt49-v7+/overlays/mcp2517fd-can2.dtbo Kiwi-Project/Kiwi-OS/RaspberryPi/DT-Overlays/mcp2517fd-can2-overlay.dts

# Install ESP8089 driver.
wget https://github.com/al177/esp8089/releases/download/1.9.20180430/esp8089-dkms_1.9.20180430_all.deb.gz
gzip -d esp8089-dkms_1.9.20180430_all.deb.gz
sudo dpkg --install esp8089-dkms_1.9.20180430_all.deb

# Copy config.txt and cmdline.txt to /boot/.
sudo cp Kiwi-Project/Kiwi-OS/RaspberryPi/FS/boot/config.txt /boot/config.txt
sudo cp Kiwi-Project/Kiwi-OS/RaspberryPi/FS/etc/network/interfaces.d/can0 /etc/network/interfaces.d/
sudo cp Kiwi-Project/Kiwi-OS/RaspberryPi/FS/etc/network/interfaces.d/usb0 /etc/network/interfaces.d/

sudo apt-get -y install dnsmasq
sudo mv /etc/dnsmasq.conf /etc/dnsmasq.conf_alt
sudo cp Kiwi-Project/Kiwi-OS/RaspberryPi/FS/etc/dnsmasq.conf /etc/

# Append ds1307 (RTC) to modules.
echo "rtc-ds1307" | sudo tee -a /etc/modules

# After reboot
#sudo hwclock -r && date
#sudo update-rc.d hwclock.sh enable
#sudo update-rc.d fake-hwclock remove
#sudo apt-get -y remove fake-hwclock
#sudo rm /etc/cron.hourly/fake-hwclock
#sudo rm /etc/init.d/fake-hwclock
#sudo cp Kiwi-Project/Kiwi-OS/RaspberryPi/FS/etc/init.d/hwclock.sh /etc/init.d/
